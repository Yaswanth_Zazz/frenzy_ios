Pod::Spec.new do |s|
  s.name          = "FrenzySDK"
  s.version       = "0.0.1"
  s.summary       = "iOS SDK for Frenzy"
  s.description   = "iOS SDK for Frenzy, including example app"
  s.homepage      =  "https://bitbucket.org/Yaswanth_Zazz/"   
 s.license       = { :type => "MIT", :file => "LICENSE" }
  s.author        = "Dennis_Zazz"
  s.platform      = :ios, "13.0"
 s.ios.deployment_target  = "13.0"
  s.swift_version = "4.2"
  s.source        = {
    :git => "https://bitbucket.org/Yaswanth_Zazz/frenzy_ios.git",
    :tag => "#{s.version}"
  }
  s.source_files        = "FrenzySDK", "FrenzySDK/**/*.{h,m,swift}"
  s.public_header_files = "FrenzySDK/**/*.h"
  s.resource_bundles = {'FrenzySDK' => ['FrenzySDK/Resources/**/*.{xib,storyboard,xcassets,ttf,TTF}'] }
 s.resources = "FrenzySDK/Resources/**/*.{xib,storyboard,xcassets,ttf,TTF}"

  s.dependency 'KingfisherWebP'
  s.dependency 'IQKeyboardManager'
  s.dependency 'Alamofire'
  s.dependency 'SwiftyJSON'
  s.dependency 'ObjectMapper'
  s.dependency 'AAPickerView'
  s.dependency 'UICircularProgressRing'
  s.dependency 'FLAnimatedImage' 
  s.dependency 'Socket.IO-Client-Swift'
  s.dependency 'SwiftMessages'
  s.dependency 'SearchTextField'
  s.dependency 'Haptico'
  s.dependency 'KDCircularProgress'
  s.dependency 'DropDown'
  s.dependency 'ActiveLabel'
  s.dependency 'MarqueeLabel' 

end