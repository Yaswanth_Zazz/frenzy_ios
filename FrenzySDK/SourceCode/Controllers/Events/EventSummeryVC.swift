

import UIKit

class EventSummeryVC: BaseViewController {
    
    static let identifier = "EventSummeryVC"
    @IBOutlet weak var lblTeam1: UILabel!
    @IBOutlet weak var lblTeam2: UILabel!
    @IBOutlet weak var ivTeam1: UIImageView!
    @IBOutlet weak var ivTeam2: UIImageView!
    @IBOutlet weak var tbl_summery: UITableView!
    @IBOutlet weak var viewTeam: UIView!
    @IBOutlet weak var viewEvent: UIView!
    @IBOutlet weak var lblTeamEvent: UILabel!
    @IBOutlet weak var ivTeamEvent: UIImageView!
    @IBOutlet weak var lblEventAddress: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblFinalScore: UILabel!
    @IBOutlet weak var lblScoreHeight: NSLayoutConstraint!
    @IBOutlet weak var lblFinalScoreHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var eventItem:EventItem = EventItem()
    var delightersFirst = [Delighter]()
    var tableViewData = [SummaryData]()
    var isLeftCell = true
    var eventTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTable()
        self.configure(item:self.eventItem)
        eventPostsGetCall()
        eventDetailGetCall()
    }
    
    func configure(item:EventItem){
        updateBGcolor(with: item)
        configureDateAndLocation(of: item)
        switch item.eventCategory {
        case .MATCH:
            self.lblTeam1.text = item.teamOne
            self.lblTeam2.text = item.teamTwo
            self.ivTeam1.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryVC.self), with:nil))
            self.ivTeam2.kf.setImage(with: URL.init(string: item.team2_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryVC.self), with:nil))
            self.viewTeam.isHidden = false
            self.viewEvent.isHidden = !self.viewTeam.isHidden
            self.lblScoreHeight.constant = 70
            self.lblFinalScoreHeight.constant = 32
            break
        case .EVENT:
            self.lblTeamEvent.text = eventTitle//item.eventName
            self.ivTeamEvent.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryVC.self), with:nil))
            self.viewTeam.isHidden = true
            self.viewEvent.isHidden = !self.viewTeam.isHidden
            self.lblScoreHeight.constant = 0
            self.lblFinalScoreHeight.constant = 0
            break
            case .none:
            break
        }
    }
    
    func updateBGcolor(with event: EventItem) {
        DispatchQueue.main.async {
            self.topView.backgroundColor = UIColor(hexColor: event.topColor)
            self.bottomView.backgroundColor = UIColor(hexColor: event.bottomColor)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite()
    }
    
    func configureDateAndLocation(of event: EventItem) {
        let date = event.event_timestamp?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "MM/dd/yyyy") ?? ""
        var completeAddress = ""
        if !(event.loc_title ?? "").isEmpty{
            completeAddress = event.loc_title!.trimmingCharacters(in: .whitespaces)
        }
           
        if !(event.loc_city ?? "").isEmpty {
            if completeAddress.isEmpty{
               completeAddress =  event.loc_city!.trimmingCharacters(in: .whitespaces)
            }else{
                 completeAddress = completeAddress + "," + event.loc_city!.trimmingCharacters(in: .whitespaces)
            }
            
        }
        
        if !(event.loc_state ?? "").isEmpty {
            if completeAddress.isEmpty{
                 completeAddress = event.loc_state!.trimmingCharacters(in: .whitespaces)
            }else{
                 completeAddress = completeAddress + "," + event.loc_state!.trimmingCharacters(in: .whitespaces)
            }
        }
        
        if completeAddress.isEmpty{
            lblEventAddress.text = date
        }else{
            lblEventAddress.text = date + ", " + completeAddress
        }
    }
    
    func populateTableViewData(){
        let startData = SummaryData(type: .starter, delighter: nil)
        tableViewData.append(startData)
        
        for (index,delighter) in delightersFirst.enumerated() {
            let dl = delighter
            if index%2 == 0 {
               dl.isLeft = true
            }else{
                 dl.isLeft = false
            }
            let lineData = SummaryData(type: .line, delighter: nil)
            tableViewData.append(lineData)
            tableViewData.append(lineData)
            let delighterData = SummaryData(type: .delighter, delighter: dl)
            tableViewData.append(delighterData)
        }
        
    }
    
    
    func createGapBetweenDelighters(with index: Int, isFirstHalf: Bool = true) {
        var delighters = [Delighter]()
        if isFirstHalf {
            delighters = delightersFirst
        }
//        else {
//            delighters = delightersSecond
//        }
        
        if index > 0 {
            let previousDelighter = delighters[index-1]
            let currentDelighter = delighters[index]
            
            let secDiff = abs(previousDelighter.remainingSeconds! - currentDelighter.remainingSeconds!)
            
            let loopIteration = secDiff/30
            
            for _ in 0..<loopIteration {
                let lineData = SummaryData(type: .line, delighter: nil)
                tableViewData.append(lineData)
            }
        }
        
    }
    
    func eventDetailGetCall(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventDetailGet(eventId: eventItem.id ?? 0)), completion: { apiResponse in
            
            switch apiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                guard let result = data?.result as? EventItem else {
                    return
                }
                if result.eventCategory ?? EventCategory.EVENT == .MATCH {
                    self.updateGoals(with: result)
                }
                
                //self.updateBGcolor(with: result)
                self.configure(item: result)
                break
            }
        })
    }
    
    func eventPostsGetCall(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventPostsGetWithMoment(eventId: eventItem.id ?? 0)), completion: { apiResponse in
            
            print("in Callback")
            
            switch apiResponse {
            case .Failure(_):
                
                break
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                self.delightersFirst.removeAll()
                let startData = SummaryData(type: .starter, delighter: nil)
                self.tableViewData.append(startData)
                var index = 0
                for result in results {
                    let lineData = SummaryData(type: .line, delighter: nil)
                    self.tableViewData.append(lineData)
                    self.tableViewData.append(lineData)
                    if (result["p_type"] as? String ?? "delighter") == "delighter" {
                        let delighter = Delighter().initDelighter(from: result)
                        let dl = delighter
                        if index%2 == 0 {
                            dl.isLeft = true
                        }else{
                            dl.isLeft = false
                        }
                        let delighterData = SummaryData(type: .delighter, delighter: dl)
                        self.tableViewData.append(delighterData)
                        index = index + 1
                    }else{
                        let movemtn = Movement().initMovementFrom(json: result)
                        let mv = movemtn
                        if index%2 == 0 {
                            mv.isLeft = true
                        }else{
                            mv.isLeft = false
                        }
                        let movemtnData = SummaryData(type: .movemnt, delighter: nil,moment: mv)
                        self.tableViewData.append(movemtnData)
                        index = index + 1
                    }
                }
                self.tbl_summery.reloadData()
                break
            }
        })
    }
    
    func updateGoals(with item: EventItem){
        self.lblScore.text = "\(item.team1_goals ?? 0) : \(item.team2_goals ?? 0)"
    }
    
    override func onClickDismiss(_ sender: Any ) {
        if let nev = self.navigationController{
            for controller in nev.viewControllers as Array {
                if controller.isKind(of: HomeVC.self) {
                    DispatchQueue.main.async {
                     nev.popToViewController(controller, animated: true)
                    }
                    break
                }
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

extension EventSummeryVC : UITableViewDelegate,UITableViewDataSource {
    
    func setupTable(){
        self.tbl_summery.dataSource = self
        self.tbl_summery.delegate = self
        self.tbl_summery.register(UINib.init(nibName: EventSummeryCell.identifier, bundle: Bundle(for: EventSummeryCell.self)), forCellReuseIdentifier: EventSummeryCell.identifier)
        self.tbl_summery.register(UINib.init(nibName: EventSummeryStartCell.identifier, bundle: Bundle(for: EventSummeryStartCell.self)), forCellReuseIdentifier: EventSummeryStartCell.identifier)
        self.tbl_summery.register(UINib.init(nibName: EventSummeryLineCell.identifier, bundle: Bundle(for: EventSummeryLineCell.self)), forCellReuseIdentifier: EventSummeryLineCell.identifier)
        self.tbl_summery.register(UINib.init(nibName: EventSummery2QCell.identifier, bundle: Bundle(for: EventSummery2QCell.self)), forCellReuseIdentifier: EventSummery2QCell.identifier)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // MARK: - Cell Line Calculation Height when few goals happend
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        // MARK: - Cell Line Calculation Height when few goals happend
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let summaryData = tableViewData[indexPath.row]
        
        switch summaryData.type {
        case .starter:
            let cell  = tableView.dequeueReusableCell(withIdentifier: EventSummeryStartCell.identifier, for: indexPath) as!  EventSummeryStartCell
            cell.selectionStyle = .none
            return cell
        case .line:
            let cell = tableView.dequeueReusableCell(withIdentifier: EventSummeryLineCell.identifier, for: indexPath) as!  EventSummeryLineCell
            cell.selectionStyle = .none
            return cell
        case .delighter:
            let cell  = tableView.dequeueReusableCell(withIdentifier: EventSummeryCell.identifier, for: indexPath) as!  EventSummeryCell
            cell.selectionStyle = .none
            cell.isLeft = summaryData.delighter!.isLeft
            cell.baseVC = self
            cell.movement = nil
            cell.eventId = eventItem.id ?? 0
            cell.eventBottomColor = eventItem.bottom_color ?? ""
            cell.delighter = summaryData.delighter ?? Delighter()
            cell.configure()
            return cell
        case .movemnt:
            let cell  = tableView.dequeueReusableCell(withIdentifier: EventSummeryCell.identifier, for: indexPath) as!  EventSummeryCell
            cell.selectionStyle = .none
            cell.isLeft = summaryData.moment!.isLeft
            cell.baseVC = self
            cell.delighter = nil
            cell.eventId = eventItem.id ?? 0
            cell.movement = summaryData.moment ?? Movement()
            cell.configureMovement()
            return cell
        case .quarter:
            let cell  = tableView.dequeueReusableCell(withIdentifier: EventSummery2QCell.identifier, for: indexPath) as!  EventSummery2QCell
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? EventsCell{
                cell.transform = .init(scaleX: 0.90, y: 0.90)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? EventsCell{
                cell.transform = .identity
            }
        }
    }
    
    
}



enum CellType: String {
    case starter = "starter"
    case line = "line"
    case delighter = "delighter"
    case quarter = "quarter"
    case movemnt = "movemtn"
}

struct SummaryData {
    var type: CellType
    var delighter: Delighter?
    var moment: Movement?
    
    init(type: CellType, delighter: Delighter?, moment : Movement? = nil) {
        self.type = type
        self.delighter = delighter
        self.moment = moment
    }
}
