

import UIKit
import SwiftyJSON

//protocol UpdateDemoDelighterAndMomentDataProtocol: class {
//    //func updateDelightNumAndMomentType()
//}
class EventMovementVC: BaseViewController {
    
    enum DemoMomentAndDelighterType {
        case none, moment, delighter
    }
    
    static let identifier = "EventMovementVC"
    @IBOutlet weak var lblTeam1: UILabel!
    @IBOutlet weak var lblTeam2: UILabel!
    @IBOutlet weak var ivTeam1: UIImageView!
    @IBOutlet weak var ivTeam2: UIImageView!
    @IBOutlet weak var lblCountDown: UILabel!
    @IBOutlet weak var tbl_movement: UITableView!
    @IBOutlet weak var viewTeam: UIView!
    @IBOutlet weak var viewEvent: UIView!
    @IBOutlet weak var lblTeamEvent: UILabel!
    @IBOutlet weak var ivTeamEvent: UIImageView!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var demoTutorialMovementView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    var refreshControl = FrenzyRefreshControl()
    var eventItem:EventItem = EventItem()
    var delighters = [AnyObject]()//Delighter
    var team1Goals = "0"
    var team2Goals = "0"
    var timer: Timer?
    var eventTitle : String = ""
    var isEventEnd:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vcs = self.navigationController?.viewControllers {
            for itemVC in vcs{
                if(itemVC.isKind(of: EventCountDownVC.self)){
                    itemVC.removeFromParent()
                    break
                }
            }
        }
        self.setupTable()
        self.configure(item:eventItem)
        self.tbl_movement.addRefreshControll(actionTarget: self, action: #selector(refreshData))
       
        if isGlobalDemoEvent , let title = eventItem.title , title.lowercased() == "start here!"{
        
            demoDelighterNum = 1
            demoEventMomentType = .delighter
            isMomentApiCalled = false
            isFirstDelighterApiCalled = false
            isSecondDelighterApiCalled = false
            isThirdDelighterApiCalled = false
            
            self.getDemoDelighterAndMoment(eventID: eventItem.id ?? 0)
            demoTutorialMovementView.isHidden = true
        }else{
            demoTutorialMovementView.isHidden = true
        }
        
        IOSocket.sharedInstance.connect()
        receiveSockets()
        self.setObserver()
        self.eventDetailGetCall()
    }
    
    override func appCameFromBackGround() {
        self.eventDetailGetCall()
        IOSocket.sharedInstance.connect()
    }
    
    @objc func startAnimation(){
        startTutorialANimation(demoTutorialMovementView, nil)
    }
    
    func setObserver() {
        print("Set Observer demo event:\(demoEventId)")
        
        NotificationCenter.default.addObserver(forName: .homeFeedRefresh, object: nil, queue: OperationQueue.main) { (notifItem) in
            if isGlobalDemoEvent{
               print("Set observer ntification")
            }else{
                print("event post ")
                self.eventPostsGetCall()
                self.eventDetailGetCall()
            }
        }
        if isGlobalDemoEvent , let title = eventItem.title , title.lowercased() == "start here!" {
            NotificationCenter.default.addObserver(self, selector: #selector(updateDelightNumAndMomentType), name: .demoDeligter, object: nil)
        }
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: .demoDeligter, object: nil)
    }

    @objc func refreshData() {
        self.eventPostsGetCall()
    }
    
    deinit {
        self.removeObserver()
    }
    
    override func onMomentEnd(id: Int) {
        super.onMomentEnd(id: id)
        print("Moments end")
        self.delay(0.5) {
          self.refreshData()
        }
    }
    
    func updateBGcolor(with event: EventItem) {
        DispatchQueue.main.async {
            self.topView.backgroundColor = UIColor(hexColor: event.topColor)
            self.bottomView.backgroundColor = UIColor(hexColor: event.bottomColor)
        }
    }
    
    func receiveSockets(){
        IOSocket.sharedInstance.getGoalsCount(completion: { result in
            if result.count > 0 {
                let eventId = "\(self.eventItem.id ?? 0)"
                let team1Id = "\(self.eventItem.team1_id ?? 0)"
                let team2Id = "\(self.eventItem.team2_id ?? 0)"
                                
                if eventId == result.first?["event_id"] as? String{
                    print("Event is  String")
                    DispatchQueue.main.async {
                        if team1Id == result.first?["team_id"] as? String{
                            self.team1Goals = result.first?["total_goals"] as? String ?? "\(result.first?["total_goals"] as? Int ?? 0)"
                        }
                        if team2Id == result.first?["team_id"] as? String ?? "\(result.first?["team_id"] as? Int ?? 0)" {
                            self.team2Goals = result.first?["total_goals"] as? String ?? "\(result.first?["total_goals"] as? Int ?? 0)"
                        }
                        if let nbaStatus = result.first?["nba_status_text"] as? String , !nbaStatus.isEmpty{
                            self.scoreLbl.text = nbaStatus
                            self.scoreLbl.isHidden = false
                        }else{
                            self.scoreLbl.isHidden = true
                        }
                        self.updateGoals()
                    }
                }
                else if eventId == "\(result.first?["event_id"] as? Int ?? 0)" {
                    DispatchQueue.main.async {
                            if let teamId =  result.first?["team_id"] as? Int, team1Id == "\(teamId)" {
                                self.team1Goals = result.first?["total_goals"] as? String ?? "\(result.first?["total_goals"] as? Int ?? 0)"
                            }
                            if let teamId =  result.first?["team_id"] as? Int, team2Id == "\(teamId)" {
                                self.team2Goals = result.first?["total_goals"] as? String ?? "\(result.first?["total_goals"] as? Int ?? 0)"
                            }
                            if let nbaStatus = result.first?["nba_status_text"] as? String , !nbaStatus.isEmpty{
                                self.scoreLbl.text = nbaStatus
                                self.scoreLbl.isHidden = false
                            }else{
                                self.scoreLbl.isHidden = true
                            }
                            self.updateGoals()
                        }
                    }
                }
        })
        
        IOSocket.sharedInstance.getEventEnd(completion: { result in
            print("result:\(result)")
            if result.count > 0 {
                guard let eventId = self.eventItem.id else {
                    return
                }
                var isDelete = ""
                if let value = result.first!["is_delete"] as? Int{
                    isDelete = "\(value)"
                }else if let value = result.first!["is_delete"] as? String{
                    isDelete = value
                }
                if isDelete == "1"{
                    self.dismissEventMovementVC()
                }else{
                    let id = result.first!["event_id"] as? String ?? "\(result.first!["event_id"] as? Int ?? 0)"
                    print("event id:\(id)")
                    if "\(eventId)" == id , !self.isEventEnd{
                        self.isEventEnd = true
                        self.moveToEventSummaryVC()
                    }
                }
            }
        })
        
        IOSocket.sharedInstance.getVideoUpdate { (result) in
            if result.count > 0 {
                self.refreshData()
            }
        }
        
        IOSocket.sharedInstance.stopMoment(completion: { result in
            print("End Moments Result:\(result)")
            if result.count > 0 {
                self.refreshData()
                if (result.first!["moment_id"] as? Int) != nil{
                    print("Moment stops")
                   // self.onMomentEnd(id: id)
                   // momentEndIds.append(id)
                }
            }
        })
    }
    
    func updateGoals(){
        self.lblCountDown.text = "\(self.team1Goals) : \(self.team2Goals)"
    }
    
    func moveToEventSummaryVC(){
        let vc =  EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
        vc.eventItem = self.eventItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func eventPostsGetCall(){
        
        DispatchQueue.main.async {
             self.tbl_movement.pullAndRefresh()
        }
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventPostsGetWithMoment(eventId: eventItem.id ?? 0)), completion: { [self] apiResponse in
            self.tbl_movement.endRefreshing()
            switch apiResponse {
            case .Failure(_):
                self.delighters.removeAll()
                self.tbl_movement.reloadData()
                break
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                self.delighters.removeAll()
                var isMoveToVideoscreen = false
                for result in results {
                    if (result["p_type"] as? String ?? "delighter") == "delighter" {
                        let delighter = Delighter().initDelighter(from: result)
                        self.delighters.append(delighter)
                        
                        if isGlobalDemoEvent{
                            self.delay(1, completion: {
                             
                            })
                        }
                    }else{
                        let movemtn = Movement().initMovementFrom(json: result)
                        self.delighters.append(movemtn)
                        if isGlobalDemoEvent{
                            self.delay(1, completion: {
                         
                            })
                        }else{
                        if selectedMomentID > 0{
                            if movemtn.id == selectedMomentID{
                                if movemtn.video_count > 0{
                                    isMoveToVideoscreen = true
                                }
                            }
                        }
                        }
                    }
                }
                if isMoveToVideoscreen {
                    let vc =  MovemenVideosVC.instantiate(fromAppStoryboard: .Movement)
                    vc.momentId = selectedMomentID
                    vc.eventId = eventItem.id
                    self.navigationController?.pushViewController(vc, animated: true)
                    selectedMomentID = 0
                    isMoveToVideoscreen = false
                    return
                }
                self.tbl_movement.reloadData()
                break
            }
        })
    }
    
    func eventDetailGetCall(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventDetailGet(eventId: eventItem.id ?? 0)), completion: { apiResponse in
            
            switch apiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                guard let result = data?.result as? EventItem else {
                    return
                }
                self.eventItem.section_id = result.section_id
                self.eventItem.my_section = result.my_section
                self.team1Goals = "\(result.team1_goals ?? 0)"
                self.team2Goals = "\(result.team2_goals ?? 0)"
                self.updateGoals()
               // self.updateBGcolor(with: result)
                self.configure(item: result)
                
                break
            }
        })
    }
    
    func configure(item:EventItem){
        updateBGcolor(with: item)
        
        switch item.eventCategory {
        case .MATCH:
            self.lblTeam1.text = item.teamOne
            self.lblTeam2.text = item.teamTwo
            self.ivTeam1.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder:  UIImage(named: "placeholder", in: Bundle(for: CommentCell.self), with: nil))
            self.ivTeam2.kf.setImage(with: URL.init(string: item.team2_logo ?? ""), placeholder:  UIImage(named: "placeholder", in: Bundle(for: CommentCell.self), with: nil))
            self.viewTeam.isHidden = false
            self.viewEvent.isHidden = !self.viewTeam.isHidden
            
            if let isShowScore = item.show_score , isShowScore == 1{
                self.topViewHeightConstraint.constant = 205
                self.lblCountDown.isHidden = false
                if(item.is_nba=="NBA" || item.is_nba=="WNBA"){
                    self.scoreLbl.text = item.nba_status_text
                    self.scoreLbl.isHidden = false
                }else{
                    scoreLbl.isHidden = true
                }
                self.updateGoals()
            }else{
                self.lblCountDown.isHidden = true
            }
             
            break
        case .EVENT:
            if let team1 = item.team1_title, !team1.isEmpty {
                  self.lblTeamEvent.text = team1
            }else{
                self.lblTeamEvent.text = item.eventName //eventTitle
            }
          
            self.viewTeam.isHidden = true
            self.viewEvent.isHidden = !self.viewTeam.isHidden
            self.ivTeamEvent.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder:  UIImage(named: "placeholder", in: Bundle(for: CommentCell.self), with: nil))
            self.lblCountDown.isHidden = true
            //self.topViewHeightConstraint.constant = 160
            self.view.layoutIfNeeded()
            break
            case .none:
            break
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite()
        self.eventPostsGetCall()
        self.eventDetailGetCall()
        self.tbl_movement.reloadData()
        if eventItem.title?.lowercased() == "start here!"{
            print("start here events")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       // self.tutorialCommentContainerView.layer.removeAllAnimations()
        self.timer?.invalidate()
    }
    
    func setUpDemoMomentANimationView(){
         startAnimation()
       // self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startAnimation), userInfo: nil, repeats: true)
    }
    
    override func onClickDismiss(_ sender: Any ) {
        dismissEventMovementVC()
    }
    
    func dismissEventMovementVC(_ isFromBaseVC: Bool = false){
        if let nev = self.navigationController{
            for controller in nev.viewControllers as Array {
                if controller.isKind(of: HomeVC.self) {
                    self.clearDemoEventApi()
                    if !isFromBaseVC , eventItem.title?.lowercased() == "start here!"{
                        print("from EventMovement")
                        isDemoEventClear = true
                    }else{
                        isDemoEventClear = false
                        print("from Base VC")
                    }
                    self.delay(0.0) {
                        nev.popToViewController(controller, animated: false)
                    }
                    break
                }
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickSettings(_ sender: Any) {
        self.showSettingPopUp(isVenuHide: !self.eventItem.isGroundEvent) { (index) in
            if index ==  1{
                self.openSettingsVC(with: self.eventItem)
            }else if index == 2{
                let vc =  EventTicketVC.instantiate(fromAppStoryboard: .home)
                vc.eventItem = self.eventItem
                vc.isFromSettings = true
                self.present(vc, animated: true, completion: nil)
            }else if index == 3{
                  self.confirmationCustomeAlert(title: "", discription: "Are you sure you want to exit this event?", btnColor1: UIColor.colorFromRGB(0xff1a57), btnColor2: UIColor.colorFromRGB(0xffffff),btnTitle1: "YES", btnTitle2: "NO", complition: { (confirm, tag) in
                             if confirm {
                                // clear demo event settings
                                self.clearDemoEventApi()
                                ///
                                self.showLoading(on: self.view, of: FrenzyColor.white)
                                APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: "CALL `sp_eventExit`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(self.eventItem.id!))"), completion: { apiResponse in
                                        switch apiResponse {
                                        case .Failure(_):
                                            self.showErrorToast(title: "Some thing wrong!", description: "Server not reponding properly, try again!") { (isDone) in
                                                
                                            }
                                            break
                                        case .Success(_):
                                            let vc = HomeVC.instantiate(fromAppStoryboard: .home)
                                             self.navigationController?.pushViewController(vc, animated: true)
                                            
                                            break
                                        }
                                    })
                             }
                         })
            }
        }
    }
    
    
}

extension EventMovementVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delighters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = delighters[indexPath.row]
        if let delighter = item as? Delighter {
            
            if delighter.title != "", delighter.description != "" {
                //show detail cell
                let cell  = tableView.dequeueReusableCell(withIdentifier: MovementDetailCell.identifier, for: indexPath) as!  MovementDetailCell
                cell.delighter = delighter
                cell.configure()
                cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
                cell.clickableDelighterUrlClousure = { url in
                    self.delay(0) {
                        self.delighterLinkViewApi(urlStr: url, delighter: delighter)
                    }
                }
                cell.selectionStyle = .none
                return cell
            } else if delighter.description.isEmpty || delighter.title.isEmpty {
                let cell  = tableView.dequeueReusableCell(withIdentifier: MovementSimpleCell.identifier, for: indexPath) as!  MovementSimpleCell
                cell.baseVC = self
                cell.delighter = delighter
                cell.configure(isDescription: delighter.description.isEmpty)
                cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
                cell.clickableDelighterUrlClousure = { url in
                    self.delay(0) {
                        self.delighterLinkViewApi(urlStr: url, delighter: delighter)
                    }
                }
                cell.selectionStyle = .none
                
                return  cell
                
            }else {
                //show image/sponsor cell
                let cell = tableView.dequeueReusableCell(withIdentifier: MovementSponserCell.identifier, for: indexPath) as!  MovementSponserCell
                cell.baseVC = self
                cell.configure(image: delighter.image ?? "")
                cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
                cell.selectionStyle = .none
                return  cell
            }
        }
        else {
            if let moment = item as? Movement {
               
                let cell  = tableView.dequeueReusableCell(withIdentifier: MovementKindVideoCell.identifier, for: indexPath) as!  MovementKindVideoCell
                cell.ivProfile.kf.setImage(with: URL.init(string: moment.image), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventMovementVC.self), with:nil))
              
                if moment.title.isValidHtmlString(){
                    cell.lblTitle.attributedText = moment.title.htmlToAttributedString
                }else {
                    cell.lblTitle.text = moment.title.htmlToString ?? ""
                }
                if moment.title.contains("style=\"color:#ffffff"), (moment.title.contains("background-color:#ffffff") || !moment.title.contains("background-color:")){
                    cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                cell.lblTitle.lineBreakMode = .byTruncatingTail
                cell.labelVideoCount.text = "\(moment.video_count)"
                cell.lblTitle.sizeToFit()
                if moment.isLive{
                    cell.mainViewe.backgroundColor = FrenzyColor.white
                    cell.liveView.isHidden = false
                    cell.videoView.isHidden = true
                }else{
                    cell.mainViewe.backgroundColor = .white
                    cell.liveView.isHidden = true
                    cell.videoView.isHidden = false
                }
                cell.selectionStyle = .none
                return  cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO CALL IN CASE OF WAVE VIDEO SLIDER
        let item = delighters[indexPath.row]
        if let delighter = item as? Delighter {
            self.openDelighterVC(with: delighter, isFromTimeLine: true , isFromTblSelection: true , eventID: "\(eventItem.id ?? 0)" , bottomColor: eventItem.bottomColor)
        }else{
            if let moment = item as? Movement{
                if moment.isLive {
                    
                    let nowDuratioin = moment.remainingDuration
                    if nowDuratioin > 0 && !momentEndIds.contains(moment.id){
                        //
                        let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                        let flashArr = UserDefaults.standard.array(forKey: kFlashKey) as? [Bool] ?? [Bool]()
                        let screenArr = UserDefaults.standard.array(forKey: kScreenKey) as? [Bool] ?? [Bool]()
                        let vibrateArr = UserDefaults.standard.array(forKey: kVibrateKey) as? [Bool] ?? [Bool]()
                        let sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
                        
                        print("All Event Id: \(eventIdArr)")
                        
                        for (index,eventId) in eventIdArr.enumerated() {
                            if eventId == moment.eventId{
                                let userPermission = UserPermissionInfo(flash: flashArr[index], screen: screenArr[index], vibrate: vibrateArr[index])
                                let mySec = sectionIdArr[index]
                                self.openMovementVC(with: moment, userPermission: userPermission,duration: nowDuratioin , isFromTblSelection: true  , eventId: "\(eventId)" , mySec: mySec)
                                break
                            }
                        }
                    }else{
                        if let cell = tableView.cellForRow(at: indexPath) as? MovementKindVideoCell{
                            cell.mainViewe.backgroundColor = .white
                            cell.liveView.isHidden = true
                            cell.videoView.isHidden = false
                            cell.shake()
                            
                        }else{
                            let cell = tableView.cellForRow(at: indexPath)
                            cell?.shake()
                        }
                    }
                }else if moment.video_count > 0 {
                    let vc = MovemenVideosVC.instantiate(fromAppStoryboard: .Movement)
                    vc.momentId = moment.id
                    vc.eventId = moment.eventId
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let cell = tableView.cellForRow(at: indexPath)
                    cell?.shake()
                }
                
            }
        }
    }
    
    func setupTable(){
        self.tbl_movement.delegate = self
        self.tbl_movement.dataSource = self
        self.tbl_movement.register(UINib.init(nibName: MovementSimpleCell.identifier, bundle: Bundle(for: MovementSimpleCell.self)), forCellReuseIdentifier: MovementSimpleCell.identifier)
        self.tbl_movement.register(UINib.init(nibName: MovementSponserCell.identifier, bundle: Bundle(for: MovementSponserCell.self)), forCellReuseIdentifier: MovementSponserCell.identifier)
        self.tbl_movement.register(UINib.init(nibName: MovementDetailCell.identifier, bundle: Bundle(for: MovementDetailCell.self)), forCellReuseIdentifier: MovementDetailCell.identifier)
        self.tbl_movement.register(UINib.init(nibName: MovementKindVideoCell.identifier, bundle: Bundle(for: MovementKindVideoCell.self)), forCellReuseIdentifier: MovementKindVideoCell.identifier)
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? MovementSimpleCell{
                cell.transform = .init(scaleX: 0.90, y: 0.90)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? MovementSimpleCell{
                cell.transform = .identity
            }
        }
    }
}

extension EventMovementVC : TwicketSegmentedControlDelegate{
    func didSelect(_ segmentIndex: Int) {
        switch segmentIndex {
        case 0:
            break
        case 1:
           
            break
        default:
            break
        }
    }
}

//MARK:- DemoDelighterAndMomentprotocol
extension  EventMovementVC {
    
    func delighterLinkViewApi(urlStr: String , delighter: Delighter){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.api_DelighterLinkView(event_id: "\(eventItem.id ?? 0)", control_id: "\(delighter.id ?? 0)", control_type: "delighter_link", viewer_id: "\(self.localUser?.userItem.id ?? 0)"  , url: urlStr)) { (ApiResponse) in
            switch ApiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                print(data as Any)
            }
        }
    }
    
    func clearDemoEventApi(){
        if isGlobalDemoEvent , let title =  eventItem.title , title.lowercased() == "start here!"{
            isGlobalDemoEvent = false
            demoDelighterNum = 1
            demoEventMomentType = .delighter
            isMomentApiCalled = false
            isFirstDelighterApiCalled = false
            isSecondDelighterApiCalled = false
            isThirdDelighterApiCalled = false
            demoEventId = 0
            UserDefaults.standard.removeObject(forKey: "demo_event")
            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.removeObserver(self, name: .demoDeligter, object: nil)
            
            var eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
            if let id = eventItem.id ,eventIdArr.contains(id){
                for (index , savedId) in eventIdArr.enumerated(){
                    if savedId == id{
                        eventIdArr.remove(at: index)
                        break
                    }
                }
                UserDefaults.standard.set(eventIdArr, forKey: kEventIdKey)
            }
            
            self.getDemoDelighterAndMoment(true , eventID: eventItem.id!)
        }else{
            print("Demo event not clear")
        }
    }
    
    @objc func updateDelightNumAndMomentType() {
     
        if let title = eventItem.title , title.lowercased() == "start here!"{
        print("Demo update Notification")
        
            if demoDelighterNum == 1 , demoEventMomentType == .delighter{
            print("Hello Moment")
            demoEventMomentType = .moment
            if !isMomentApiCalled{
                self.delay(2) {
                    self.exitDemoDelighterEvents()
                    if !isDemoEventClear {
                        self.getDemoDelighterAndMoment(eventID : self.eventItem.id ?? 0)
                    }
                }
            }
        }
        
        else if demoDelighterNum == 1 , demoEventMomentType == .moment{
            print("Hello Delighter 2")
          
            if !isSecondDelighterApiCalled{
                if self.demoTutorialMovementView.isHidden{
                    self.demoTutorialMovementView.isHidden = false
                    self.delay(2.0){
                        self.setUpDemoMomentANimationView()
                    }
                }
                self.delay(6) {
                    
                    self.exitDemoDelighterEvents()
                    
                    demoDelighterNum = 2
                    demoEventMomentType = .delighter
                    self.timer?.invalidate()
                  
                    self.delay(2.0){
                        self.demoTutorialMovementView.isHidden = true
                    }
                    if !isDemoEventClear {
                        self.getDemoDelighterAndMoment(eventID : self.eventItem.id ?? 0)
                    }
                }
            }
        }
        else if demoEventMomentType == .delighter , demoDelighterNum == 2{
            print("delighter 3")
                      
            if !isThirdDelighterApiCalled{
                self.delay(2) {
                    self.exitDemoDelighterEvents()
                    demoDelighterNum = 3
                    demoEventMomentType = .delighter
                    if !isDemoEventClear {
                        self.getDemoDelighterAndMoment(eventID : self.eventItem.id ?? 0)
                    }
                }
            }
        }else{
            print("demo num:\(demoEventMomentType) , demo number:\(demoDelighterNum)")
        }
        }else{
            print("Start Here event are not available")
        }
    }
    
    func exitDemoDelighterEvents(){
        if let Vcs = self.navigationController?.viewControllers {
            for vc in Vcs{
                
                if let movementVideo  = vc as? MovemenVideosVC{
                    print("exit Video from Demo")
                    movementVideo.onClickDismiss(UIButton())
                }
                if let momentWaveVc = vc as? MovementWaveVC {
                    print("exit Movements from Demo")
                    momentWaveVc.onClickDismiss(UIButton())
                }
                if let momentTextVc = vc as? MovementTextVC{
                    print("exit Delighter from Demo")
                    momentTextVc.onClickDismiss(UIButton())
                }
            }
        }
    }
}
