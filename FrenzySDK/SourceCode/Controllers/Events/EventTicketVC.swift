

import UIKit
import AAPickerView

struct LevelType {
     static let A_Z = "A-Z"
     static let Z_A = "Z-A"
     static let a1_n = "1-n"
     static let n_1 = "n-1"
}

class EventTicketVC: BaseViewController {
    static let identifier = "EventTicketVC"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTeamLeft: UIImageView!
    @IBOutlet weak var tickAtVenue: UIImageView!
    @IBOutlet weak var tickHome: UIImageView!
    @IBOutlet weak var imgTeamRight: UIImageView!
    @IBOutlet weak var imgTeamEvent: UIImageView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var farAwaySwitch: UISwitch!
    @IBOutlet weak var tfSelectSeat: AAPickerView!
    @IBOutlet weak var tfSelectRow: AAPickerView!
    @IBOutlet weak var tfSelectLevel: AAPickerView!
    @IBOutlet weak var tfSectionNumber: AAPickerView!
    
    @IBOutlet weak var assignedSeatConstraints: NSLayoutConstraint!
    @IBOutlet weak var genralAddmissionEventview: UIView!
    @IBOutlet weak var assignedAddmissionEventview: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var viewRight: UIView!
    @IBOutlet weak var vewGround: UIView!
    @IBOutlet weak var imgLeftHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgLeftWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: TransitionButton!
    @IBOutlet weak var loaderView: UIView!
    var selected_level,selected_row,selected_seat : String?
    @IBOutlet weak var ivBack: UIImageView!
    @IBOutlet weak var roundView: UIView!   //round ground container
    @IBOutlet weak var attendingTutorialContainerView: UIView!
    @IBOutlet weak var tutorialSelectSectionContainerView: UIView!
    @IBOutlet weak var btnVenue: TransitionButton!
    
    @IBOutlet weak var generelAdmissionViewHeight: NSLayoutConstraint!
    var isGenralSeats : Bool = true
    @IBOutlet weak var btnRemote: TransitionButton!
    @IBOutlet weak var stageView: UIView!
    @IBOutlet weak var stageSectionView: UIView!
    @IBOutlet weak var scrollView: UIView!
    @IBOutlet weak var imgAssignedSeets: UIImageView!

    var levelsArray = ["A","B","C","D"]
    var rowsArray = ["1","2","3","4"]
    var seatArray = ["1","2","3","4","5","6","7"]
    var eventItem : EventItem = EventItem()
    var sections = [Section]()
    var genralsections = [Section]()
    
    var isSectionValueChanged : Bool = false
        
    var isFromSettings = false
    var isVenuSelected : Bool = true
    var timer: Timer?
    var isFromStartHereEvent : Bool = false
    
    func genrateStrinFromcount(count : Int) -> [String] {
        var arr = [String]()
        for index in 1..<count+1{
            arr.append("\(index)")
        }
        return arr
    }
    
    func handleTutorialView(isRemote:Bool){
        if isFromStartHereEvent{
            startAnimation()
            self.genralAddmissionEventview.isHidden = true
            attendingTutorialContainerView.isHidden = !isRemote
            tutorialSelectSectionContainerView.isHidden = isRemote
        }
    }
    
    @IBAction func onClickVenue(_ sender: Any) {
        self.disablesubmitButton()
        handleTutorialView(isRemote: false)
        isVenuSelected = true
        self.btnVenue.alpha = 1.0
        self.btnRemote.alpha = 0.95
        self.viewSeats.isHidden = false
        self.tickHome.isHidden = true
        self.tickAtVenue.isHidden = false
        self.viewSeats.isHidden = false
        self.imgAssignedSeets.image = UIImage(named: "un_check_box", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgGenrralAdmission.image = UIImage(named: "un_check_box", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.genralAddmisionSetting()
    }
    
    @IBAction func onClickRemote(_ sender: Any) {
        handleTutorialView(isRemote: true)
        self.enableSubmitButton()
        isVenuSelected = false
        self.btnVenue.alpha = 0.95
        self.btnRemote.alpha = 1.0
        self.tickHome.isHidden = false
        self.tickAtVenue.isHidden = true
        self.scrollView.isHidden = true
        self.viewSeats.isHidden = true
    }
    
    @IBOutlet weak var imgGenrralAdmission: UIImageView!
    
    @IBOutlet weak var viewSeats: UIView!
    
    @IBAction func onClickGenralAdmission(_ sender: Any) {
        isGenralSeats = true
        self.enableSubmitButton()
        self.imgGenrralAdmission.image = UIImage(named: "check_box", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgAssignedSeets.image = UIImage(named: "un_check_box", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.scrollView.isHidden = true
        self.scrollView.isUserInteractionEnabled = true
        self.scrollView.alpha = 1.0
        self.farAwaySwitch.isOn = true
    }
    
    @IBAction func onClickAssignedSeats(_ sender: Any) {
        isGenralSeats = false
        self.disablesubmitButton()
        self.imgAssignedSeets.image = UIImage(named: "check_box", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgGenrralAdmission.image = UIImage(named: "un_check_box", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.scrollView.isHidden = false
        self.scrollView.isUserInteractionEnabled = true
        self.scrollView.alpha = 1.0
        self.farAwaySwitch.isOn = false
        if self.validatioin() {
            self.enableSubmitButton()
        }
    }
    
        
    func genrateLevelsFromcount(count : Int, type : String) -> [String] {
        if type == LevelType.A_Z{
            var arr = [String]()
            for index in 0..<count{
                if index > a_z_Array.count{
                    arr.append("\(a_z_Array[index%26])\(a_z_Array[index%26])")
                }else{
                    arr.append("\(a_z_Array[index%26])")
                }
            }
            return arr
        }else if type == LevelType.Z_A{
            var arr = [String]()
            for index in 0..<count{
                if index > z_a_Array.count{
                    arr.append("\(z_a_Array[index%26])\(z_a_Array[index%26])")
                }else{
                    arr.append("\(z_a_Array[index%26])")
                }
            }
            return arr
        }else if type == LevelType.a1_n{
            var arr = [String]()
            for index in 1..<count+1{
                arr.append("\(index)")
            }
            return arr
        }else{
            var arr = [String]()
            for index in 1..<count+1{
                arr.append("\(count+1-index)")
            }
            return arr
        }
    }
    
    
    func genralAddmisionSetting() {
        if self.eventItem.isGenralevent {
            self.assignedAddmissionEventview.isHidden = true
            if(isFromSettings){
                self.onClickGenralAdmission(UIButton())
            }
        }else if self.eventItem.isAssignedevent{
            self.assignedSeatConstraints.constant = -24
            self.genralAddmissionEventview.isHidden = true
            if(isFromSettings){
                self.onClickAssignedSeats(UIButton())
            }
        }else{
            self.assignedSeatConstraints.constant = 24
         
            if(isFromSettings){
                let list = DataManager.sharedInstance.getGenralEvents()
                if list.contains(self.eventItem.id ?? 0){
                    self.onClickGenralAdmission(UIButton())
                }else{
                    self.onClickAssignedSeats(UIButton())
                }
            }
            self.assignedAddmissionEventview.isHidden = false
            self.genralAddmissionEventview.isHidden = false
        }

        if isFromStartHereEvent{
            self.genralAddmissionEventview.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = FrenzyColor.greenColor
        self.configure(item:eventItem)
        self.setupViewsHeightAccordingToIphone()
        self.setupBackButton()
        self.tfSectionNumber.pickerType = .string(data: numbersSection)
       
        self.tfSectionNumber.valueDidSelected = { (index) in
            DispatchQueue.main.async {
                self.isSectionValueChanged = true
                self.updateGround(with: index as! Int)
            }
        }
        self.tfSectionNumber.pickerView(self.tfSectionNumber.stringPicker!, didSelectRow: 0, inComponent: 0)
        self.scrollView.isHidden = true
       
        self.setUpSubmitButton()
        //MARK: Select Level
        self.levelsArray = self.genrateLevelsFromcount(count: self.eventItem.levels ?? 0, type: self.eventItem.level_order ?? "A-Z")
        self.tfSelectLevel.pickerType = .string(data: levelsArray)
      
        self.tfSelectLevel.valueDidSelected = { (index) in
            DispatchQueue.main.async {
                if self.validatioin() {
                    self.enableSubmitButton()
                }
                self.selected_level = self.levelsArray[index as! Int]
                self.upDataDataAdditionalInfoForAnimateMomment(level: self.selected_level)
            }
        }
        self.tfSelectLevel.pickerView(self.tfSelectLevel.stringPicker!, didSelectRow: 0, inComponent: 0)
        
        //MARK: Select Row
        self.rowsArray = self.genrateStrinFromcount(count: self.eventItem.rows_per_level ?? 10)
        self.tfSelectRow.pickerType = .string(data: rowsArray)
       
        self.tfSelectRow.valueDidSelected = { (index) in
            DispatchQueue.main.async {
                if self.validatioin() {
                    self.enableSubmitButton()
                }
                self.selected_row = self.rowsArray[index as! Int]
                self.upDataDataAdditionalInfoForAnimateMomment(row: self.selected_row)
            }
        }
        self.tfSelectRow.pickerView(self.tfSelectRow.stringPicker!, didSelectRow: 0, inComponent: 0)
        
        //MARK: Select Seat
        self.seatArray = self.genrateStrinFromcount(count: self.eventItem.seats_per_row ?? 100)
        self.tfSelectSeat.pickerType = .string(data: seatArray)
      
        self.tfSelectSeat.valueDidSelected = { (index) in
            DispatchQueue.main.async {
                
                if self.validatioin() {
                    self.enableSubmitButton()
                }
                self.selected_seat = self.seatArray[index as! Int]
                self.upDataDataAdditionalInfoForAnimateMomment(seat: self.selected_seat)
            }
        }

        let saveArray = DataManager.sharedInstance.getAdditionalDataForAnimateMoment()
        var getSavedModel = AdditionalInfoAnimateMoment()
        for index in 0..<saveArray.count{
            let data  = saveArray[index]
            if data.event_id == self.eventItem.id ?? 0 {
                getSavedModel = data
                break
            }
        }
        self.selected_row = getSavedModel.row
        self.tfSelectRow.text = self.selected_row
        self.selected_level =  getSavedModel.level
        self.tfSelectLevel.text = self.selected_level
        self.selected_seat = getSavedModel.seatNumber
        self.tfSelectSeat.text = self.selected_seat
        
        self.tfSelectSeat.pickerView(self.tfSelectSeat.stringPicker!, didSelectRow: 0, inComponent: 0)
        
        self.getEventSectionCall()
        self.setupBtnSubmit()
    
        if isFromStartHereEvent{
            generelAdmissionViewHeight.constant = 0
            genralAddmissionEventview.isHidden = true
            handleTutorialView(isRemote: true)
            startAnimation()
            //self.timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.startAnimation), userInfo: nil, repeats: true)
        }else{
            attendingTutorialContainerView.isHidden = true
            tutorialSelectSectionContainerView.isHidden = true
        }
    }
    
    @objc func startAnimation(){
        startTutorialANimation(attendingTutorialContainerView, tutorialSelectSectionContainerView)
    }

    func setUpSubmitButton() {
         let list = DataManager.sharedInstance.getFarAwayEvents()
               if isFromSettings {
                   if list.contains(eventItem.id ?? 0){
                       isVenuSelected = false
                       self.btnVenue.alpha = 0.95
                       self.btnRemote.alpha = 1.0
                       
                       self.tickHome.isHidden = false
                       self.tickAtVenue.isHidden = true
                       
                       self.scrollView.isHidden = true
                       self.viewSeats.isHidden = true
                       self.delay(0.2) {
                           if self.validatioin(){
                               self.enableSubmitButton()
                           }
                       }
                   }else{
                       self.disablesubmitButton()
                       isVenuSelected = true
                       self.btnVenue.alpha = 1.0
                       self.btnRemote.alpha = 0.95
                       self.viewSeats.isHidden = false
                       self.tickHome.isHidden = true
                       self.tickAtVenue.isHidden = false
                       self.viewSeats.isHidden = false
                       self.genralAddmisionSetting()
                       self.delay(0.2) {
                           if self.validatioin(){
                               self.enableSubmitButton()
                           }
                       }
                   }
               }else{
                   self.disablesubmitButton()
               }
    }
    
    func enableSubmitButton()  {
        self.btnSubmit.isEnabled = true
        self.btnSubmit.alpha = 1.0
    }
    
    func disablesubmitButton()  {
        self.btnSubmit.isEnabled = false
        self.btnSubmit.alpha = 0.5
    }
    
    func upDataDataAdditionalInfoForAnimateMomment(level : String? = "" , row : String? = "" , seat : String? = "") {
        var saveArray = DataManager.sharedInstance.getAdditionalDataForAnimateMoment()
        var newModel = AdditionalInfoAnimateMoment()
        var foundIndex = -1
        for index in 0..<saveArray.count{
            let data  = saveArray[index]
            if data.event_id == self.eventItem.id ?? 0 {
                newModel = data
                foundIndex = index
                break
            }
        }
        if !(level!.isEmpty){
            newModel.level = level!
        }
        if !(row!.isEmpty){
            newModel.row = row!
        }
        if !(seat!.isEmpty){
            newModel.seatNumber = seat!
        }
        if foundIndex > -1 {
            saveArray[foundIndex] = newModel
        }else{
            newModel.event_id = self.eventItem.id!
            saveArray.append(newModel)
        }
        DataManager.sharedInstance.saveAdditionalDataForAnimateMoment(dataArray: saveArray)
    }
    
    func setupBackButton() {
        if isFromSettings {
            ivBack.image = UIImage(named: "down_arrow.pdf", in: Bundle(for: EventDetailsVC.self), with: nil)
        } else {
            ivBack.image = UIImage(named: "ic_back_arrow.pdf", in: Bundle(for: EventDetailsVC.self), with: nil)
        }
    }
    
    func setupBtnSubmit() {
        self.btnSubmit.cornerRadiusButton = 29
        self.btnSubmit.spinnerColor = .white
        if isFromSettings {
            self.btnSubmit.setTitle("SUBMIT", for: .normal)
        } else {
            self.btnSubmit.setTitle("SUBMIT", for: .normal)
        }
    }
    
    func updateGround(with index: Int) {
        
        var totalSections = 0
        //if sections are fetched from eventSectionGet api Call, then populate ground w.r.t section count else by eventItem.total_sections
        if sections.count > 0 {
            totalSections = sections.count
        } else {
            totalSections = self.eventItem.total_sections ?? 0
        }
        
        if self.eventItem.ground_type ?? "" == GroundType.ROUND.rawValue {
            self.createCircularGround(with: totalSections, selected: index)
        } else {
            self.createStageGround(with: totalSections, selected: index)
        }
    }

    deinit {
    }
    
    func configure(item:EventItem){
        switch item.eventCategory{
        case .MATCH:
            self.imgTeamEvent.isHidden = true
            self.viewRight.isHidden = !self.imgTeamEvent.isHidden
            self.viewLeft.isHidden = !self.imgTeamEvent.isHidden
            self.imgTeamLeft.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventTicketVC.self), with: nil))
            self.imgTeamRight.kf.setImage(with: URL.init(string: item.team2_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventTicketVC.self), with: nil))
            break
                                         
        case .EVENT:
            self.imgTeamEvent.isHidden = false
            self.viewRight.isHidden = !self.imgTeamEvent.isHidden
            self.viewLeft.isHidden = !self.imgTeamEvent.isHidden
            self.imgTeamEvent.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventTicketVC.self), with: nil))
            break
        case .none:
            break
        }
    }
    
    func generateNumericSections(of size: Int) -> [String] {
        var numericSections = [String]()
        
        for i in 0..<size {
            numericSections.append(String(i+1))
        }
        return numericSections
    }
    
    func updateSectionPickerView(sections: [String]) {
        self.tfSectionNumber.pickerType = .string(data: sections)
    }
    
    func getEventSectionCall() {
        self.showLoading(on: self.view, of: FrenzyColor.purpleColor)
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventSectionGet(eventId: eventItem.id ?? 0)), completion: { apiResponse in
            self.hideLoading(on: self.view)
            switch apiResponse {
        
            case .Failure(_):
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    
                    let numericSections = self.generateNumericSections(of: self.eventItem.total_sections ?? 0)
                    self.updateSectionPickerView(sections: numericSections)
                   
                    if self.isFromSettings {
                        let sectionIndex = 0
                        self.tfSectionNumber.valueDidSelected?(sectionIndex)
                        self.tfSectionNumber.text = numericSections[sectionIndex]
                    } else {
                        self.tfSectionNumber.valueDidSelected?(0)
                        self.tfSectionNumber.text = numericSections[0]
                    }
                })
                
                //
                break
            case .Success(let data):
                
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                
                if results.count > 0 {
                    self.sections = [Section]()//removeAll()
                    for result in results {
                        let data = Section().initSections(from: result)
                        
                        if self.eventItem.isAssignedevent , data.sectionTitle == "General Admission" , results.count == 1{
                            self.sections.append(data)
                        }else {
                            if data.sectionTitle != "General Admission" {
                                self.sections.append(data)
                            }else{
                                self.genralsections.append(data)
                            }
                        }
                    }
                    self.updateSectionPicker()
                    self.setUpSubmitButton()
                }
                break
            }
        })
    }
    
    func updateSectionPicker() {
        var sectionTitles = [String]()
        
        for section in sections {
            print(section.sectionTitle ?? "")
            sectionTitles.append(section.sectionTitle ?? "")
        }
        
        if sectionTitles.count > 0{
            
            self.updateSectionPickerView(sections: sectionTitles)
          //  if !isFromStartHereEvent{
                if isFromSettings {
                    let sectionIndex = getSectionId()
                    self.tfSectionNumber.valueDidSelected?(sectionIndex)
                    self.tfSectionNumber.text = sectionTitles[sectionIndex]
                    self.tfSectionNumber.stringPicker?.selectRow(sectionIndex, inComponent: 0, animated: false)
                } else {
                    if sectionTitles.count > 0 {
                        self.tfSectionNumber.valueDidSelected?(0)
                        self.tfSectionNumber.text = sectionTitles[0]
                    }
                }
                
          //  }
        }
    }
    
    func getSectionId() -> Int {
        for (index,sec) in self.sections.enumerated() {
            if sec.id ?? 0 == eventItem.my_section || sec.id ?? 0 == eventItem.section_id {
                return index
            }
        }
        return 0
    }
    
    func getSectionId_BySectionTitle()->Int?{
        for (index,sec) in self.sections.enumerated() {
            if let title =  sec.sectionTitle ,  let txt = tfSectionNumber.text  , title == txt{
                return index
            }
        }
        return nil
    }
    
    func validatioin() -> Bool {
//        if let levelText = tfSelectLevel.text, levelText.isEmpty , isVenuSelected,!isGenralSeats {
//           return false
//        }else if let rowText = tfSelectRow.text, rowText.isEmpty ,isVenuSelected,!isGenralSeats {
//             return false
//        }else if let seatText = tfSelectSeat.text, seatText.isEmpty, isVenuSelected,!isGenralSeats {
//             return false
//        }else
//
        if  let sectionText = tfSectionNumber.text, sectionText.isEmpty,isVenuSelected,!isGenralSeats{
             return false
        }else {
             return true
        }
    }
    
    func submitEventTicketCall() {
        
        if let sectionText = tfSectionNumber.text, sectionText.isEmpty, isVenuSelected, !isGenralSeats{
                showErrorToast(title: "Error", description: "Section cannot be empty.", completation: { done in
                })
        }else {
            if isGenralSeats {
                var list = DataManager.sharedInstance.getGenralEvents()
                if list.contains(self.eventItem.id ?? 0 ) == false{
                    list.append(self.eventItem.id ?? 0)
                }
                DataManager.sharedInstance.saveGenralEvents(dataArray: list)
            }else{
                var list = DataManager.sharedInstance.getGenralEvents()
                if list.contains(self.eventItem.id ?? 0 ) == true{
                    list.remove(at: list.firstIndex(of: (self.eventItem.id ?? 0))!)
                }
                DataManager.sharedInstance.saveGenralEvents(dataArray: list)
            }
            
            let eventId = eventItem.id ?? 0
            
            var selectedRow = 0
            
            if let txt = tfSectionNumber.text , sections.count > 0 ,sections[tfSectionNumber.stringPicker!.selectedRow(inComponent: 0)].sectionTitle != txt {
                guard let row = getSectionId_BySectionTitle() else{
                    return
                }
                selectedRow = row
            }else{
                selectedRow = tfSectionNumber.stringPicker!.selectedRow(inComponent: 0)
            }
            
            var sectionId = 0
            if self.isGenralSeats{
                if self.genralsections.count > 0 {
                    sectionId = self.genralsections.first!.id!
                }else if self.sections.count > 0{
                    if selectedRow < sections.count {
                        sectionId = sections[selectedRow].id ?? 0
                    }
                }
            }else{
                if selectedRow < sections.count {
                    sectionId = sections[selectedRow].id ?? 0
                }
            }
            btnSubmit.startAnimation()
            var isVenu = 0
            if isVenuSelected{
                isVenu = 1
            }
            APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventTicketUpdate(eventId: eventId, section: sectionId, level: self.selected_level ?? "A" ,row: self.selected_row ?? "0", seatNo:Int(self.selected_seat ?? "0") ?? 0, isVenu: isVenu)), completion: { apiResponse in
                self.btnSubmit.stopAnimation()
               // NotificationCenter.default.post(name: .homeFeedRefresh, object: nil)
                switch apiResponse {
                case .Failure(_):
                    break
                case .Success( _):
                    if self.isFromSettings {
                        self.eventItem.my_section = sectionId
                        self.updatePreferences(with: sectionId)
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.updatePreferences(with: sectionId)
                        self.openNextController()
                    }
                    break
                }
                
            })
        }
    }
    
    @IBAction func onFarAwaySwitchChange(_ sender: Any) {
        if (sender as! UISwitch).isOn{
            self.scrollView.isUserInteractionEnabled = false
            self.scrollView.alpha = 0.4
        }else{
            self.scrollView.isUserInteractionEnabled = true
            self.scrollView.alpha = 1.0
        }
    }
    
    func openNextController() {
//        self.showLoading(on: self.view, of: UIColor.clear)
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventDetailGet(eventId: self.eventItem.id ?? 0))) { (apiResponse) in
//            self.hideLoading(on: self.view)
            switch apiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                guard let result = data?.result as? EventItem else {
                    return
                }
                self.eventItem = result
            }
            switch self.eventItem.eventStatus {
            case .UPCOMING:
                let vc = EventCountDownVC.instantiate(fromAppStoryboard: .home)
                vc.eventItem = self.eventItem
                self.navigationController?.pushViewController(vc, animated: true)
                
            case .ONGOING:
                let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                vc.eventItem = self.eventItem
                vc.eventTitle = self.eventItem.title  ?? ""
                isGlobalDemoEvent = self.isFromStartHereEvent
                if self.isFromStartHereEvent{
                    UserDefaults.standard.setValue(self.eventItem.id ?? 0, forKey: "demo_event")
                }
                self.navigationController?.pushViewController(vc, animated: true)
                
            case .ENDED:
                let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                vc.eventItem = self.eventItem
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func updatePreferences(with sectionId: Int){
        
        let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
        var sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
        
        for (index,eventId) in eventIdArr.enumerated() {
            if eventId == self.eventItem.id ?? 0 {
                sectionIdArr[index] = sectionId
                break
            }
        }
        
        UserDefaults.standard.removeObject(forKey: kSectionIdKey)
        
        print("Saved EventID Arr: \(eventIdArr)")
        
        UserDefaults.standard.set(sectionIdArr, forKey: kSectionIdKey)
    }
    
    @IBAction func onClickSubmit(_ sender: Any) {
      
        if isVenuSelected == false {
            var list = DataManager.sharedInstance.getFarAwayEvents()
            if list.contains(eventItem.id ?? 0) == false{
                list.append(eventItem.id ?? 0)
            }
            DataManager.sharedInstance.saveFarAwayEvents(dataArray: list)
            self.submitEventTicketCall()
       
        }else{
            
            let selectedRow = tfSectionNumber.stringPicker!.selectedRow(inComponent: 0)
            if self.isGenralSeats{
                if self.genralsections.count > 0 {
                    self.validateSectionPassword(password: self.genralsections.first?.password)
                    
                }else if self.sections.count > 0{
                    if selectedRow < sections.count {
                        self.validateSectionPassword(password: sections[selectedRow].password)
                    }
                }
            }else{
                if selectedRow < sections.count {
                    self.validateSectionPassword(password: sections[selectedRow].password)
                }
            }
        }
    }
    
    func validateSectionPassword(password:String?){
       
        if let pwd = password  , !pwd.isEmpty {
            
            self.showPasswordAlert(eventItem.eventName ?? "", passowrd: pwd) { (done, cancel) in
               
                if let isPwdMatched = done , isPwdMatched{
                  
                    var list = DataManager.sharedInstance.getFarAwayEvents()
                    if let indexxx = list.firstIndex(of:  self.eventItem.id ?? 0){
                        list.remove(at: indexxx)
                        DataManager.sharedInstance.saveFarAwayEvents(dataArray: list)
                    }
                    self.submitEventTicketCall()
                }
            }
        }else{
            var list = DataManager.sharedInstance.getFarAwayEvents()
            if let indexxx = list.firstIndex(of:  eventItem.id ?? 0){
                list.remove(at: indexxx)
                DataManager.sharedInstance.saveFarAwayEvents(dataArray: list)
            }
            self.submitEventTicketCall()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite()
    }
    
    func setupViewsHeightAccordingToIphone(){
        switch(checkIphoneIs()){
        case .IPHONE_5_5S_5C,.IPHONE_6_6S_7_8:
            self.imgLeftHeightConstraint.constant = 150
            self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
            self.imgTeamLeft.cornerRadius = 75
            self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
            self.imgTeamEvent.cornerRadius = self.imgTeamLeft.cornerRadius
            break
        case .IPHONE_6PLUS_6SPLUS_7PLUS_8PLUS:
            self.imgLeftHeightConstraint.constant = 160
            self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
            self.imgTeamLeft.cornerRadius = 80
            self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
            self.imgTeamEvent.cornerRadius = self.imgTeamLeft.cornerRadius
            break
        case .IPHONE_X_XS_11_Pro,.IPHONE_XS_Max_11_Pro_Max,.IPHONE_XR_11:
            self.imgLeftHeightConstraint.constant = 175
            self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
            self.imgTeamLeft.cornerRadius = 87.5
            self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
            self.imgTeamEvent.cornerRadius = self.imgTeamLeft.cornerRadius
            break
        case .UNKNOWN:
            break
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
            self.timer?.invalidate()
    }
    
    override func onClickDismiss(_ sender: Any ) {
        if let nev = self.navigationController{
            for controller in nev.viewControllers as Array {
                if controller.isKind(of: HomeVC.self) {
                    nev.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension EventTicketVC {
    
    //MARK: Create Circle Sections
    func createCircularGround(with sections: Int, selected: Int = 0) {
        
        guard sections > 0 else {
            return
        }
        
        enableStageGroundView(false)
        removeSublayers(from: self.roundView)
        let borderWidth:CGFloat = 40.0
        let dashSpace: Double = 2
        let radius = Double(roundView.bounds.height/2)      //radius of circle
        let dashLength = ((2 * Double.pi * radius) / Double(sections)) - dashSpace     //(circumference of circle / sections) - space in between two dashes
        
        //all sections layer
        let circleLayer = CAShapeLayer()
        circleLayer.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: roundView.bounds.height, height: roundView.bounds.height)).cgPath
        circleLayer.lineWidth = borderWidth
        circleLayer.strokeColor =  FrenzyColor.lightGray.cgColor
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineJoin = .round
        circleLayer.lineDashPattern = [NSNumber(floatLiteral: dashLength), NSNumber(floatLiteral: dashSpace)]
        
        roundView.layer.addSublayer(circleLayer)
        
        //selected section layer
        let arcOrigin = CGPoint(x: roundView.bounds.width/2, y: roundView.bounds.height/2)
        let arcAngle = dashLength/radius    //angle of arc = arcLength/radius (in radians)
        let spaceAngle = dashSpace/radius   //angle of space between two sections
        let startAngle = (arcAngle + spaceAngle) * Double(selected)   //(arc angle + space angle) * num of times you want to push startangle forward
        let endAngle = startAngle + arcAngle
        let selectedLayer = CAShapeLayer()
        selectedLayer.path = UIBezierPath(arcCenter: arcOrigin, radius: CGFloat(radius), startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: true).cgPath
        selectedLayer.lineWidth = borderWidth
        selectedLayer.strokeColor =  FrenzyColor.redColor.cgColor
        selectedLayer.fillColor = UIColor.clear.cgColor
        
        roundView.layer.addSublayer(selectedLayer)
    }
    
    //MARK: Create Stage Sections
    func createStageGround(with sections: Int, selected: Int = 0) {
        guard sections > 0 else {
            return
        }
        
        enableStageGroundView(true)
        removeSublayers(from: stageSectionView)
        let sectionSpace: Double = 2        //space between two sections
        var offsetX = 4                     //value by which next section's width will increase
        let firstSectionWidth = stageSectionView.bounds.width/4     //the width of first section
        var sectionHeight = (Double(stageSectionView.bounds.height)/Double(sections)) - sectionSpace    //based on view it is contained and space
        let sectionPointX = Int((stageSectionView.bounds.width - firstSectionWidth)/2)      //the X pos of first section
        var sectionYpos: Double = sectionSpace  //the Y pos of first section
        var sectionOffsetX = 0          //the offset by which each successive section will move left
        
        //dont exceed the size of section more than 20 if there are less sections
        if sectionHeight > 20 {
            sectionHeight = 20
            offsetX = 5
        }
        
        for index in 0..<sections {
            
            var sectionWidth = Int(firstSectionWidth) + (sectionOffsetX * 2)
            
            //if section width is going out of view, then make it equal to view and cancel the effect of offset increment
            if sectionWidth > Int(stageSectionView.bounds.width) {
                sectionWidth = Int(stageSectionView.bounds.width)
                sectionOffsetX -= offsetX
            }
            
            let rectLayer = CAShapeLayer()
            rectLayer.path = UIBezierPath(roundedRect: CGRect(x: sectionPointX - sectionOffsetX, y: Int(sectionYpos), width: sectionWidth, height: Int(sectionHeight)), cornerRadius: 1).cgPath
            if index == selected {
                rectLayer.fillColor = FrenzyColor.redColor.cgColor
            } else {
                rectLayer.fillColor = FrenzyColor.lightGray.cgColor
            }
            rectLayer.lineWidth = 0.0
            stageSectionView.layer.addSublayer(rectLayer)
            sectionYpos = sectionYpos + sectionHeight + sectionSpace    //updating Y pos of section
            sectionOffsetX += offsetX       //updating offset of X for each section
        }
        
    }
    
    
    func removeSublayers(from view: UIView) {
        for layer in view.layer.sublayers ?? [CALayer]() {
            if layer.isKind(of: CAShapeLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
    }
    
    func enableStageGroundView(_ enable: Bool) {
        self.stageView.isHidden = !enable
        self.roundView.isHidden = enable
    }
}

class AdditionalInfoAnimateMoment:Codable{
    var level: String = ""
    var row: String = ""
    var seatNumber: String? = ""
    var event_id: Int = -1
}
