//
//  EventRecomendationCell.swift
//  Frenzy
//
//  Created by CP on 2/17/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class EventRecomendationCell: UICollectionViewCell {

    static let identifier = "EventRecomendationCell"
    var baseVC : BaseViewController?
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var dateView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func configure(item : EventItem) {
        if(item.eventCategory == .EVENT){
            dateView.backgroundColor = FrenzyColor.frozyKindColor
            let mainString = "\(item.eventName ?? "") \(item.eventPlace ?? "")"
            self.baseVC?.setAttributedTextForLabel(mainString:mainString , attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.lblDate.text = item.date
            self.lblDay.text = item.day
        }else{
            let mainString = "\(item.eventName ?? "") \(item.eventPlace ?? "")"//"\(item.teamOne ?? "") vs \(item.teamTwo ?? "")"
            self.baseVC?.setAttributedTextForLabel(mainString:mainString , attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            //self.baseVC?.setAttributedTextForLabel(mainString: mainString , attributedStringsArray: ["vs"], lbl: self.lblTitle, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.lblDate.text = item.date
            self.lblDay.text = item.day
            dateView.backgroundColor = FrenzyColor.greenColor
        }
       
    }
    
}
