

import UIKit
import FLAnimatedImage

class MovementSimpleCell: UITableViewCell {
    static let identifier = "MovementSimpleCell"
    @IBOutlet weak var ivImage:FLAnimatedImageView!
    @IBOutlet weak var lblTextOutlet: LinkTextView!//Nunito Reguler 12,Nunito Reguler 16
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    var baseVC:BaseViewController?
    var delighter: Delighter?
    var clickableDelighterUrlClousure : (_ url:String)->Void = {_ in}

    @IBOutlet weak var titleStackView: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblTextOutlet.textContainer.maximumNumberOfLines = 3
        self.lblTextOutlet.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(isDescription:Bool){
        guard let delight = delighter else {
            return
        }
        if(isDescription){
            self.lblTextOutlet.font = FrenzyFont.kNunitoRegular(14).font()
            if delight.description.isValidHtmlString(){
                self.lblTextOutlet.attributedText = delight.description.htmlToAttributedString
            }else{
                self.lblTextOutlet.text = delight.description.htmlToString ?? ""
                self.lblTextOutlet.textColor = FrenzyColor.purpleColor
            }
            if delight.description.contains("style=\"color:#ffffff") ,(delight.description.contains("background-color:#ffffff") || !delight.description.contains("background-color:")){
                lblTextOutlet.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }else{
            self.lblTextOutlet.font = FrenzyFont.kNunitoRegular(22).font()
            if delight.title.isValidHtmlString(){
                var newTitle = delight.title
                let title = delight.title
                if title.contains("text-align:center"){
                    newTitle = title.replacingOccurrences(of: "text-align:center", with: "text-align:center; margin-bottom: -100px;")
               
                }else if title.contains("text-align:right"){
                    newTitle = title.replacingOccurrences(of: "text-align:right", with: "text-align:right; margin-bottom: -100px;")
                
                }else if title.contains("text-align:left"){
                    newTitle = title.replacingOccurrences(of: "text-align:left", with: "text-align:left; margin-bottom: -100px;")
                }
                self.lblTextOutlet.attributedText = newTitle.htmlToAttributedString
              
            }else{
                self.lblTextOutlet.text = delight.title.htmlToString ?? ""
                self.lblTextOutlet.textColor = FrenzyColor.purpleColor
            }
            if delight.title.contains("style=\"color:#ffffff") ,(delight.title.contains("background-color:#ffffff") || !delight.title.contains("background-color:")) {
                lblTextOutlet.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    
         lblTime.text = delight.postTime ?? ""
        ivImage.kf.setImage(with: URL.init(string: delight.image ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: MovementSimpleCell.self), with: nil))
    }
    
    @objc func label2Clicked(_ sender: UITapGestureRecognizer) {
        
        if let attributedText = lblTextOutlet.attributedText {
            let text = attributedText.string
            handleDescriptionTapGesture(text: text, sender: sender)
        }else {
            guard let text = lblTextOutlet.text else { return }
            handleDescriptionTapGesture(text: text, sender: sender)
        }
    }
    
    func handleDescriptionTapGesture(text:String , sender: UITapGestureRecognizer){
       // var extractUrl = ""
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: text) else {
                continue
            }
            let url = text[range]
            print(url)
           // extractUrl = String(url)
            break
        }
    }
    
}

extension MovementSimpleCell : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        clickableDelighterUrlClousure(URL.absoluteString)
        self.openUrl(URL.absoluteString)
        return true
    }
}
