

import UIKit
import FLAnimatedImage

class MovementSponserCell: UITableViewCell {
    static let identifier = "MovementSponserCell"
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var ivImage: FLAnimatedImageView!
    var baseVC:BaseViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(image: String) {
        ivImage.kf.setImage(with: URL.init(string: image), placeholder: UIImage(named: "placeholder", in: Bundle(for: MovementSponserCell.self), with: nil))
    }
    
}
