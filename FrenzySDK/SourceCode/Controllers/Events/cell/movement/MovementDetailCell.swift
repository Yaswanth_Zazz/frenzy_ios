//
//  MovementDetailCell.swift
//  Frenzy
//
//  Created by CodingPixel on 27/02/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
import FLAnimatedImage

class MovementDetailCell: UITableViewCell {

    static let identifier = "MovementDetailCell"
    
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: LinkTextView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDescription: LinkTextView!
    @IBOutlet weak var ivImage: FLAnimatedImageView!
    @IBOutlet weak var titleStackView: UIStackView!
    var clickableDelighterUrlClousure : (_ url:String)->Void = {_ in}
    
    var delighter: Delighter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textContainer.maximumNumberOfLines = 2
        lblDescription.textContainer.maximumNumberOfLines = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(){
        guard let delight = delighter else {
            return
        }
        
        let htmlTitle = delight.title
        let htmlDescription = delight.description
        if htmlTitle.isValidHtmlString(){
            var newTitle = htmlTitle
            let title = htmlTitle
            if title.contains("text-align:center"){
                newTitle = title.replacingOccurrences(of: "text-align:center", with: "text-align:center; margin-bottom: -50px;")
           
            }else if title.contains("text-align:right"){
                newTitle = title.replacingOccurrences(of: "text-align:right", with: "text-align:right; margin-bottom: -50px;")
            
            }else if title.contains("text-align:left"){
                newTitle = title.replacingOccurrences(of: "text-align:left", with: "text-align:left; margin-bottom: -50px;")
            }
            self.lblTitle.attributedText = newTitle.htmlToAttributedString
        }else{
            lblTitle.text = htmlTitle.htmlToString ?? ""
        }
        lblTime.text = delight.postTime ?? ""
        
        if htmlDescription.isValidHtmlString(){
            var newTitle = htmlDescription
            let title = htmlDescription
            if title.contains("text-align:center"){
                newTitle = title.replacingOccurrences(of: "text-align:center", with: "text-align:center; margin-bottom: -50px;")
           
            }else if title.contains("text-align:right"){
                newTitle = title.replacingOccurrences(of: "text-align:right", with: "text-align:right; margin-bottom: -50px;")
            
            }else if title.contains("text-align:left"){
                newTitle = title.replacingOccurrences(of: "text-align:left", with: "text-align:left; margin-bottom: -50px;")
            }
            self.lblDescription.attributedText = newTitle.htmlToAttributedString
        }else{
            lblDescription.text = htmlDescription.htmlToString ?? ""
        }
       
        if htmlTitle.contains("style=\"color:#ffffff"), (htmlTitle.contains("background-color:#ffffff") || !htmlTitle.contains("background-color:")){
            lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        if htmlDescription.contains("style=\"color:#ffffff"), (htmlDescription.contains("background-color:#ffffff") || !htmlDescription.contains("background-color:")){
            lblDescription.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        ivImage.kf.setImage(with: URL.init(string: delight.image ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: MovementDetailCell.self), with: nil))
        lblTitle.delegate = self
        lblDescription.delegate = self
    }
    
    @objc func label2Clicked(_ sender: UITapGestureRecognizer) {
        
        if let attributedText = lblDescription.attributedText {
            let text = attributedText.string
            handleDescriptionTapGesture(text: text, sender: sender)
        }else {
            guard let text = lblDescription.text else { return }
            handleDescriptionTapGesture(text: text, sender: sender)
        }
    }
    
    func handleDescriptionTapGesture(text:String , sender: UITapGestureRecognizer){
       // var extractUrl = ""
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))
        
        for match in matches {
            
            guard let range = Range(match.range, in: text) else {
                continue
            }
            let url = text[range]
            print(url)
            break
        }
    }
}


extension MovementDetailCell : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == lblDescription || textView == lblTitle{
            clickableDelighterUrlClousure(URL.absoluteString)
            self.openUrl(URL.absoluteString)
            return true
        }
        return false
    }
}

extension UITableViewCell{
    
    func openUrl(_ urlLink : String){
        var link = ""
        if urlLink.hasPrefix("http://") || urlLink.hasPrefix("https://"){
            link = urlLink
        }else{
            link = "https://\(urlLink)"
        }
        
        if let url = URL(string: link), UIApplication.shared.canOpenURL(url) {//
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
