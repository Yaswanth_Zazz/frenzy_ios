//
//  MovementKindVideoCell.swift
//  Frenzy
//
//  Created by CP on 5/13/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class MovementKindVideoCell: UITableViewCell {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var liveView: UIView!
    @IBOutlet weak var labelVideoCount: UILabel!
    @IBOutlet weak var mainViewe: UIView!
    static let identifier = "MovementKindVideoCell"
    @IBOutlet weak var ivProfile:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    } 
}
