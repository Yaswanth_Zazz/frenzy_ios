//
//  FrenzyCountCell.swift
//  Frenzy
//
//  Created by CodingPixel on 18/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class FrenzyCountCell: UITableViewCell {

    static let identifier = "FrenzyCountCell"
    
    @IBOutlet weak var lblPersonCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateFrenzies(with count: Int) {
        lblPersonCount.text = String(count)
    }
    
}
