//
//  SponsorLinkCell.swift
//  Frenzy
//
//  Created by CodingPixel on 18/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class SponsorLinkCell: UITableViewCell {

    static let identifier = "SponsorLinkCell"
    
    var clickSponsorCall: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickSponsor(_ sender: Any) {
        clickSponsorCall?()
    }
    
    
}
