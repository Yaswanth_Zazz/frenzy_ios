//
//  FrenzyLogoCell.swift
//  Frenzy
//
//  Created by CodingPixel on 26/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class FrenzyLogoCell: UITableViewCell {

    static let identifier = "FrenzyLogoCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
