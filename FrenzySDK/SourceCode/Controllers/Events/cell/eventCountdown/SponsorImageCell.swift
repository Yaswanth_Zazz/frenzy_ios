//
//  SponsorImageCell.swift
//  Frenzy
//
//  Created by CodingPixel on 18/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class SponsorImageCell: UITableViewCell {

    static let identifier = "SponsorImageCell"
    
    @IBOutlet weak var ivSponsor: UIImageView!
    var clickSponsorCall: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(imgUrl: String) {
        self.ivSponsor.kf.setImage(with: URL.init(string: imgUrl), placeholder: UIImage(named: "placeholder", in: Bundle(for: SponsorImageCell.self), with:nil))
    }
    
    @IBAction func onClickSponsor(_ sender: Any) {
        self.clickSponsorCall?()
    }
    
    
}
