

import UIKit

class SearchVC:  BaseViewController, EventDismissDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tbl_events: UITableView!
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var cheerView: CheerView!
    var mDataSearch = [EventItem]()
    var isApiCallOnGoing:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cheerView.config.particle = .confetti(allowedShapes: [Particle.ConfettiShape.circle])
        self.cheerView.startLow()
        self.tfSearch.delegate = self
        self.tbl_events.register(UINib.init(nibName: EventsCell.identifier, bundle: Bundle(for: EventsCell.self)), forCellReuseIdentifier: EventsCell.identifier)
        tfSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.lblTitleHeader.text = kFindTeam
        self.view.backgroundColor = FrenzyColor.themeColor
        tbl_events.backgroundColor = .clear
        resultView.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite()
    }
    
    func removeEvventDetailView() {
        self.lblTitleHeader.text = kFindTeam
        self.containerView.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    override func onClickDismiss(_ sender: Any ) {
        if(self.containerView.isHidden){
            super.onClickDismiss(sender)
        }else{
            self.removeEvventDetailView()
        }
    }
    
    func apiCall(text:String,page:Int = 0,limit:Int = kLimitPage){
        self.isApiCallOnGoing = true
        self.tbl_events.reloadData()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.searchOrSuggestion(type: .SEARCH, searchTxt: text, page: page).encodeUFT8())) { (ApiRes) in
            switch ApiRes {
            case .Failure(let error):
                print(error as Any)
                self.resultView.isHidden = false
                self.mDataSearch.removeAll()
                self.isApiCallOnGoing = false
                self.tbl_events.reloadData()
                break
            case .Success(let data):
                let itemListNew = data?.result as? [EventItem]
                if(page == 0){
                    self.mDataSearch.removeAll()
                }
                self.mDataSearch.append(contentsOf: itemListNew ?? [EventItem]())
                self.resultView.isHidden = false
                self.isApiCallOnGoing = false
                self.tbl_events.reloadData()
                break
            }
        }
    }
}

extension SearchVC :  UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        return true
    }
}

extension SearchVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isApiCallOnGoing ? (self.mDataSearch.count + 1) : self.mDataSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.isApiCallOnGoing && (self.mDataSearch.count == indexPath.row)){
            let cell = UITableViewCell()
            let loaderView = UIActivityIndicatorView()
            loaderView.startAnimating()
            loaderView.frame = cell.contentView.frame
            cell.contentView.addSubview(loaderView)
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            
//            loaderView.snp.makeConstraints {
//                make in
//                make.edges.equalTo(cell.contentView)
//            }
            return cell
        }else{
            let cell  = tableView.dequeueReusableCell(withIdentifier: EventsCell.identifier, for: indexPath) as!  EventsCell
            let indexCell = indexPath.row
            let indexCellPath = indexPath
            let item = self.mDataSearch[indexCell]
            cell.baseVC = self
            if item.eventStatus == .ONGOING{
                cell.setliveEventView(isLive: true, item: item)
            }else{
                cell.setliveEventView(isLive: false, item: item)
            }
            
            cell.onDragOpenDetail = {
                self.tableView(self.tbl_events, didSelectRowAt: indexCellPath)
            }
            cell.selectionStyle = .none
            return  cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        self.checkUserIsBlocked(self.mDataSearch[indexPath.row].id) { (isBlocked) in
           
            if isBlocked{
               let _ = SweetAlert().showAlert("", subTitle: "You've been removed from the event by the event host for violating community guidelines, terms of use, or privacy policy.\n\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57), buttonTitleColor: .white) { isConfirm in
                }
            }else{
                
        switch self.mDataSearch[indexPath.row].subscriptionStatus {
        case .NONE:
            switch self.mDataSearch[indexPath.row].eventStatus{
                
            case .UPCOMING,.ONGOING:
                self.containerView.isHidden = false
                let feed = self.storyboard?.instantiateViewController(withIdentifier: EventDetailsVC.identifier) as! EventDetailsVC
              
                feed.eventItem = self.mDataSearch[indexPath.row]
                feed.eventDismissDelegate = self
                feed.view.frame = self.containerView.bounds
                
                if let pwd = self.mDataSearch[indexPath.row].password  , !pwd.isEmpty{
                    self.showPasswordAlert(self.mDataSearch[indexPath.row].eventName ?? "", passowrd: pwd) { (done, cancel) in
                        if let isPwdMatched = done , isPwdMatched{
                            self.addChild(feed)
                            self.containerView.addSubview(feed.view)
                        }
                    }
                }else{
                    self.addChild(feed)
                    self.containerView.addSubview(feed.view)
                }

            case .ENDED:
                
                let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                vc.eventItem = self.mDataSearch[indexPath.row]
                
                if let pwd = self.mDataSearch[indexPath.row].password  , !pwd.isEmpty{
                    self.showPasswordAlert(self.mDataSearch[indexPath.row].eventName ?? "", passowrd: pwd) { (done, cancel) in
                        if let isPwdMatched = done , isPwdMatched{
                            DispatchQueue.main.async {
                             self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                      self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            }
            
            
        case .PENDING:
            //show event ticket page
            let feed = self.storyboard?.instantiateViewController(withIdentifier: EventTicketVC.identifier) as! EventTicketVC
            feed.eventItem = self.mDataSearch[indexPath.row]
          
            if let pwd = self.mDataSearch[indexPath.row].password  , !pwd.isEmpty{
                self.showPasswordAlert(self.mDataSearch[indexPath.row].eventName ?? "", passowrd: pwd) { (done, cancel) in
                    if let isPwdMatched = done , isPwdMatched{
                        DispatchQueue.main.async {
                         self.navigationController?.pushViewController(feed, animated: true)
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                 self.navigationController?.pushViewController(feed, animated: true)
                }
            }
            
            
        case .COMPLETED:
            
            switch self.mDataSearch[indexPath.row].eventStatus {
          
            case .UPCOMING:
                //show countDown Screen
                let feed = self.storyboard?.instantiateViewController(withIdentifier: EventCountDownVC.identifier) as! EventCountDownVC
                feed.eventItem = self.mDataSearch[indexPath.row]
                self.navigationController?.pushViewController(feed, animated: true)
                  
            case .ONGOING:
                //show movement screen
                let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                vc.eventItem = self.mDataSearch[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            case .ENDED:
                //show end result screen
                let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                vc.eventItem = self.mDataSearch[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                  
            }
        }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.0) {
            if let cell = tableView.cellForRow(at: indexPath) as? EventsCell{
                cell.transform = .init(scaleX: 0.90, y: 0.90)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.0) {
            if let cell = tableView.cellForRow(at: indexPath) as? EventsCell{
                cell.transform = .identity
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) < 10{
            if(self.mDataSearch.count < kLimitPage){
                return
            }
            if(self.mDataSearch.count % kLimitPage == 0 && !self.isApiCallOnGoing){
                let page = (self.mDataSearch.count / kLimitPage)// + 1
                //MARK: Call For Search Paging
                self.apiCall(text:self.tfSearch.text!,page:page)
            }
        }
    }
}

extension SearchVC {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !(textField.text?.isEmpty ?? false) {
            self.apiCall(text: textField.text!)
        } else {
            self.resultView.isHidden = true
        }
    }
}
