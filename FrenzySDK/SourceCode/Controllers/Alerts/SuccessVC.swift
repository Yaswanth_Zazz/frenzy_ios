//
//  SuccessVC.swift
//  Frenzy
//
//  Created by CP on 1/27/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class SuccessVC: BaseViewController {

    static let identifier = "SuccessVC"
    var icAgreementAccepted:Bool = true
    @IBOutlet weak var ivAgreement:UIImageView!
    
    var popUpDismissWithAcceptence:((Bool) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true // available in IOS13
        }
    }

    @IBAction func onClickOk(_ sender:UIButton){
        self.icAgreementAccepted  = true
        if(icAgreementAccepted){
            DataManager.sharedInstance.setSuccessPopUpShown(isShown : true)
            self.dismiss(animated: true, completion: {
                //MARK: TODO if any thing need to happend on dissmis of sucess
                self.popUpDismissWithAcceptence?(true)
            })
        }else{
            self.showErrorToast(title: kError, description: kAgreementTextError) { (isDone) in
                
            }
        }
    }
    
    @IBAction func onClickAgreement(_ sender:UIButton){
        if(icAgreementAccepted){
            self.icAgreementAccepted = false
            self.ivAgreement.image = UIImage(named: "un_check_box", in: Bundle(for: SuccessVC.self), with: nil)
        }else{
            icAgreementAccepted = true
            self.ivAgreement.image = UIImage(named: "check_box", in: Bundle(for: SuccessVC.self), with: nil)
        }
    }
    

}
