//
//  FullImageVC.swift
//  Frenzy
//
//  Created by Jessie on 5/5/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
//import Kingfisher

class FullImageVC: BaseViewController {
    static let identifier = "FullImageVC"
    var mainImg : URL?
    @IBOutlet weak var imgView: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgView.kf.setImage(with: mainImg, placeholder: UIImage(named: "placeholder", in: Bundle(for: FullImageVC.self), with: nil))
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       isAllowBothOriantations = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isAllowBothOriantations = false
    }
}
