//
//  CameraPreviewVC.swift
//  Frenzy
//
//  Created by CP on 5/29/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class CameraPreviewVC: BaseViewController {

    static let identifier = "CameraPreviewVC"
    @IBOutlet weak var cheerView: UIView!
    let videoBackgroundPlay = VideoBackground()
    var videoUrl:URL? = nil
    var outputFileURL:URL!
    var image:UIImage!
    var ratio:CGFloat!
    var length:Int?
    var momentId:Int = 0
    var videoAdded:((VideoSliderModel) -> Void)?
    var onBackPressed:(() -> Void)?
    var videoUploaded: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoBackgroundPlay.willLoopVideo = true
        setVideoGravity()
        self.videoBackgroundPlay.darkness = 0
        self.videoBackgroundPlay.isMuted = false
        self.videoBackgroundPlay.play(view: cheerView, url: videoUrl ?? URL.init(string: "")!)//(view: cheerView, videoName: "frenzyVid", videoType: "mp4", isMuted: true)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.videoBackgroundPlay.isMuted = true
         self.videoBackgroundPlay.pause()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.videoBackgroundPlay.isMuted = true
        self.videoBackgroundPlay.pause()
    }
    
    func setVideoGravity() {
        switch checkIphoneIs() {
        case .IPHONE_5_5S_5C,.IPHONE_6_6S_7_8:
            self.videoBackgroundPlay.videoGravity = .resizeAspectFill
        default:
            self.videoBackgroundPlay.videoGravity = .resizeAspect
        }
    }
    
    @IBAction func onClickUpload(_ sd:UIButton){
       
        self.showLoaderFile() { loaderVc in
            
            APIManager.sharedInstance.opertationWithRequestWithFileVideoUploading(withApi: API.uploadVideo(viewer_id: "\(self.localUser?.userItem.id ?? 0)", moment_id: "\(self.momentId)", ratio: "\(self.ratio!)", length: "\(self.length ?? 0)"), image: self.image!,video:self.outputFileURL, completion: { (APIResponse) in
               
                loaderVc.dismiss(animated: true)
              
                switch APIResponse {
                    
                case .Success(let data):
                    print (data as Any)
                    self.videoAdded?(data?.result as? VideoSliderModel ?? VideoSliderModel())
                    self.navigationController?.popViewController(animated: false)
                    break
                case .Failure(let error):
                    self.showErrorToast(title: kError, description: "Video failed to upload. \(error ?? "")", completation: { confirm in
                   
                    })
                }
            }) { (progress) in
                
                loaderVc.setupProgress(progress)
               /* if progress == 1.0 {
                    self.delay(1.0) {
                     loaderVc.dismiss(animated: true)
                        self.videoUploaded?()
                        self.navigationController?.popViewController(animated: false)
                    }
                } */
                print("Upload progress \(progress)")
            }
        }
    }
    
    func autoBackBtnPress(){
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onClickBack(_ sd:UIButton) {
        self.onBackPressed?()
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onClickRetake(_ sd:UIButton) {
        self.videoBackgroundPlay.pause()
        self.navigationController?.popViewController(animated: false)
    }

}
