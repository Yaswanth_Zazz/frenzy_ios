//
//  VideoSliderCell.swift
//  Frenzy
//
//  Created by CP on 5/13/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoSliderCell: UICollectionViewCell {
    
    static let identifier = "VideoSliderCell"
    
    @IBOutlet weak var videoView:UIView!
    @IBOutlet weak var indicator:UIActivityIndicatorView!
    @IBOutlet weak var lblUpvoteCount:UILabel!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblReport:UILabel!
    @IBOutlet weak var ivUpvoted:UIImageView!
    @IBOutlet weak var ivUserName:UIImageView!
    @IBOutlet weak var ivReport:UIImageView!
    @IBOutlet weak var ivThumbnail:UIImageView!
    var player : AVPlayer?
    var playerLayer : AVPlayerLayer?
    var playNext:(() -> Void)?
    var item:VideoSliderModel? = nil
    var baseVC : BaseViewController?
 
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideo) , name: .stopVideo, object: nil)
    }
    
    func updateLabels(_ item:VideoSliderModel){
        self.item = item
        self.lblUserName.text = item.userName
        if item.upvotedCount > 0 {
           self.lblUpvoteCount.text = "\(item.upvotedCount) upvoted"
        }else{
             self.lblUpvoteCount.text = "Upvote"
        }
       
        self.ivUpvoted.tintColor = (item.isUpvoted) ? UIColor.init(hexColor: "8CC63F") : UIColor.white
        self.ivReport.tintColor = (item.isReported) ? UIColor.init(hexColor: "FBB040") : UIColor.white
        self.lblReport.text = item.isReported ? "Unreport" : "Report"
        self.lblReport.textColor = self.ivReport.tintColor
        self.ivUserName.loadGif(url: URL.init(string: item.userImage), placeholder:UIImage.init(named: "frenzy_logo", in: Bundle(for: CommentCell.self), with: nil))
        self.ivThumbnail.loadGif(url: URL.init(string: item.thumbnail_image ?? ""),placeholder: UIImage.init(named: "frenzy_logo", in: Bundle(for: CommentCell.self), with: nil))
    }
    
    func configure(_ item:VideoSliderModel){
        updateLabels(item)
        self.setVideoFileInPlayer(from: item.videoPath)
    }
    
   
    func playVideo(ratio : Double,path:String) {
        
        self.setVideoFileInPlayer(from: path)
      
        DispatchQueue.main.async {
            self.player?.isMuted = false
            self.playerLayer = AVPlayerLayer(player: self.player)
            self.playerLayer?.frame = self.videoView.bounds //self.view.bounds
            self.ivThumbnail.contentMode = .scaleAspectFill
            self.playerLayer?.videoGravity = .resizeAspectFill
//            if ratio > 1.5 {
//                self.ivThumbnail.contentMode = .scaleAspectFill
//                self.playerLayer?.videoGravity = .resizeAspectFill
//            }else{
//                self.ivThumbnail.contentMode = .scaleAspectFit
//                self.playerLayer?.videoGravity = .resizeAspect
//            }
            self.player?.volume = 1.0
            self.videoView.layer.sublayers?.removeAll()
            self.videoView.layer.addSublayer(self.playerLayer!)
            self.player?.seek(to: .zero)
            self.player?.isMuted = false
            self.player?.play()
            self.videoView.layoutSubviews()
            self.videoView.layoutIfNeeded()
        }
    }
    
    @objc func stopVideo(){
        player?.pause()
    }
    
    func setVideoFileInPlayer(from filePath: String) {
//        guard let url = URL(string: filePath) else{
//            print(" filePath url is not found")
//            return
//        }
//        print("converted Url:- \(url)")
//
//        encodeVideo(at: url) { (convertedUrl, error) in
//            if let err = error {
//                print("Error:- \(err)")
//            }else if let encodeUrl = convertedUrl{
             
                var item: AVPlayerItem!
                CacheManager.shared.getFileWith(stringUrl: filePath) { result in
                    switch result {
                    case .success(let url):
                        item = AVPlayerItem.init(url: url)
                        self.setVideoPlayer(from: item)

                        break
                    case .failure(_):
                        let link = URL.init(string: filePath)!
                        item = AVPlayerItem.init(url: link)
                        self.setVideoPlayer(from: item)

                        break
                    }
                }
           // }
      //  }
    }
    
    func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)
            
        let startDate = Date()
            
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }
            
        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        let filePath = documentsDirectory.appendingPathComponent("\(String(Date().timeIntervalSince1970))-rendered-Video.mp4")
            
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }
            
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession.timeRange = range
            
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                completionHandler?(nil, exportSession.error)
            case .cancelled:
                print("Export canceled")
                completionHandler?(nil, nil)
            case .completed:
                //Video conversion finished
                let endDate = Date()
                    
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                completionHandler?(exportSession.outputURL, nil)
                    
                default: break
            }
                
        })
    }
    
    @objc func playerDidFinishPlaying() {
        self.playNext?()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                DispatchQueue.main.async {[weak self] in
                    if newStatus == .playing || newStatus == .paused {
                        self?.indicator.isHidden = true
                        self?.ivThumbnail.isHidden = self?.indicator.isHidden ?? true
                        self?.indicator.stopAnimating()
                    } else {
                        self?.indicator.isHidden = false
                        self?.ivThumbnail.isHidden = self?.indicator.isHidden ?? false
                        self?.indicator.startAnimating()
                    }
                }
            }
        }
    }
    
    func setVideoPlayer(from item: AVPlayerItem) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        
        let pl = AVPlayer(playerItem: item)
        self.player = pl
        self.player?.isMuted = false
        self.player?.volume = 1.0
        self.player?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
    }
    
    var onReportClicked:(() -> Void)?
    
    @IBAction func onClickReport(_ sender:UIButton){
        self.onReportClicked?()
    }
    
    var onUpVoteClicked:(() -> Void)?
    @IBAction func onClickUpVote(_ sender:UIButton){
        self.onUpVoteClicked?()
    }
    
    var onProfileClicked:(() -> Void)?
    @IBAction func onClickProfile(_ sender:UIButton){
        self.onProfileClicked?()
    }
    
}
class VideoSlidAPI:Codable{
    var id:Int = 0
    var viewer_id:Int? = 0
    var viewer_name:String? = ""
    var viewer_image:String? = ""
    var moment_id:Int? = 0
    var url:String? = ""
    var default_url:String? = ""
    var ratio:String? = ""
    var length:String? = "0"
    var thumbnail:String? = ""
    var created_at:String? = ""
    var updated_at:String? = ""
    var likes:Int? = 0
    var reports:Int? = 0
    var is_liked:Int? = 0
    var is_reported:Int? = 0
    func getVidModel() -> VideoSliderModel {
        let item = VideoSliderModel()
        item.id = self.id
        item.viewer_id = self.viewer_id
        item.viewer_name = self.viewer_name
        item.viewer_image = self.viewer_image
        item.moment_id = self.moment_id
        item.url = self.url
        item.default_url = self.default_url
        item.length = Int(self.length ?? "1") ?? 1
        item.thumbnail_image = self.thumbnail
        item.ratio = Double.init(self.ratio ?? "0.0") ?? 0.0
        return item
    }
}


class VideoSliderModel:Codable {
        
    var videoPath:String {
        return url ?? ""
    }
    
    var userImage:String{
        return viewer_image ?? ""
    }
    var id:Int = 0
    var viewer_id:Int? = 0
    var viewer_name:String? = ""
    var viewer_image:String? = ""
    var moment_id:Int? = 0
    var userName:String{
        return viewer_name ?? ""
    }
    var url:String? = ""
    var default_url : String? = ""
    var ratio:Double? = 0.0
    var length:Int? = 0
    var thumbnail_image:String? = ""
    var created_at:String? = ""
    var updated_at:String? = ""
    var likes:Int? = 0
    var reports:Int? = 0
    var is_liked:Int? = 0
    var is_reported:Int? = 0
    var isUpvoted:Bool{
        return (is_liked ?? 0) > 0
    }
    var upvotedCount :Int{
        return likes ?? 0
    }
    var isReported:Bool {
        return (is_reported ?? 0) > 0
    }
    
    static func getObject(JSON:[String:Any]) -> VideoSliderModel{
        let item = VideoSliderModel()
        item.id =  JSON["id"] as? Int ?? 0
        item.viewer_id = JSON["viewer_id"] as? Int ?? 0
        item.viewer_name = JSON["viewer_name"] as? String ?? ""
        item.viewer_image = JSON["viewer_image"] as? String ?? ""
        item.moment_id = JSON["moment_id"] as? Int ?? 0
        item.url = JSON["url"] as? String ?? ""
        item.default_url = JSON["default_url"] as? String ?? ""
        item.ratio = JSON["ratio"] as? Double ?? 0.0
        item.length = JSON["length"] as? Int ?? 0
        item.thumbnail_image = JSON["thumbnail_img"] as? String ?? ""
        if(item.thumbnail_image!.isEmpty){
            item.thumbnail_image = JSON["thumbnail_image"] as? String ?? ""
        }
        item.created_at = JSON["created_at"] as? String ?? ""
        item.updated_at = JSON["updated_at"] as? String ?? ""
        return item
    }
    
    static func getObjectLike(JSON:[String:Any]) -> VideoSliderModel{
        //        [{"id":5,"viewer_id":2,"viewer_name":"Tester","viewer_image":"http:\/\/139.162.3.157\/frenzy_stable\/public\/images\/dps\/2viewer_profile.jpg","moment_id":1312,"url":"http:\/\/139.162.3.157\/frenzy_stable\/public\/videos\/video8304.mp4","ratio":null,"length":0,"thumbnail_image":"http:\/\/139.162.3.157\/frenzy_stable\/public\/videos\/thumbnail3021.jpg","created_at":"2020-05-13 07:59:51","updated_at":"2020-05-13 07:59:51","likes":0,"reports":0,"is_liked":0,"is_reported":0}]
        let item = VideoSliderModel()
        item.id =  JSON["video_id"] as? Int ?? 0
        item.viewer_id = JSON["viewer_id"] as? Int ?? 0
        if item.id == 0 {
           item.id =  Int(JSON["video_id"] as? String ?? "0")!
        }
        
        if item.viewer_id == 0 {
           item.viewer_id =  Int(JSON["viewer_id"] as? String ?? "0")!
        }
        return item
    }
    static func getData() -> [VideoSliderModel]{
        var list = [VideoSliderModel]()
        for index in 0..<5{
            let item = VideoSliderModel()
            switch index {
            case 0:
                item.id = index
                item.url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
                break
            case 1:
                
                item.id = index
                item.url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
                break
            case 2:
                item.id = index
                item.url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4"
                break
            case 3:
                item.id = index
                item.url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4"
                break
            case 4:
                item.id = index
                item.url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
                break
            default:
                item.id = index
                item.url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4"
                break
                
            }
            list.append(item)
        }
        return list
    }
}
