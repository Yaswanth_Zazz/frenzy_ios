//
//  MovemenVideosVC.swift
//  Frenzy
//
//  Created by CP on 5/13/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
import DropDown
import AVKit
import AVFoundation

class MovemenVideosVC: BaseViewController {
    
    static let identifier = "MovemenVideosVC"
    var momentId:Int? = 0
    var eventId:Int? = 0
    @IBOutlet weak var newVidesCount:UILabel!
    @IBOutlet weak var newVidesView:UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var currentFilter:UILabel!
    var currentfilter:Filter = .MOST_RECENT
    var mData = [VideoSliderModel]()//VideoSliderModel.getData()//
    var pullRefresher:UIRefreshControl = UIRefreshControl()
    var mDataNewAdding: [VideoSliderModel] = [VideoSliderModel]()
    var onDissMiss:(() -> Void)?
    var isFilter: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCL()
        // Do any additional setup after loading the view.
        self.setupNewCount(0)
        DropDown.startListeningToKeyboard()
        DropDown.appearance().setupCornerRadius(20)
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.init(hexColor: "D41C4E")
        DropDown.appearance().textFont = self.newVidesCount.font!
        DropDown.appearance().backgroundColor = .white//self.newVidesView.backgroundColor
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        IOSocket.sharedInstance.connect()
        setupReceive()
        self.apiCall()
    }
    
    func apiCall(isNeedRefresh:Bool = false,page:Int = 0,pageLimit:Int = kLimitPage){
        if(mData.count == 0 || page > 0 || isNeedRefresh){
            // API CALL HERE
            if(!self.pullRefresher.isRefreshing){
                self.pullRefresher.beginRefreshing()
            }
            var filter = FilterCase.recent
            switch self.currentfilter {
                
            case .MOST_RECENT:
                filter = .recent
            case .MOST_UPVOTE:
                filter = .voted
            case .MY:
                filter = .my
            }
            APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.getMovVideosPro(momentId: self.momentId ?? 0, type: filter, page: page, limit: pageLimit))) { (APIResponse) in
                
                switch APIResponse {
                    
                case .Success(let data):
                    if(isNeedRefresh || page == 0){
                        self.mData.removeAll()
                        self.collectionView.reloadData()
                    }
                    self.mData.append(contentsOf: data?.result as? [VideoSliderModel] ?? [VideoSliderModel]())
                    self.isFilter = false
                    if self.mData.count == 0{
                        self.onClickDismiss(UIButton())
                    }
                    else if page == 0 || isNeedRefresh{
                        self.collectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredVertically, animated: true)
                    }
                    self.collectionView.reloadData()
                    break
                case .Failure(let error):
                    self.showErrorToast(title: kError, description: "No Video Uploaded. \(error ?? "")", completation: { confirm in
                        
                    })
                    self.onClickDismiss(UIButton())
                   // self.navigationController?.popViewController(animated: true)
                    
                }
                self.isFilter = false
                self.pullRefresher.endRefreshing()
            }
            //CALL `sp_momentVideosGet`(Viewer_id INT,  Moment_id INT, Type['recent', 'voted', 'my'] ,  Page INT,  Limit INT)
            //let item = VideoSliderModel.getObject(JSON: resule.first ?? [String:Any]()) parse data like dis if coming from api same
        }
    }
    
    func setupReceive(){
       
        IOSocket.sharedInstance.stopMoment(completion: { result in
            if isLogOutCalled {
                return
            }
            if result.count > 0 {
                if self.eventId == (result.first!["event_id"] as! Int) &&  self.momentId  == (result.first!["moment_id"] as! Int){
                    self.onClickDismiss(UIButton())
                }
            }
        })
        
        IOSocket.sharedInstance.removeBlockVideo(completion: { result in
            if isLogOutCalled {
                return
            }
            if result.count > 0 {
                var eventID = "0"
                
                if let eventId = result.first?["event_id"] as? Int{
                    eventID = "\(eventId)"
                }
                else if let id = result.first?["event_id"] as? String{
                    eventID = id
                }
                
                var movementId = "0"
                if let id = result.first?["moment_id"] as? Int{
                    movementId = "\(id)"
                }
                else if let id = result.first?["moment_id"] as? String{
                    movementId = id
                }
                
                if "\(self.eventId ?? 0)" == eventID &&  "\(self.momentId ?? 0)"  == movementId{
                    self.apiCall(isNeedRefresh: true, page: 0)
                }
                
            }
        })
        
        IOSocket.sharedInstance.receiverVideo { (resule) in
            //mDataNewAdding add here and send count update match with moment id then add
            let item = VideoSliderModel.getObject(JSON: resule.first ?? [String:Any]())
            if(item.moment_id == self.momentId){
                if(self.mDataNewAdding.count == 0){
                    self.mDataNewAdding.append(item)
                }else{
                    self.mDataNewAdding.insert(item, at: 0)
                }
                self.setupNewCount(self.mDataNewAdding.count)
            }
        }
        IOSocket.sharedInstance.receiverLikeVideo { (result) in
            let item = VideoSliderModel.getObjectLike(JSON: result.first ?? [String:Any]())
            if let index = self.mData.firstIndex(where: { (items) -> Bool in
                return items.id == item.id
            }),item.viewer_id != self.localUser?.userItem.id{
                self.mData[index].likes! += 1
                if let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? VideoSliderCell{
                    cell.updateLabels(self.mData[index])
                }
            }
        }
        IOSocket.sharedInstance.receiverUnLikeVideo { (result) in
            let item = VideoSliderModel.getObjectLike(JSON: result.first ?? [String:Any]())
            if let index = self.mData.firstIndex(where: { (items) -> Bool in
                return items.id == item.id
            }),item.viewer_id != self.localUser?.userItem.id{
                if(self.mData[index].likes! > 0){
                    self.mData[index].likes! -= 1
                }
                if let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? VideoSliderCell{
                    cell.updateLabels(self.mData[index])
                }
            }
        }
    }
    
    deinit {

    }
    
    func setupNewCount(_ count :Int){
        self.newVidesView.isHidden = count == 0
        self.setAttributedTextForLabel(mainString: "\(count) \(count > 1 ? "Videos" : "Video")", attributedStringsArray: ["\(count)","\(count > 1 ? "Videos" : "Video")"], lbl: self.newVidesCount, color: [UIColor.init(hexColor: "D41C4E"),.white], attFont: [self.newVidesCount.font!,self.newVidesCount.font!])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.newVidesView.cornerRadius = self.newVidesView.frame.size.height/2
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        for (index,_) in self.mData.enumerated() {
            if let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? VideoSliderCell{
                setupEndDisplay(for: cell)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: .stopVideo, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite(isBlack: false)
    }
    
    @IBAction func onClickFilter(_ sender:UIButton){
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = [Filter.MOST_RECENT.rawValue, Filter.MOST_UPVOTE.rawValue, Filter.MY.rawValue]
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.currentfilter = Filter.allCases[index]
            self.currentFilter.text = self.currentfilter.rawValue
            self.isFilter = true
            
           // NotificationCenter.default.post(name: .stopVideo, object: nil)
            
            self.collectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredVertically, animated: true)
            
            self.apiCall(isNeedRefresh: true, page: 0)
        }
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        //        dropDown.selectRow(at: 3)
        dropDown.show()
    }
    
    @IBAction func onClickNewVides(_ sender:UIButton){
        self.mData.insert(contentsOf: self.mDataNewAdding, at: 0)
        self.mDataNewAdding.removeAll()
        self.setupNewCount(self.mDataNewAdding.count)
    }
   
    override func onClickDismiss(_ sender: Any ) {
        print("automatic Demo Video")
        self.onDissMiss?()
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: false)
        }
    }
}

enum Filter :String,CaseIterable{
    case MOST_RECENT = "Most Recent"
    case MOST_UPVOTE = "Most Upvoted"
    case MY = "Mine"
}

extension MovemenVideosVC :UICollectionViewDelegate,UICollectionViewDataSource {
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.y < 0 {
//            topConstraint.constant = -scrollView.contentOffset.y/2
//        } else {
//            topConstraint.constant = 0
//        }
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VideoSliderCell.identifier, for: indexPath) as! VideoSliderCell
        let item = self.mData[indexPath.item]
        cell.baseVC = self
        cell.configure(item)
        
        cell.onReportClicked = {
            if(item.isReported) {
                self.mData[indexPath.item].is_reported = item.isReported ? 0 : 0
                cell.updateLabels(self.mData[indexPath.item])
                APIManager.sharedInstance.opertationWithRequest(withApi: API.unReportVideo(viewer_id: "\(self.localUser?.userItem.id ?? 0)", video_id: "\(self.mData[indexPath.item].id)")) { (APIResponse) in
                    
                }
            }else{
                self.mData[indexPath.item].is_reported = item.isReported ? 1 : 1
                cell.updateLabels(self.mData[indexPath.item])
                APIManager.sharedInstance.opertationWithRequest(withApi: API.reportVideo(viewer_id: "\(self.localUser?.userItem.id ?? 0)", video_id: "\(self.mData[indexPath.item].id)")) { (APIResponse) in
                    
                }
            }
            //API CAL FOR REPORT
        }
        cell.onUpVoteClicked = {
            self.mData[indexPath.item].is_liked = item.isUpvoted ? 0 : 1
            if(self.mData[indexPath.item].isUpvoted){
                if let  _ = self.mData[indexPath.item].likes{
                    self.mData[indexPath.item].likes! += 1
                }else{
                    self.mData[indexPath.item].likes = 1
                }
                IOSocket.sharedInstance.sendVideoLikeUnLike(viewer_id: self.localUser?.userItem.id ?? 0, video_id: self.mData[indexPath.item].id, isLike: 1)
                APIManager.sharedInstance.opertationWithRequest(withApi: API.likeVideo(viewer_id: "\(self.localUser?.userItem.id ?? 0)", video_id: "\(self.mData[indexPath.item].id)")) { (APIResponse) in
                    
                }
                //API CALL FOR SAVE
            }else{
                if let _ = self.mData[indexPath.item].likes{
                    if((self.mData[indexPath.item].likes ?? 0) > 0){
                        self.mData[indexPath.item].likes! -= 1
                    }
                }else{
                    self.mData[indexPath.item].likes = 0
                }
                IOSocket.sharedInstance.sendVideoLikeUnLike(viewer_id: self.localUser?.userItem.id ?? 0, video_id: self.mData[indexPath.item].id, isLike: 0)
                APIManager.sharedInstance.opertationWithRequest(withApi: API.unLikeVideo(viewer_id: "\(self.localUser?.userItem.id ?? 0)", video_id: "\(self.mData[indexPath.item].id)")) { (APIResponse) in
                    
                }
                //API CALL FOR UNSAVE
            }
            cell.updateLabels(self.mData[indexPath.item])
        }
        cell.onProfileClicked = {
            
        }
        cell.playNext = {
            print("play next")
            if !self.isFilter {
                if((indexPath.item + 1) < self.mData.count){
                    let indexPathNew = IndexPath.init(row: indexPath.item + 1, section: 0)
                    print("indexpath: \(indexPath.row) ,\(indexPathNew.row) ")
                    collectionView.scrollToItem(at: indexPathNew, at: .centeredVertically, animated: true)
                }else{
                    
                    let indexPathNew = IndexPath.init(row: 0, section: 0)
                    if(indexPath.item == indexPathNew.item){
                        self.setupWillDisplay(for: cell, at: indexPathNew)
                    }else{
                        collectionView.scrollToItem(at: indexPathNew, at: .centeredVertically, animated: true)
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let videoCell = cell as? VideoSliderCell else {
            return
        }
        if !self.isFilter {
            self.setupWillDisplay(for: videoCell, at: indexPath)
        }
       // print("will display at index: \(indexPath.row)")
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let videoCell = cell as? VideoSliderCell else {
            return
        }
        if  !self.isFilter{
            setupEndDisplay(for: videoCell)
        }
       // print("will end display at index: \(indexPath.row)")
    }
    
    func setupWillDisplay(for cell: VideoSliderCell, at indexPath: IndexPath){
        let item = self.mData[indexPath.item]
        cell.playVideo(ratio:item.ratio ?? 1.6, path:item.videoPath)
    }
    
    func setupEndDisplay(for cell: VideoSliderCell) {
        guard let filePath1 = cell.item?.videoPath else {
            return
        }
        cell.stopVideo()
        cell.setVideoFileInPlayer(from: filePath1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize( width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
    
    func setupCL(){
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: VideoSliderCell.identifier, bundle: Bundle(for: MovemenVideosVC.self)), forCellWithReuseIdentifier: VideoSliderCell.identifier)
        let cellSize = CGSize( width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)//CGSize(width:self.collectionView.frame.size.width , height:self.collectionView.frame.size.height)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.isPagingEnabled = true
        collectionView.reloadData()
        self.pullRefresher.tintColor = .white
        
        self.pullRefresher.addTarget(self, action: #selector(self.refershEvents), for: .valueChanged)
        //        self.pullRefresher.frame.origin.y = 100
        self.pullRefresher.bounds = CGRect(x: self.pullRefresher.bounds.origin.x,
                                           y: -50,
                                           width: self.pullRefresher.bounds.size.width,
                                           height: self.pullRefresher.bounds.size.height)
        self.collectionView.refreshControl = self.pullRefresher
    }
    
    @objc func refershEvents(){
        //        self.pullRefresher.endRefreshing()
        self.pullRefresher.isHidden = false
        self.apiCall(isNeedRefresh: true, page: 0)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) < 10{
            if(self.mData.count < kLimitPage){
                return
            }
            if(self.mData.count % kLimitPage == 0){
                let page = (self.mData.count / kLimitPage)
                //MARK: Call For Upcoming Paging
                self.apiCall( page: page)
            }
        }
    }
}
