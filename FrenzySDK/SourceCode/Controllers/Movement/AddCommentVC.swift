//
//  AddCommentVC.swift
//  Frenzy
//
//  Created by CodingPixel on 17/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class AddCommentVC: BaseViewController {

    static let identifier = "AddCommentVC"
    
    @IBOutlet weak var tvComment: TextView!//UITextView!
    @IBOutlet weak var btSubmit: TransitionButton!
    
    var isAddComment = true
    var commentText = ""
    var commentId = 0       //need only while editing comment
    var delighter: Delighter?
    var commentCallback: ((Comment) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tvComment.delegate = self
        self.setupTextView()
        tvComment.smartQuotesType = .yes
        self.configureSubmitBtn(self.tvComment)
        self.view.backgroundColor = FrenzyColor.themeColor
    }
    
  
    func setupTextView() {
        tvComment.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        tvComment.text = commentText
        if !isAddComment {
            if let placeholderLabel = tvComment.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = true
            }
        }
    }
    
    func configureSubmitBtn(_ textView: UITextView) {
        if textView.text.count > 0 {
            self.btSubmit.alpha = 1.0
            self.btSubmit.backgroundColor = FrenzyColor.purpleColor
            self.btSubmit.setTitleColor(UIColor.white, for: .normal)
            self.btSubmit.isUserInteractionEnabled = true
        } else {
            self.btSubmit.alpha = 0.5
            self.btSubmit.backgroundColor = UIColor.clear
            self.btSubmit.setTitleColor(FrenzyColor.purpleColor, for: .normal)
            self.btSubmit.isUserInteractionEnabled = false
        }
    }
    
    func postCommentCall() {
        btSubmit.startAnimation()
        print("PostComment Call")
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.commentSet(delighterId: delighter?.id ?? 0, comment: tvComment.text)), completion: { apiResponse in
        
            self.btSubmit.stopAnimation()
            
            switch apiResponse {
            case .Failure(_):
                break
                
            case .Success(let data):
                
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }

                let comment = Comment().initComment(from: results.first!)
                IOSocket.sharedInstance.sendComment(withParm: results.first! as [String : AnyObject], completion: {
                self.commentCallback?(comment)
                    
                })
                self.dismiss(animated: true, completion: nil)
                break
            }
        })
    }
    
//    func sendingDictionary() -> [String: AnyObject]{
//        var data = [String: AnyObject]()
//        data["sender_id"] = user.id  as AnyObject
//        data["chat_id"] = chat_id as AnyObject
//        data["message"] = msgTextView.text as AnyObject
//        //           data["last_name"] = self.user.last_name as AnyObject
//        //           data["first_name"] = self.user.first_name as AnyObject
//        data["sender_name"] = self.user.name as AnyObject
//        data["file_type"] = filetype as AnyObject
//        data["profil_image"] = user.image as AnyObject
//        data["file_ratio"]  = filteRatio as AnyObject
//        data["date"] = self.getToday() as AnyObject
//        data["file_path"] = self.uploadPath as AnyObject
//
//
//        return data
//    }
    
    func editCommentCall() {
        btSubmit.startAnimation()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.commentUpdate(commentId: commentId, delighterId: delighter?.id ?? 0, comment: tvComment.text)), completion: { apiResponse in
            
            self.btSubmit.stopAnimation()
            
            switch apiResponse {
            case .Failure(_):
                
                break
            case .Success(let data):
                
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                
                let comment = Comment().initComment(from: results.first!)
                self.commentCallback?(comment)
                IOSocket.sharedInstance.sendCommentEdit(withParm: results.first! as [String : AnyObject], completion: {
                    
                })
                
//                if let nav = self.presentingViewController as? UINavigationController {
//                    if let vc = nav.viewControllers.last as? MovementTextVC {
//                        vc.hasPostedComment = true
//                        vc.comments.append(comment)
//                    }
//                }
                
                
                
                self.dismiss(animated: true, completion: nil)
                break
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func onClickSubmit(_ sender: Any) {
        if isAddComment{
            postCommentCall()
        } else {
            editCommentCall()
        }
    }
    

}


extension AddCommentVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        configureSubmitBtn(textView)
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = true
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = textView.text.count > 0
        }
    }
    
}
