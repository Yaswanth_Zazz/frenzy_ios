

import UIKit
import UICircularProgressRing
import FLAnimatedImage
import Kingfisher
import SwiftMessages
import AVKit
import MarqueeLabel
import AudioToolbox

var isDashboardActive = false

class MovementWaveVC: BaseViewController {
    static let identifier = "MovementWaveVC"
    @IBOutlet weak var lblTitle : MarqueeLabel!//UILabel!
    @IBOutlet weak var ivImage :FLAnimatedImageView!
    @IBOutlet weak var circulerProgress :UICircularTimerRing!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var waveView: UIView!
    @IBOutlet weak var lblMovementUrl: UILabel!
    var mData = [VideoSliderModel]()
    var movement: Movement?
    var userPermission: UserPermissionInfo?
    var timer : Timer?
    var flashSpeedTimer: Timer?
    var screenTimer: Timer?
    var vibrateTimer: Timer?
    var duration : Int?
    var totalTime = 3
    var isDismissedCalled = false
    var isFromTblSelection : Bool = false
    var isCamOpen:Bool = false

    @IBOutlet weak var whiteCircleImg: UIImageView!
    @IBOutlet weak var redCircleImage: UIImageView!
    
    @IBOutlet weak var videoosImg: UIImageView!
    
    @IBOutlet weak var lblLeading: NSLayoutConstraint!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    
    @IBOutlet weak var img1_view: UIView!
    @IBOutlet weak var img2_view: UIView!
    @IBOutlet weak var tutorialCameraView: UIView!
    @IBOutlet weak var img3_view: UIView!
    @IBOutlet weak var img4_view: UIView!
    @IBOutlet weak var labl1: UILabel!
    @IBOutlet weak var timerView: UIView!
    
    var tutorialTimer: Timer?
    var event_id = ""
    var isMomentEnd:Bool = false
  //  var sections = [Section]()
    var mySelectedSection = 0
    var mySectionIndex = -1
    var momentSectionArr = [Int]()
    var randomSectionArr = [Int]()
    var flashedSectionArr = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  getEventSectionCall()
        isDismissedCalled = false
        redCircleImage.cornerRadiusWithBorder(color: .clear, borderWidth: 0.0)
        redCircleImage.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        whiteCircleImg.cornerRadiusWithBorder(color: .white, borderWidth: 2.5)
        whiteCircleImg.backgroundColor = .clear
        endMomentsocket()
       
        self.setUpVideos(count: 0)
        self.apiCall()
        momentViewApi()
        setDemoObserver()
        
        IOSocket.sharedInstance.getVideoUpdate { (result) in
          
            if result.count > 0 {
                if let v_id = result.first!["video_id"] as? Int, let m_id = result.first!["moment_id"] as? Int, self.movement?.id == m_id{
                    for index in 0..<self.mData.count{
                        if self.mData[index].id == v_id{
                            self.mData.remove(at: index)
                            break
                        }
                    }
                    self.setUpVideos(count: self.mData.count)
                }
            }
        }
        
        IOSocket.sharedInstance.removeBlockVideo(completion: { result in
            if isLogOutCalled {
                return
            }
            if result.count > 0 {
                var eventID = "0"
                
                if let eventId = result.first?["event_id"] as? Int{
                    eventID = "\(eventId)"
                }
                else if let id = result.first?["event_id"] as? String{
                    eventID = id
                }
                
                var movementId = "0"
                if let id = result.first?["moment_id"] as? Int{
                    movementId = "\(id)"
                }
                else if let id = result.first?["moment_id"] as? String{
                    movementId = id
                }
                if "\(self.movement?.eventId ?? 0)" == eventID &&  "\(self.movement?.id ?? 0)"  == movementId{
                    print("update videos")
                    self.apiCall()
                }
            }
        })
        configureMovement()
    }
    
    @objc func startAnimation(){
        startTutorialANimation(tutorialCameraView, nil)
    }
    
    func setDemoObserver() {
        NotificationCenter.default.addObserver(forName: .exitDemoMoment, object: nil, queue: OperationQueue.main) { (notifItem) in
            print("Exit Demo Moment")
            self.timer?.invalidate()
            self.flashSpeedTimer?.invalidate()
            self.vibrateTimer?.invalidate()
            self.screenTimer?.invalidate()
            self.lblTime.text = "00:00"
            //isGlobalDemoEvent = false
            FlashFrenzy.offFlash()
        }
    }
    
    override func onMomentEnd(id: Int) {
        print("End moment")
        isMomentEnd = true
        FlashFrenzy.offFlash()
        if isDismissedCalled{
            return
        }
        if isLogOutCalled {
            return
        }
        if self.movement?.id  == id{
            if(self.isCamOpen){
                self.cameraVcPopFromMovement()
                self.delay(0.5, completion: {
                    print("onMoment Id Camera On")
                    self.onClickDismiss(UIButton())
                })
            }else{
                self.cameraVcPopFromMovement()
                print("onMoment Id Camera OFF")
                self.onClickDismiss(UIButton())
            }
        }
    }
    
    func setUpVideos(count : Int) {
        DispatchQueue.main.async {
            if count == 0 {
                self.img1_view.isHidden = true
                self.img2_view.isHidden = true
                self.img3_view.isHidden = true
                self.img4_view.isHidden = true
            }else  if count == 1 {
                self.labl1.isHidden = true
                self.img1_view.isHidden = false
                self.img1.loadGif(url: URL.init(string: self.mData.first?.thumbnail_image ?? ""))
                self.img1.accessibilityIdentifier = self.mData.first?.url
                self.img2_view.isHidden = true
                self.img3_view.isHidden = true
                self.img4_view.isHidden = true
                
                //img1.image = .
            }else  if count == 2 {
                self.labl1.isHidden = true
                self.img1_view.isHidden = false
                self.img1.loadGif(url: URL.init(string: self.mData.first?.thumbnail_image ?? ""))
                self.img1.accessibilityIdentifier = self.mData.first?.url
                self.img2_view.isHidden = false
                self.img2.loadGif(url: URL.init(string: self.mData[1].thumbnail_image ?? ""))
                self.img2.accessibilityIdentifier = self.mData[1].url
                self.img3_view.isHidden = true
                self.img4_view.isHidden = true
                
                //img1.image = .
                //img2.image = .
            }else  if count == 3 {
                self.labl1.isHidden = true
                self.img1_view.isHidden = false
                self.img1.loadGif(url: URL.init(string: self.mData.first?.thumbnail_image ?? ""))
                self.img1.accessibilityIdentifier = self.mData.first?.url
                self.img2_view.isHidden = false
                self.img2.loadGif(url: URL.init(string: self.mData[1].thumbnail_image ?? ""))
                self.img2.accessibilityIdentifier = self.mData[1].url
                self.img3_view.isHidden = false
                self.img3.loadGif(url: URL.init(string: self.mData.last?.thumbnail_image ?? ""))
                self.img3.accessibilityIdentifier = self.mData.last?.url
                self.img4_view.isHidden = true
                
                //img1.image = .
                //img2.image = .
                //img3.image = .
            }else  if count == 4 {
                self.labl1.isHidden = true
                self.img1_view.isHidden = false
                self.img1.loadGif(url: URL.init(string: self.mData.first?.thumbnail_image ?? ""))
                self.img1.accessibilityIdentifier = self.mData.first?.url
                self.img2_view.isHidden = false
                self.img2.loadGif(url: URL.init(string: self.mData[1].thumbnail_image ?? ""))
                self.img2.accessibilityIdentifier = self.mData[1].url
                self.img3_view.isHidden = false
                self.img3.loadGif(url: URL.init(string: self.mData[2].thumbnail_image ?? ""))
                self.img3.accessibilityIdentifier = self.mData[2].url
                self.img4_view.isHidden = false
                self.img4.loadGif(url: URL.init(string: self.mData.last?.thumbnail_image ?? ""))
                self.img4.accessibilityIdentifier = self.mData.last?.url
                
                //img1.image = .
                //img2.image = .
                //img3.image = .
                //img4.image = .
            }else  if count > 4 {
                self.labl1.isHidden = false
                self.img1_view.isHidden = false
                self.img1.loadGif(url: URL.init(string: self.mData.first?.thumbnail_image ?? ""))
                self.img1.accessibilityIdentifier = self.mData.first?.url
                self.img2_view.isHidden = false
                self.img2.loadGif(url: URL.init(string: self.mData[1].thumbnail_image ?? ""))
                self.img2.accessibilityIdentifier = self.mData[1].url
                self.img3_view.isHidden = false
                self.img3.loadGif(url: URL.init(string: self.mData[2].thumbnail_image ?? ""))
                self.img3.accessibilityIdentifier = self.mData[2].url
                self.img4_view.isHidden = false
                self.img4.loadGif(url: URL.init(string: self.mData[3].thumbnail_image ?? ""))
                self.img4.accessibilityIdentifier = self.mData[3].url
                
                //img1.image = .
                //img2.image = .
                //img3.image = .
                //img4.image = .
                
                self.labl1.text = "+\(count-4)"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite()
        
        if isGlobalDemoEvent {
            tutorialCameraView.isHidden = false
            startAnimation()
        }else{
            tutorialCameraView.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupCirculerProgress()
    }
    
    func endMomentsocket(){
        
        IOSocket.sharedInstance.receiverVideo { (resule) in
            //mDataNewAdding add here and send count update match with moment id then add
            let item = VideoSliderModel.getObject(JSON: resule.first ?? [String:Any]())

            if(item.moment_id == self.movement?.id){

                if(self.mData.count == 0){
                    self.mData.append(item)
                }else{
                    self.mData.insert(item, at: 0)
                }
                self.mData = self.mData.removeDuplicates()
                self.setUpVideos(count: self.mData.count)
            }
        }
    }
    
    override func appCameFromBackGround() {
        super.appCameFromBackGround()
        guard let moment = movement else {
            return
        }
        self.totalTime = moment.remainingDuration
        if !isCamOpen{
            //updateSelectedSectionFlashTime(moment: moment)
            self.restartVibrationFlash()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if(isCamOpen){
            print("camera open")
            return Void()
        }
        print("camera close")
        self.ivImage.animatedImage = nil
        self.ivImage.image = nil
        self.timer?.invalidate()
        self.screenTimer?.invalidate()
        FlashFrenzy.offFlash()
        self.vibrateTimer?.invalidate()
        self.flashSpeedTimer?.invalidate()
    }

    override func viewDidDisappear(_ animated: Bool) {
        self.tutorialTimer?.invalidate()
        if(isCamOpen){
            print("camera open")
            return Void()
        }
        print("camera close")
        self.ivImage.animatedImage = nil
        self.ivImage.image = nil
        self.timer?.invalidate()
        self.screenTimer?.invalidate()
        FlashFrenzy.offFlash()
        self.vibrateTimer?.invalidate()
        self.flashSpeedTimer?.invalidate()
    }
    
    @IBAction func onClickRecordButton(_ sender: Any) {
        self.isCamOpen = true
        FlashFrenzy.offFlash()
        let vc = CameraVC.instantiate(fromAppStoryboard: .Movement)
        vc.momentId = self.movement?.id
        vc.eventId = self.movement?.eventId
        
        vc.videoAdded = { item in
            //CALL AGAIN
            self.isCamOpen = false
            self.restartVibrationFlash()
            if(self.mData.count == 0){
                self.mData.append(item)
            }else{
                self.mData.insert(item, at: 0)
            }
            self.mData = self.mData.removeDuplicates()
            IOSocket.sharedInstance.sendVideoObject(viewer_id: self.localUser?.userItem.id ?? 0, viewer_image: self.localUser?.userItem.profile_image ?? "", viewer_name: self.localUser?.userItem.name ?? "", moment_id: self.movement?.id ?? 0, url: item.url ?? "", thumbnail_img: item.thumbnail_image ?? "", id: item.id ,default_url : item.default_url ?? "")
        }
        vc.videoUploaded = {
            self.isCamOpen = false
            self.restartVibrationFlash()
        }
        vc.onDissMiss = {
            self.isCamOpen = false
            if !self.isMomentEnd{
                self.restartVibrationFlash()
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func restartVibrationFlash(){
        
        if let moment = self.movement {
        switch moment.style {
        case .ANIMATE:
            movementFlash(with: moment, isFlashToggle: false)
            break
        case .FLASH:
            movementFlash(with: moment, isFlashToggle: true)
            break
        case .FLOW:
            movementWaveFlash(with: moment)
         //   movementFlash(with: moment, isFlashToggle: true)
            break
        case .FRENZY:
            timerView.isHidden = true
            movementFlash(with: moment, isFlashToggle: false)
            break
        case .THROB:
            movementThrobFlash(with: moment)
            break
        case .WAVE:
            movementWaveFlash(with: moment)
           // movementFlash(with: moment, isFlashToggle: false)
            break
        }
        }
    }
    
    @IBAction func onClickFrenzyMap(_ sender: Any) {
        self.openVCVideo()
    }
    
    func openVCVideo(){
        if self.mData.count > 0{
            let vc = MovemenVideosVC.instantiate(fromAppStoryboard: .Movement)
                   vc.momentId = self.movement?.id
                   vc.mData = self.mData
                   vc.eventId = self.movement?.eventId
                   self.isCamOpen = true
                   FlashFrenzy.offFlash()
                   vc.onDissMiss = {
                       self.isCamOpen = false
                        self.restartVibrationFlash()
                        self.apiCall()
                   }
                   self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.videoosImg.shake()
        }
       
    }
    
    func openVCVideoWithApiCalls(){
           self.showLoading(on: self.view, of: FrenzyColor.purpleColor)
           APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.getMovVideosPro(momentId: self.movement!.id , type: FilterCase.recent, page: 0, limit: 0))) { (APIResponse) in
               switch APIResponse {
               case .Success(let data):
                   self.hideLoading(on: self.view)
                   let arr = data?.result as? [VideoSliderModel] ?? [VideoSliderModel]()
                   if arr.count > 0 {
                       let vc = MovemenVideosVC.instantiate(fromAppStoryboard: .Movement)
                       vc.momentId = self.movement?.id
                       vc.mData = self.mData
                       vc.eventId = self.movement?.eventId
                       self.isCamOpen = true
                    vc.onDissMiss = {
                        self.isCamOpen = false
                        self.restartVibrationFlash()
                    }
                       self.navigationController?.pushViewController(vc, animated: true)
                   }else{
                       self.videoosImg.shake()
                   }
                   break
               case .Failure(_):
                   self.hideLoading(on: self.view)
                   self.videoosImg.shake()
                   break
                   
               }
               
           }
        }
    
    @IBAction func onClickViewPlay(_ sender: UIButton) {
        self.openVCVideo()
    }
    
    let vcPLayer = AVPlayerViewController()
   
    func playVideo(vidPath:String){
        let player = AVPlayer(url: URL.init(string: vidPath)!)
        
        vcPLayer.player = player
        vcPLayer.delegate = self
        self.isCamOpen = true
        self.present(vcPLayer, animated: true) {
            self.vcPLayer.player?.play()
        }
    }
    
    func momentViewApi(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.api_DelighterLinkView(event_id: event_id, control_id: "\(self.movement?.id ?? 0)", control_type: "moment", viewer_id: "\(self.localUser?.userItem.id ?? 0)" , url: "moment")) { (ApiResponse) in
            switch ApiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                print(data as Any)
            }
        }
    }
    
    func apiCall(page:Int = 0,pageLimit:Int = kLimitPage){
        let filter = FilterCase.recent
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.getMovVideosPro(momentId: self.movement?.id ?? 0, type: filter, page: page, limit: pageLimit))) { (APIResponse) in
            switch APIResponse {
                
            case .Success(let data):
                if(page == 0){
                    self.mData.removeAll()
                }
                self.mData.append(contentsOf: data?.result as? [VideoSliderModel] ?? [VideoSliderModel]())
                self.mData = self.mData.removeDuplicates()
                self.setUpVideos(count: self.mData.count)
                break
            case .Failure:
                if(page == 0){
                    self.mData.removeAll()
                }
                self.setUpVideos(count: self.mData.count)
                break
            }
        }
    }
    
    @IBAction func onClickVideoAndVideoThumbnail (_ sender:UIButton){
        self.openVCVideo()
    }
    
    func configureMovement(){
        // reset all timer
        guard let moment = movement else {
            return
        }
        lblTitle.type = .continuous
        self.lblTitle.numberOfLines = 0
        self.lblTitle.animationDelay = 0
        self.lblTitle.speed = .duration(18.0)
       
        let momentTitle = moment.title
        
        if momentTitle.isValidHtmlString() {
            print("valid Html")
            var leftSpace = ""
            var rightSpace = ""
            if moment.is_scroll == 1  , !moment.title.contains("text-align:"){
                leftSpace   = "                                                 "
                rightSpace  = "                                                 "
            }else {
                rightSpace = ""
                leftSpace = ""
            }
            let momentTitleFirst = NSMutableAttributedString(string: rightSpace, attributes: [NSAttributedString.Key.font: FrenzyFont.kNunitoSemiBold(18.0).font() , NSAttributedString.Key.foregroundColor: UIColor.init(hexColor: moment.colorTitle)])

            let momentTitleSecond = NSMutableAttributedString(string: leftSpace, attributes: [NSAttributedString.Key.font: FrenzyFont.kNunitoSemiBold(18.0).font() , NSAttributedString.Key.foregroundColor: UIColor.init(hexColor: moment.colorTitle)])
            
            var htmlTitle = NSMutableAttributedString()

            if let htmlStr = momentTitle.htmlToAttributedString {
                htmlTitle = htmlStr
            }
            momentTitleFirst.append(htmlTitle)
            momentTitleFirst.append(momentTitleSecond)
          
            self.lblTitle.attributedText =  momentTitleFirst
            self.lblMovementUrl.attributedText =  momentTitleFirst
          
            if (!momentTitle.contains("\"color:#") || momentTitle.contains("style=\"color:#000000") ), !momentTitle.contains("background-color:"){
                lblTitle.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                lblMovementUrl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            if !moment.title.contains("text-align:"){
                lblMovementUrl.textAlignment = .center
            }
            self.lblTitle.textAlignment = .center

            if moment.is_scroll == 1{
                self.lblTitle.unpauseLabel()
                self.lblTitle.restartLabel()
                lblTitle.isHidden = false
                lblMovementUrl.isHidden = true
            }else{
                self.lblTitle.pauseLabel()
                lblTitle.isHidden = true
                lblMovementUrl.isHidden = false
            }

        }else {
            
            let trimed_str = moment.title.trimmingCharacters(in: .whitespacesAndNewlines)
                .replacingOccurrences(of: "\\n{1,}", with: " ", options: .regularExpression)
           
            if moment.is_scroll == 1 {
                self.lblTitle.text = "                                \(trimed_str.htmlToString ?? "")                                "
            }else{
                self.lblTitle.text = "\(trimed_str.htmlToString ?? "")"
            }
            lblMovementUrl.text = "\(trimed_str.htmlToString ?? "")"
            self.lblTitle.textAlignment = .center
            
            if (!trimed_str.contains("\"color:#") || trimed_str.contains("style=\"color:#000000")) , !trimed_str.contains("background-color:"){
                lblTitle.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                lblMovementUrl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            if moment.is_scroll == 1{
                self.lblTitle.unpauseLabel()
                self.lblTitle.restartLabel()
                lblTitle.isHidden = false
                lblMovementUrl.isHidden = true
            }else {
                self.lblTitle.pauseLabel()
                lblTitle.isHidden = true
                lblMovementUrl.isHidden = false
            }
        }
        if isGlobalDemoEvent{
            lblTitle.textColor =  FrenzyColor.purpleColor//#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            lblMovementUrl.textColor = FrenzyColor.purpleColor//#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        self.lblTitle.labelize = false
        print("Moment Section:- \(moment.sections)")
        let sectionsString = moment.sections.components(separatedBy: ",")
        print("sectionsString count:- \(sectionsString.count)")
        if sectionsString.count > 0 {
            momentSectionArr = sectionsString.compactMap { Int($0) }
        }else{
            momentSectionArr = [Int(moment.sections) ?? 0]
        }
        if momentSectionArr.count > 0{
            self.momentSectionArr = momentSectionArr.sorted(by: { first, second in
                return first < second
            })
        }
        updateSelectedSectionFlashTime(moment: moment)
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        
        switch moment.style {
        case .ANIMATE:
            print("Moment Style: ANIMATE")
            movementFlash(with: moment, isFlashToggle: false)
            break
        case .FLASH:
            print("Moment Style: FLASH")
            movementFlash(with: moment, isFlashToggle: true)
            break
        case .FLOW:
            print("Moment Style: FLOW")
            movementWaveFlash(with: moment)
            //movementFlash(with: moment, isFlashToggle: true)
            break
        case .FRENZY:
            timerView.isHidden = true
            print("Moment Style: FRENZY")
            movementFlash(with: moment, isFlashToggle: true)//false)
            break
        case .THROB:
            print("Moment Style: THROB")
            movementThrobFlash(with: moment)
            break
        case .WAVE:
            print("Moment Style: Wave")
            movementWaveFlash(with: moment)
          //  movementFlash(with: moment, isFlashToggle: true )//false)
            break
        }
    }
    
    func updateSelectedSectionFlashTime(moment: Movement){
        if let index = self.momentSectionArr.firstIndex(of: mySelectedSection) {
            self.mySectionIndex = index
        }
        self.totalTime = moment.remainingDuration
    }
    
    func movementFlash(with movement: Movement, isFlashToggle: Bool){
        
        guard let permission = userPermission else {
            return
        }
        playVibration(canVibrate: movement.isVibration == 1 && permission.canVibrate)
        turnFlash(on: movement.isFlash == 1 && permission.canFlash, with: movement, canFlashToggle: isFlashToggle)
        configureImage(with: movement)
        configureScreen(on: permission.canScreen && movement.isScreen == 1, with: movement)
    }
    
    func movementThrobFlash(with movement: Movement){
        guard let permission = userPermission else {
            return
        }
        playVibration(canVibrate: movement.isVibration == 1 && permission.canVibrate)
        configureImage(with: movement)
        configureScreen(on: permission.canScreen && movement.isScreen == 1, with: movement)
        turnThrobFlash(on: movement.isFlash == 1 && permission.canFlash, with: movement)
    }
    
    func movementWaveFlash(with movement: Movement){
        guard let permission = userPermission else {
            return
        }
       // playVibration(canVibrate: movement.isVibration == 1 && permission.canVibrate)
        configureImage(with: movement)
        configureScreen(on: permission.canScreen && movement.isScreen == 1, with: movement)
        turnWaveFlash(on: movement.isFlash == 1 && permission.canFlash, with: movement)
    }

    func playVibration(canVibrate: Bool) {
        if canVibrate {
            if let timer = vibrateTimer{
                print("vibrate timer FIre")
                timer.fire()
            }else{
                vibrateTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.vibrationUpdateTimer), userInfo: nil, repeats: true)
            }
        }
    }
    
    func turnFlash(on flash: Bool, with movement: Movement, canFlashToggle: Bool) {
        if flash {
            FlashFrenzy.flash(for: TimeInterval(self.totalTime))
            if(movement.isStyle == 1){
                adjustFlashSpeed(for: movement, isFlashToggle: canFlashToggle)
            }
        }
    }
    
    func turnThrobFlash(on flash: Bool, with movement: Movement) {
        if flash {
            FlashFrenzy.flash(for: TimeInterval(self.totalTime))
            if(movement.isStyle == 1){
                adjustFlashBrightness(for: movement)
            }
        }
    }
    
    func turnWaveFlash(on flash: Bool, with movement: Movement) {
        if flash {
            FlashFrenzy.flash(for: TimeInterval(self.totalTime))
            if(movement.isStyle == 1){
                adjustWaveFlash(for: movement, isFlashToggle: true)
            }
        }
    }
    
    func adjustWaveFlash(for movement: Movement, isFlashToggle: Bool) {
        print("flashSpeed:-\(movement.flashSpeed)")
        if isFlashToggle {
            if let timer = self.flashSpeedTimer{
                print("flash timer FIre")
                timer.fire()
            }else{
                self.flashSpeedTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateFlashTimer), userInfo: nil, repeats: true)
            }
        }
        else {
            updateFlashTimer()
        }
    }
    
    func adjustFlashSpeed(for movement: Movement, isFlashToggle: Bool) {
        if movement.flashSpeed > 1, isFlashToggle {
            if let timer = self.flashSpeedTimer{
                print("flash timer FIre")
                timer.fire()
            }else{
                self.flashSpeedTimer = Timer.scheduledTimer(timeInterval: 1.0/Double(movement.flashSpeed), target: self, selector: #selector(self.updateFlashTimer), userInfo: nil, repeats: true)
            }
        }
        else {
            updateFlashTimer()
        }
    }
    
    func adjustFlashBrightness(for movement: Movement) {
        if movement.flashSpeed >= 1 {
            if let timer = self.flashSpeedTimer {
                print("flash timer FIre")
                timer.fire()
            }else{
                self.flashSpeedTimer = Timer.scheduledTimer(timeInterval: 1.0/Double(movement.flashSpeed), target: self, selector: #selector(self.updateFlashBrightnessTimer), userInfo: nil, repeats: true)
            }
        }
    }
    
    func configureImage(with movement: Movement) {
        if !movement.image.isEmpty && movement.isScreen == 1{
            ivImage.isHidden = false
            ivImage.kf.setImage(with: URL.init(string: movement.image), placeholder: UIImage(named: "placeholder", in: Bundle(for: MovementWaveVC.self), with: nil))
        } else {
            ivImage.isHidden = true
        }
    }
    
    func configureScreen(on changeScreen: Bool, with movement: Movement) {
        if changeScreen {
            self.view.backgroundColor = UIColor(hexColor: movement.color)
            self.parent?.view.backgroundColor = UIColor(hexColor: movement.color)
            var seconds : TimeInterval = 1
           
            switch movement.style {
            case .FLASH:
                seconds = 1
                break
            case .THROB:
                seconds = 2
                break
            default:
                seconds = 0
                break
            }
            
            if seconds  != 0 {
                if(movement.isStyle == 1){
                     DispatchQueue.main.async {
                        if let timer = self.screenTimer{
                            print("screen timer FIre")
                            timer.fire()
                        }else{
                            self.screenTimer = Timer.scheduledTimer(timeInterval: seconds, target: self, selector: #selector(self.screenUpdateTimer), userInfo: nil, repeats: true)
                        }
                    }
                }
            }
        }
    }
    
    @objc func screenUpdateTimer() {
        if(!isCamOpen){
            DispatchQueue.main.async {
                guard let moment = self.movement else {
                    return
                }
                if moment.style != .FLOW{
                    self.mainContentView.BlinkIn()
                }
//                self.parent?.view.BlinkIn()
            }
        }
    }
    
    func setupCirculerProgress(){
        self.circulerProgress.shouldShowValueText = false
        self.circulerProgress.startTimer(to: TimeInterval(totalTime)) { state in
            switch state {
            case .finished:
                print("finished")
                self.timer?.invalidate()
                self.flashSpeedTimer?.invalidate()
                self.vibrateTimer?.invalidate()
                self.screenTimer?.invalidate()
                self.lblTime.text = "00:00"
                self.isMomentEnd = true
                FlashFrenzy.offFlash()
                self.cameraVcPopFromMovement()
                self.onClickDismiss(UIButton())
                
            case .continued(_):
                break
            case .paused(_):
                break
            }
        }
    }
       
    func cameraVcPopFromMovement(){
        if let Vcs = self.navigationController?.viewControllers {
            for vc in Vcs{
                if let cameraVC = vc as? CameraVC{
                    print("exit camera Screen")
                    cameraVC.onClickDismiss(UIButton())
                    break
                }
            }
        }
    }
    
    override func onClickDismiss(_ sender: Any ) {
        if !((self.isViewLoaded && (self.view.window != nil))) || isDismissedCalled {
            print("movement Dismissed not Called")
           // self.navigationController?.popViewController(animated: true)
            return
        }
        
        print("movement Dismissed Called")
        isDismissedCalled = true
        self.timer?.invalidate()
        self.flashSpeedTimer?.invalidate()
        FlashFrenzy.offFlash()
        self.vibrateTimer?.invalidate()
        self.screenTimer?.invalidate()
        self.circulerProgress.resetTimer()
        
        if isGlobalDemoEvent , !isFromTblSelection {
            print("Manually poped")
            NotificationCenter.default.post(name: .demoDeligter, object: nil)
        }else{
            print("Automatic poped")
        }
        self.openEventScreen(id: self.movement!.id, type: "moment")
    }
    
    @objc func updateTimer() {
        self.timeFormatted(self.totalTime)
        if totalTime != 0 {
            totalTime -= 1
        }
    }
    
    @objc func updateFlashTimer() {
        if(!isCamOpen){
            let list = DataManager.sharedInstance.getFarAwayEvents()
            if let moment = movement , !list.contains(moment.eventId){
                if moment.style == .WAVE{
                    if momentSectionArr.count > 0 {
                        let rem = (moment.duration-totalTime) % (momentSectionArr.count)
                        
                        print("Reminder:\(rem)")
                        print("section Index:\(mySectionIndex)")
                        print("rem == mySectionIndex: \(rem == mySectionIndex)")
                        if rem == mySectionIndex {
                            if momentSectionArr.count == 1{
                                FlashFrenzy.toggleTorch()
                            }else{
                                FlashFrenzy.torchOn()
                            }
                            if !FlashFrenzy.isTorchOn(){
                                VibrateFrenzy.vibrate()
                            }
                        }
                        else {
                            FlashFrenzy.offFlash()
                        }
                    }else{
                        FlashFrenzy.toggleTorch()
                        if !FlashFrenzy.isTorchOn(){
                            VibrateFrenzy.vibrate()
                        }
                    }
                }
                else if let moment = movement , moment.style == .FLOW {
                    if momentSectionArr.count > 0{
                        if let randomElement = momentSectionArr.randomElement(){
                            if randomElement == mySelectedSection {
                                print("mySelectedSection Flow")
                                FlashFrenzy.toggleTorch()
                                if !FlashFrenzy.isTorchOn(){
                                    VibrateFrenzy.vibrate()
                                }
                            }else{
                                FlashFrenzy.offFlash()
                            }
                            randomSectionArr.append(randomElement)
                            momentSectionArr.remove(object: randomElement)
                            if momentSectionArr.count == 0{
                                print("All random Section Flash")
                                momentSectionArr = randomSectionArr
                                randomSectionArr = [Int]()
                            }
                        }
                    }else{
                        print("moment SectionArr  count ")
                        FlashFrenzy.toggleTorch()
                        if !FlashFrenzy.isTorchOn(){
                            VibrateFrenzy.vibrate()
                        }
                    }
                }else{
                    FlashFrenzy.toggleTorch()
                }
            }
            else{
                FlashFrenzy.toggleTorch()
            }
        }
    }
    
    @objc func vibrationUpdateTimer() {
        if(!isCamOpen){
            VibrateFrenzy.vibrate()
           // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
    
    @objc func updateFlashBrightnessTimer() {
        if(!isCamOpen){
            FlashFrenzy.toggleBrightness()
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60)
        UIView.transition(with: self.lblTime,
                          duration: 0.25,
                          options: .curveEaseIn,
                          animations: { [weak self] in
                            if(seconds < 10){
                                self?.lblTime.text = "0\(seconds)"
                            }else{
                                self?.lblTime.text = "\(seconds)"
                            }
                            if minutes < 10 {
                                self?.lblTime.text = "0\(minutes):\(self?.lblTime.text ?? "")"
                            }else{
                                self?.lblTime.text = "\(minutes):\(self?.lblTime.text ?? "")"
                            }
            }, completion: nil)
    }
}

extension MovementWaveVC: AVPlayerViewControllerDelegate {
    func playerViewController(_ playerViewController: AVPlayerViewController, willEndFullScreenPresentationWithAnimationCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.isCamOpen = false
    }
}
