
import UIKit
import FLAnimatedImage
import ActiveLabel

class MovementTextVC: BaseViewController {
    static let identifier = "MovementTextVC"
    
    // MARK: Outlet New
    @IBOutlet weak var uiCross : UIView!
    @IBOutlet weak var ivDelighter: UIImageView!
    @IBOutlet weak var lblTitle: LinkTextView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var writeCommentView: UIView!
    var isFromTimeLine : Bool?
    @IBOutlet weak var imgOriantation: UIImageView!
    @IBOutlet weak var txtViewDesc: LinkTextView!
    @IBOutlet weak var tvComment: TextView!
    @IBOutlet weak var viewOriantation: UIView!
    // MARK: variables
    
    @IBOutlet weak var descriptionStackView: UIStackView!
    @IBOutlet weak var viewCommnetCount: UIView!
    var delighter: Delighter?
    var openBase64Img = false
    var comments = [Comment]()
    @IBOutlet weak var lblCommentCount: UILabel!
    var hasPostedComment = false
    var longPressGesture: UILongPressGestureRecognizer?
    var isPotrait = true
    var isReadMore =  false
    var event_Id = ""
    var isFromTblSelection : Bool = false
    var bottomColor : String = "#FFDD1B"
    @IBOutlet weak var seperatorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableAndScrollView()
        self.setupLongPressGesture()
        self.populateData()
        self.getCommentsCall()
        self.delighterViewApi()
        IOSocket.sharedInstance.connect()
        self.receiveCommentSocket()
        tableView.prefetchDataSource = self
        tvComment.delegate  = self
        txtViewDesc.delegate = self
        txtViewDesc.isSelectable = true
        lblTitle.delegate = self
        tvComment.smartQuotesType = .yes
    }
    
    @IBAction func onClickOriantation(_ sender: Any) {
        if isPotrait {
            isPotrait = false
        }else{
            isPotrait = true
        }
    }
    
    func searchItem(id : Int ) -> Bool {
        var  isFound  = false
        self.comments.forEach { (coments) in
            if coments.id == id{
                isFound = true
            }
        }
        return isFound
    }
    
    func receiveCommentSocket() {
        IOSocket.sharedInstance.getComment(completion: { results in
            
            if results.count > 0 {
                guard let _ = DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id,
                      let _ = results.first!["user_id"] as? Int,
                      let delighterID = self.delighter?.id,
                      let commentDelighterId = results.first!["delighter_id"] as? Int else {
                    return
                }
                if delighterID == commentDelighterId {
                    let comment = Comment().initComment(from: results.first!)
                    if !self.searchItem(id: comment.id!) {
                        self.comments.insert(comment, at: self.comments.count)
                        
                        if self.comments.count > 0 {
                            self.tableView.isHidden = false
                            self.viewCommnetCount.isHidden = true
                            self.tableView.isScrollEnabled = true
                        }
                        self.tableView.reloadData()
                        DispatchQueue.main.async {
                            self.scrollToBottom()
                        }
                    }
                }
            }
        })
        
        IOSocket.sharedInstance.getCommentEdit(completion: { results in
            
            if results.count > 0 {
                guard
                    let _ = results.first!["user_id"] as? Int,
                    let delighterID = self.delighter?.id,
                    let commentDelighterId = results.first!["delighter_id"] as? Int,
                    let commentID = results.first!["id"] as? Int else {
                    return
                }
                
                if delighterID == commentDelighterId {
                    for (index,commentt) in self.comments.enumerated() {
                        if (commentt.id ?? 0) == commentID {
                            self.comments.remove(at: index)
                            let comment = Comment().initComment(from: results.first!)
                            comment.isUpdate = 1
                            comment.name = commentt.name
                            comment.profileImage =  commentt.profileImage
                            self.comments.insert(comment, at: index)
                            
                            if self.comments.count > 0 {
                                self.tableView.isHidden = false
                                self.viewCommnetCount.isHidden = true
                                self.tableView.isScrollEnabled = true
                            }
                            
                            self.tableView.reloadData()
                            DispatchQueue.main.async {
                                self.scrollToBottom()
                            }
                            break
                        }
                    }
                }
            }
            
        })
        
        IOSocket.sharedInstance.getCommentDelete(completion: { results in
            
            if results.count > 0 {
                guard let userID = DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id else {
                    return
                }
                var commentUserId = ""
                if let id = results.first!["user_id"] as? Int{
                    commentUserId = "\(id)"
                }
                
                if let id = results.first!["user_id"] as? String{
                    commentUserId = id
                }
                
                guard let delighterID = self.delighter?.id else {
                    return
                }
                
                guard let commentDelighterId = results.first!["delighter_id"] as? Int else {
                    return
                }
                guard let commentID = results.first!["id"] as? Int else {
                    return
                }
                
                if delighterID == commentDelighterId {
                    if "\(userID)" != commentUserId {
                        for (index,comment) in self.comments.enumerated() {
                            if (comment.id ?? 0) == commentID {
                                self.comments.remove(at: index)
                                self.tableView.reloadData()
                                break
                            }
                        }
                    }
                }
            }
            
        })
    }

    func delighterViewApi(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.api_DelighterLinkView(event_id: event_Id, control_id: "\(self.delighter?.id ?? 0)", control_type: "delighter", viewer_id: "\(self.localUser?.userItem.id ?? 0)" , url: "delighter")) { (ApiResponse) in
            switch ApiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                print(data as Any)
            }
        }
    }
    
    func delighterLinkViewApi(urlStr: String){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.api_DelighterLinkView(event_id: event_Id, control_id: "\(self.delighter?.id ?? 0)", control_type: "delighter_link", viewer_id: "\(self.localUser?.userItem.id ?? 0)"  , url: urlStr)) { (ApiResponse) in
            switch ApiResponse {
            case .Failure(_):
                break
            case .Success(let data):
                print(data as Any)
            }
        }
    }
    
    @IBAction func onClickFullImageView(_ sender: Any) {
        isAllowBothOriantations = true
        let vc = FullImageVC.instantiate(fromAppStoryboard: .Movement)
        vc.mainImg = URL(string: delighter!.image ?? "")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onClickAddComment(_ sender: Any) {
      
        self.tvComment.resignFirstResponder()
        
        if  !self.tvComment.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && self.tvComment.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "Write a comment..."{
            self.showLoading(on: self.view, of: FrenzyColor.grayKindColor)
            
            APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.commentSet(delighterId: delighter?.id ?? 0, comment: tvComment.text)), completion: { apiResponse in
                self.tvComment.text = ""
                if (self.tvComment.text ?? "").isEmpty{
                    self.tvComment.text =  "Write a comment..."
                    self.tvComment.textColor = UIColor.lightGray
                }
                self.tvComment.resignFirstResponder()
                self.hideLoading(on: self.view)
                switch apiResponse {
                case .Failure(_):
                    break
                case .Success(let data):
                    guard let results = data?.result as? [[String:Any]] else {
                        return
                    }
                    let comment = Comment().initComment(from: results.first!)
                    self.comments.insert(comment, at: self.comments.count)
                    
                    if self.comments.count > 0 {
                        self.tableView.isHidden = false
                        self.viewCommnetCount.isHidden = true
                        self.tableView.isScrollEnabled = true
                    }
                    IOSocket.sharedInstance.sendComment(withParm: results.first! as [String : AnyObject], completion: {
                    })
                    self.tableView.reloadData()
                    DispatchQueue.main.async {
                        self.scrollToBottom()
                    }
                    self.dismiss(animated: true, completion: nil)
                    break
                }
            })
        }
    }
    
    @IBAction func onClickCommentCount(_ sender: Any) {
        if comments.count > 0 {
            self.tableView.isHidden = false
            self.tableView.isScrollEnabled = true
            self.viewCommnetCount.isHidden = true
            self.tableView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        
        guard let delight = delighter else {
            return
        }
        self.setBackgroundColor(delight: delight)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    func setupLongPressGesture(){
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        longPressGesture?.minimumPressDuration = 0.5 // 0.5 second press
        self.tableView.addGestureRecognizer(longPressGesture!)
    }
    
    func populateData() {
        guard let delight = delighter else {
            return
        }
        self.setBackgroundColor(delight: delight)
        if openBase64Img {
            if let imageString = delight.image, imageString != "" {
                let url = URL.init(string: imageString)
                if (url?.lastPathComponent.contains(".gif"))!{
                    fullImageView.kf.setImage(with: URL(string: imageString), placeholder:UIImage(named:"placeholder" , in: Bundle(for: MovementTextVC.self), with: nil))
                    ivDelighter.kf.setImage(with: URL(string: imageString), placeholder: UIImage(named:"placeholder" , in: Bundle(for: MovementTextVC.self), with: nil))
                }else{
                    let  img = imageString.base64toImage()
                    ivDelighter.image = img
                }
            } else {
                ivDelighter.image = UIImage(named:"placeholder" , in: Bundle(for: MovementTextVC.self), with: nil)
            }
        } else {
            fullImageView.kf.setImage(with: URL(string: delight.image ?? ""), placeholder: UIImage(named:"placeholder" , in: Bundle(for: MovementTextVC.self), with: nil))
            ivDelighter.kf.setImage(with: URL(string: delight.image ?? ""), placeholder: UIImage(named:"placeholder" , in: Bundle(for: MovementTextVC.self), with: nil))
        }
        if delight.title.isValidHtmlString(){
            var title = ""
            if let color = delight.bottom_color , color.contains("000000"){
                let str = delight.title
                if delight.title.contains("style=\"color:#000000"), (!delight.title.contains("background-color:#ffffff") || !delight.title.contains("background-color:")){
                    title = str.replacingOccurrences(of: "style=\"color:#000000", with: "style=\"color:#ffffff")
                }
                else if !delight.title.contains("style=\"color:") , !delight.title.contains("background-color:"){
                    title = str.replacingOccurrences(of: "font-family:", with: "color:#ffffff;font-family:")
                }
                else{
                    title =  delight.title
                }
            }else{
                title =  delight.title
            }
            print("delight title:-\(delight.title)")
            lblTitle.attributedText = title.htmlToAttributedString
            lblTitle.isHidden = ((delight.title.htmlToAttributedString)?.length ?? 0) == 0
            descriptionStackView.spacing = title.contains("text-align:") ? -43 : 0
            
        }else{
            lblTitle.text = delight.title.htmlToString ?? ""
            lblTitle.isHidden = (delight.title.htmlToString ?? "").isEmpty
        }
        lblTime.text = delight.postTime ?? ""
        
        if delight.description.isValidHtmlString(){
            let title = manageTextBlackCOlor(delight: delight)
            let completeText: NSMutableAttributedString? = title.htmlToAttributedString //delight.description.htmlToAttributedString
            txtViewDesc.isHidden = (completeText?.length ?? 0) == 0
          
            if let htmlTxt = completeText , htmlTxt.length > 130{
                isReadMore = true
                
                let subAttriStr : NSMutableAttributedString = htmlTxt.attributedSubstring(from: NSRange(location: 0, length: 128)) as! NSMutableAttributedString
                let readMoreAtrStr = NSMutableAttributedString(string: "... read more", attributes: [NSAttributedString.Key.font: FrenzyFont.kNunitoBold(16).font() , NSAttributedString.Key.foregroundColor: FrenzyColor.purpleColor , NSAttributedString.Key.link: "https://readmore"])
                subAttriStr.append(readMoreAtrStr)
                
                self.txtViewDesc.attributedText = subAttriStr
                
            }else{
                isReadMore = false
                self.txtViewDesc.attributedText = completeText
            }
        }else{
            
            var completeText = delight.description.htmlToString ?? ""
            txtViewDesc.isHidden = completeText.isEmpty
            
            if completeText.count > 130 {
                isReadMore = true
                
                let startIndex = completeText.index(completeText.startIndex, offsetBy: 128)
                completeText = String(completeText[..<startIndex])
                
                if !completeText.lowercased().contains("read more"){
                    completeText =  completeText.appending("... read more")
                }
                self.setDescriptionAttributedTextForLabel(mainString: completeText, attributedStringsArray: ["... read more"], lbl: self.txtViewDesc, color: [FrenzyColor.purpleColor], attFont: [FrenzyFont.kNunitoBold(16).font()])
            }else {
                isReadMore = false
                if completeText.contains("read more"){
                    self.txtViewDesc.text = completeText.replacingOccurrences(of:"read more" , with: "")
                }else{
                    self.txtViewDesc.text = completeText
                }
            }
        }
        self.txtViewDesc.linkTextAttributes = [:]
    }
     
    func setBackgroundColor(delight: Delighter){
        if isGlobalDemoEvent{
            self.view.backgroundColor = UIColor(hexaString: "#FFDD1B" , alpha: 1.0)//FrenzyColor.themeColor
            self.descriptionView.backgroundColor = UIColor(hexaString: "#FFDD1B" , alpha: 1.0)//FrenzyColor.themeColor
        }else{
            self.view.backgroundColor = UIColor(hexaString: delight.bottom_color ?? "#FFDD1B" , alpha: 1.0)
            self.descriptionView.backgroundColor = UIColor(hexaString: delight.bottom_color ?? "#FFDD1B" , alpha: 1.0)
        }
    }
    
    func setDescriptionAttributedTextForLabel(mainString : String , attributedStringsArray : [String] , lbl : UITextView , color : [UIColor], attFont:[UIFont]) {
    
        let attributedString1 = NSMutableAttributedString(string: mainString, attributes: [NSAttributedString.Key.font: FrenzyFont.kNunitoRegular(16.0).font()])
        for (index,objStr) in attributedStringsArray.enumerated() {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont[index]]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(.link, value: "https://readmore", range: range1)
            attributedString1.addAttributes([NSAttributedString.Key.foregroundColor : FrenzyColor.purpleDarkColor], range: range1)
        }
     
        lbl.attributedText = attributedString1
    }
    
    func extractUrlFromDescription(input:String){
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            
            guard let range = Range(match.range, in: input) else {
                continue
            }
            let url = input[range]
            print(url)
            self.openUrl(String(url))
            break
        }
    }
        
    @IBAction func onClickReadMore(_ sender : UIButton){
        self.eventDescriptionReadeMoreOrReadLess()
    }
    
    func manageTextBlackCOlor(delight: Delighter)->String{
        var title = ""
       
        if let color = delight.bottom_color , color.contains("000000"){
            let str = delight.description
            if delight.description.contains("style=\"color:#000000"), (!delight.description.contains("background-color:#ffffff") || !delight.description.contains("background-color:")){
                title = str.replacingOccurrences(of: "style=\"color:#000000", with: "style=\"color:#ffffff")
            }
            else if !delight.description.contains("style=\"color:") , !delight.description.contains("background-color:"){
                title = str.replacingOccurrences(of: "font-family:", with: "color:#ffffff;font-family:")
            }
            else{
                title =  delight.description
            }
        }else{
            title =  delight.description
        }
        print("delight description:-\(title)")
        return title
    }
    
    func eventDescriptionReadeMoreOrReadLess(){
        guard let delight = delighter else {
            return
        }
        if isReadMore {
            isReadMore = false
            if delight.description.isValidHtmlString(){
                
                let title = manageTextBlackCOlor(delight: delight)
                let completeText : NSMutableAttributedString? = title.htmlToAttributedString//delight.description.htmlToAttributedString
                
                let readLessAtrStr = NSMutableAttributedString(string: " read less", attributes: [NSAttributedString.Key.font: FrenzyFont.kNunitoBold(16).font() , NSAttributedString.Key.foregroundColor: FrenzyColor.purpleColor , NSAttributedString.Key.link: "https://readmore"])
                completeText?.append(readLessAtrStr)
                self.txtViewDesc.attributedText = completeText
                
            }else{
                var completeText = delight.description.htmlToString ?? ""
                if !completeText.lowercased().contains("read less"){
                    completeText =  completeText.appending(" read less")
                }
                self.setDescriptionAttributedTextForLabel(mainString: completeText, attributedStringsArray: [" read less"], lbl: self.txtViewDesc, color: [FrenzyColor.purpleColor], attFont: [FrenzyFont.kNunitoBold(16).font()])
                
            }
         
        }else{
            isReadMore = true
            
            if delight.description.isValidHtmlString(){
                let title = manageTextBlackCOlor(delight: delight)
                let completeText : NSMutableAttributedString? = title.htmlToAttributedString//delight.description.htmlToAttributedString
                
                if let htmlTxt = completeText , htmlTxt.length > 130{
                    let subAttriStr : NSMutableAttributedString = htmlTxt.attributedSubstring(from: NSRange(location: 0, length: 128)) as! NSMutableAttributedString
                    let readMoreAtrStr = NSMutableAttributedString(string: "... read more", attributes: [NSAttributedString.Key.font: FrenzyFont.kNunitoBold(16).font() , NSAttributedString.Key.foregroundColor: FrenzyColor.purpleColor , NSAttributedString.Key.link : "https://readmore"])
                    subAttriStr.append(readMoreAtrStr)
                    self.txtViewDesc.attributedText = subAttriStr
                }else{
                    isReadMore = false
                    self.txtViewDesc.attributedText = completeText
                }
            }else{
                var completeText = delight.description.htmlToString ?? ""
                if completeText.count > 130{
                    let startIndex = completeText.index(completeText.startIndex, offsetBy: 128)//completeText.count-1)
                    completeText = String(completeText[..<startIndex])
                    
                    if !completeText.lowercased().contains("read more"){
                        completeText =  completeText.appending("... read more")
                    }
                    self.setDescriptionAttributedTextForLabel(mainString: completeText, attributedStringsArray: ["... read more"], lbl: self.txtViewDesc, color: [FrenzyColor.purpleColor], attFont: [FrenzyFont.kNunitoBold(16).font()])
                    
                }else{
                    if completeText.lowercased().contains("read more"){
                        self.txtViewDesc.text = completeText.replacingOccurrences(of:"read more" , with: "")
                    }else{
                        self.txtViewDesc.text = delight.description.htmlToString ?? ""
                    }
                }
            }
        }
    }
    
    func setupTableAndScrollView() {
        tableView.delegate = self
        tableView.dataSource = self
        scrollView.delegate = self
        tableView.register(UINib.init(nibName: CommentCell.identifier, bundle: Bundle(for: MovementTextVC.self)), forCellReuseIdentifier: CommentCell.identifier)
        tableView.register(UINib.init(nibName: CommentHeaderTblCell.identifier, bundle: Bundle(for: MovementTextVC.self)), forCellReuseIdentifier: CommentHeaderTblCell.identifier)
        tableViewHeight.constant = 100// self.view.frame.height - statusBarHeight - writeCommentView.bounds.height
        self.tableView.isScrollEnabled = false
        self.tableView.bounces = true
    }
    
    func getCommentsCall() {
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.commentsGet(delighterId: delighter?.id ?? 0)), completion: { apiResponse in
            
            switch apiResponse {
            case .Failure(_):
                
                break
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                
                self.comments.removeAll()
                for result in results {
                    let comment = Comment().initComment(from: result)
                    self.comments.append(comment)
                }
                if self.comments.count > 0 {
                    self.tableView.isHidden = false
                    self.viewCommnetCount.isHidden = true
                    self.tableView.isScrollEnabled = true
                }
                self.tableView.reloadData{
                self.tableView.scrollToRow(at: IndexPath(row:self.comments.count-1 , section: 0), at: .bottom, animated: false)
                }
                break
            }
        })
    }
    
    @objc func longPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == .began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                if let cell = tableView.cellForRow(at: indexPath) as? CommentCell {
                    guard let userID = DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id, let commentUserId = cell.comment?.userId else {
                        return
                    }
                    if userID == commentUserId {
                        openActionSheet(with: indexPath.row-1)
                    }
                }
            }
        }
    }
    
    func openActionSheet(with index: Int){
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let editAction = UIAlertAction(title: "Edit", style: .default, handler: { alertAction in
            self.openEditCommentVC(with: index)
        })
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { alertAction in
            self.confirmationCustomeAlert(title: "", discription: "Are you sure you want to delete the comment?", btnColor1: UIColor.colorFromRGB(0xff1a57), btnColor2: UIColor.colorFromRGB(0xffffff),btnTitle1: "YES", btnTitle2: "NO", complition: { (confirm, tag) in
                if confirm {
                    self.deletCommentCall(on: index)
                }
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(editAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openEditCommentVC(with commentIndex: Int) {
        guard let delighter = delighter else {
            return
        }
        
        let vc = AddCommentVC.instantiate(fromAppStoryboard: .Movement)
        vc.modalPresentationStyle = .fullScreen
        vc.delighter = delighter
        vc.commentText = comments[commentIndex].comment!
        vc.isAddComment = false
        vc.commentId = comments[commentIndex].id!
        vc.commentCallback = { comment in
            DispatchQueue.main.async {
                self.comments[commentIndex].comment = comment.comment!
                self.comments[commentIndex].isUpdate = comment.isUpdate!
                self.tableView.reloadData()
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func deletCommentCall(on index: Int) {
        self.showLoading(on: self.view, of: FrenzyColor.grayKindColor)
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.commentDelete(commentId: comments[index].id!, delighterId: (delighter?.id)!)), completion: { apiResponse in
            self.hideLoading(on: self.view)
            switch apiResponse {
            case .Failure(_):
                
                break
            case .Success(_):
                let deleteParams: [String: AnyObject] = ["id": self.comments[index].id as AnyObject,
                                                         "user_id": self.comments[index].userId as AnyObject,
                                                         "delighter_id" : self.comments[index].delighterId as AnyObject,
                                                         "user_type": self.comments[index].userType as AnyObject]
                IOSocket.sharedInstance.sendCommentDelete(withParm: deleteParams, completion: {
                    
                })
                
                self.comments.remove(at: index)
                if self.comments.count == 0{
                    self.tableView.isHidden = true
                    self.viewCommnetCount.isHidden = false
                }
                self.tableView.reloadData()
                break
            }
        })
    }
    
    override func onClickDismiss(_ sender: Any ) {
        isAllowBothOriantations = false
        self.writeCommentView.isUserInteractionEnabled = true
        self.viewOriantation.isHidden = true
        
        if self.fullImageView.isHidden{
            
            if self.isFromTimeLine ?? false {
                self.navigationController?.popViewController(animated: false)
            }else{
                self.openEventScreen(id: (self.delighter?.id) ?? 0, type: "delighter")
            }
            if isGlobalDemoEvent && demoDelighterNum >= 1 , !isFromTblSelection {
                NotificationCenter.default.post(name: .demoDeligter, object: nil)
            }
        }else{
            self.fullImageView.zoomOut()
        }
    }
    
    @IBAction func onClickWriteComment(_ sender: Any) {
        guard let delighter = delighter else {
            return
        }
        
        let vc = AddCommentVC.instantiate(fromAppStoryboard: .Movement)
        vc.modalPresentationStyle = .fullScreen
        vc.delighter = delighter
        vc.commentText = ""
        vc.isAddComment = true
        vc.commentCallback = { comment in
            self.comments.insert(comment, at: self.comments.count)
            DispatchQueue.main.async {
                self.tableView.reloadData(with: {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                })
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension MovementTextVC: UITableViewDelegate, UITableViewDataSource  , UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(comments.count > 0) && self.tableView.isHidden == false{
            let statusBarHeight: CGFloat = {
                var heightToReturn: CGFloat = 0.0
                     for window in UIApplication.shared.windows {
                         if let height = window.windowScene?.statusBarManager?.statusBarFrame.height, height > heightToReturn {
                             heightToReturn = height
                         }
                     }
                return heightToReturn
            }()
            tableViewHeight.constant = self.view.frame.height - statusBarHeight - writeCommentView.bounds.height
        }else{
            tableViewHeight.constant = 40
        }
        return comments.count + 1
    }
    
    //MARK:- Prefetch data in Cell
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentHeaderTblCell.identifier, for: indexPath) as! CommentHeaderTblCell
            cell.commentLbl.text = "Comments (\(comments.count))"
            self.lblCommentCount.text = "Comments (\(comments.count))"
            cell.commentButton.addTarget(self, action: #selector(self.onClickHeader), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentCell.identifier, for: indexPath) as! CommentCell
            cell.comment = comments[indexPath.row-1]
            cell.configure()
            cell.lblComment.handleURLTap { (linke) in
                self.openUrl(linke.absoluteString)
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    @objc func onClickHeader(){
        self.tableView.reloadData()
        self.tableView.isHidden = true
        self.viewCommnetCount.isHidden = false
    }
    
    func scrollToBottom()  {
        let point = CGPoint(x: 0, y: self.tableView.contentSize.height + self.tableView.contentInset.bottom - self.tableView.frame.height)
        if point.y >= 0{
            self.tableView.setContentOffset(point, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension MovementTextVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            //start scrolling table view when the scrollview have scrolled more than offsetHeight
            let offsetHeight = ivDelighter.bounds.height + descriptionView.bounds.height
            tableView.isScrollEnabled = (self.scrollView.contentOffset.y >= offsetHeight)
            self.scrollView.bounces = !tableView.isScrollEnabled
        }
        
        if scrollView == self.tableView {
            self.tableView.isScrollEnabled = (tableView.contentOffset.y > 0)
            self.scrollView.bounces = !tableView.isScrollEnabled
        }
    }
}


extension  MovementTextVC : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if textView == lblTitle {
            print("url:\(URL.absoluteString)")
            self.delighterLinkViewApi(urlStr: URL.absoluteString)
            self.openUrl(URL.absoluteString)
            return true
        }else if textView == txtViewDesc{
            if URL.absoluteString == "https://readmore"{
                eventDescriptionReadeMoreOrReadLess()
                return false
            }else{
                print("url:\(URL.absoluteString)")
                self.delighterLinkViewApi(urlStr: URL.absoluteString)
                self.openUrl(URL.absoluteString)
                return true
            }
           
        }
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == tvComment , textView.text == "Write a comment..." {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == tvComment , textView.text.isEmpty{
            textView.text =  "Write a comment..."
            textView.textColor = UIColor.lightGray
        }
    }
}

extension StringProtocol {
    subscript(_ offset: Int)                     -> Element     { self[index(startIndex, offsetBy: offset)] }
    subscript(_ range: Range<Int>)               -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: ClosedRange<Int>)         -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: PartialRangeThrough<Int>) -> SubSequence { prefix(range.upperBound.advanced(by: 1)) }
    subscript(_ range: PartialRangeUpTo<Int>)    -> SubSequence { prefix(range.upperBound) }
    subscript(_ range: PartialRangeFrom<Int>)    -> SubSequence { suffix(Swift.max(0, count-range.lowerBound)) }
}

//MARK:- UITapGestureRecognizer Extension
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> (Bool , String)? {
        
        guard let attributedText = label.attributedText else { return (false , "") }
        
        let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
        mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))
        
        // If the label have text alignment. Delete this code if label have a default (left) aligment. Possible to add the attribute in previous adding.
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        mutableStr.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedText.length))
        
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: mutableStr)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addAttribute(NSAttributedString.Key.font, value: label.font!, range: NSRange.init(location: 0, length: attributedText.length))
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        
        //print("locationOfTouchInLabel :\(locationOfTouchInLabel)")
        
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        var alignmentOffset: CGFloat! = 0.5
        switch label.textAlignment {
        case .left, .natural, .justified:
            alignmentOffset = 0.0
        case .center:
            alignmentOffset = 0.5
        case .right:
            alignmentOffset = 1.0
        @unknown default:
            break
            //fatalError()
        }
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * alignmentOffset - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * alignmentOffset - textBoundingBox.origin.y);
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        let attributeName = NSAttributedString.Key.link
        let attributeValue = label.attributedText?.attribute(attributeName, at: indexOfCharacter, effectiveRange: nil)
        
        if let value = attributeValue {
            if let url = value as? URL {
                print("Attrubuted Link: \(url)")
                return (true , url.absoluteString)
            }
        }
        return (NSLocationInRange(indexOfCharacter, targetRange) , "")
    }
    
}
