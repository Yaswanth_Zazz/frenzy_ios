
import UIKit
import AVKit

class HomeVC: BaseViewController , EventDismissDelegate {
   
    static let identifier = "HomeVC"
    
    var isOnlyShownLiveEvents : Bool = false
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tbl_events: UITableView!
    @IBOutlet weak var tempView: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var imgShimmerView: UIImageView!
    @IBOutlet weak var segmentView: TwicketSegmentedControl!
    @IBOutlet weak var noRecordFoundView: UIView!
    @IBOutlet weak var noRecordVerticalConstarints: NSLayoutConstraint!
    @IBOutlet weak var dontSeeYourEventBtn: UIButton!
    @IBOutlet weak var notEventItemDescLbl: UILabel!
    
    var mDataPast = [EventItem]()
    var mDataUpcoming = [EventItem]()
    var mDataDemoEvent = [EventItem]()
    var totalmDataUpcoming = [EventItem]()
    var mDataSuggestion = [EventItem]()
    var segementSelected:SegementOption = SegementOption.UPCOMING
    var emptyView:NoItemFoundView?
    var pullRefresher:UIRefreshControl = UIRefreshControl()
    var isfromNotification : Bool = false
    var notiData : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = FrenzyColor.themeColor
        imgShimmerView.tintColor = UIColor.white.withAlphaComponent(0.3)//FrenzyColor.themeColor.withAlphaComponent(0.7)
        self.setupTable()
        self.setupSegmentView()
        self.apiCallSuggestion()
        isDashboardActive = true
        getDemoEventsDetailsAPi()
        self.setObserver()
    }
    
    @IBAction func btnclickNoRecord(_ sender: Any) {
        if self.checkUserIsLoggedIn(){
            self.onClickNotFound()
        }
    }
    
    func setObserver() {
        NotificationCenter.default.addObserver(forName: .homeFeedRefresh, object: nil, queue: OperationQueue.main) { (notifItem) in
            if !isGlobalDemoEvent{
               // self.apiCallUpcomingOrPast(type: .UPCOMING, page: 0)
            }
        }
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        self.removeObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IOSocket.sharedInstance.connect()
        self.setupBettryIconBlackOrWhite()
        if !self.containerView.isHidden {
            removeEvventDetailView()
        }
        if refreshHomeScreen {
            if segementSelected == .ALL{
                self.apiCallForAllEvents(text: "", page: 0, limit: 1000, isFromRefresher: true)
            }else{
                self.apiCallSuggestion(isNeedToCallUpComingData: true, isFromRefresher: true)
            }
        }
        // Handle Notification
        if isfromNotification{
            if let data = notiData {
                let eventId = data["event_id"] as? String ?? "\(data["event_id"] as? Int ??  0)"
               
                if data["noti_type"] as? String ?? "" == "moment" {
                    DispatchQueue.main.async {
                        self.showMoment(data: data , notificationEventId: eventId)
                    }
                } else if data["noti_type"] as? String ?? "" == "delighter"{
                    let delighter = Delighter().initDelighter(from: data)
                    DispatchQueue.main.async {
                        if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                            if let baseVc =  lastVC  as?  BaseViewController{
                                baseVc.openDelighterVC(with: delighter, openBase64: false , eventID: eventId)
                            }
                        }
                    }
                }
            }
            isfromNotification = false
            notiData = nil
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dontSeeYourEventBtn.cornerRadiusWithBorder()
        notEventItemDescLbl.textAlignment = .center
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func setupSegmentView(){
        let titles = [SegmentItems.kUpcoming,SegmentItems.kPastEvent, SegmentItems.KAll]
        self.segmentView.setSegmentItems(titles)
        self.segmentView.delegate = self
        self.segmentView.font = FrenzyFont.kNunitoBold(16).font()//UIFont.init(name:
        self.segmentView.highlightTextColor = .white
        self.segmentView.sliderBackgroundColor = FrenzyColor.redColor
        self.segmentView.defaultTextColor = FrenzyColor.segmentUnSelectColor
        self.segmentView.isSliderShadowHidden = true
        self.segmentView.segmentsBackgroundColor = .white
    }
    
    func removeEvventDetailView() {
        self.containerView.subviews.forEach { (inerView) in
            inerView.removeFromSuperview()
        }
        self.viewSearch.isHidden = false
        var childs = self.children
        childs.removeAll()
        self.containerView.isHidden = true
    }
    
    func apiCallSuggestion(isNeedToCallUpComingData:Bool = true, isFromRefresher:Bool = false){
        if(isNeedToCallUpComingData) {
            if !isFromRefresher {
                self.segmentView.isUserInteractionEnabled = false
                self.tbl_events.isHidden = true
                self.imgShimmerView.isHidden = !self.tbl_events.isHidden
                self.imgShimmerView.startShimmeringAnimation(direction: .leftToRight)
            }
        }
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.searchOrSuggestion(type: .SUGGESTION, searchTxt: "", lat: "\(localLatitude)", lng: "\(localLongtitude)", page: 0, limit: 10))) { (ApiRes) in
            switch ApiRes {
            case .Failure(_):
                self.mDataSuggestion.removeAll()
                self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
                break
            case .Success(let data):
                let itemListNew = data?.result as? [EventItem]
                self.mDataSuggestion.removeAll()
                self.mDataSuggestion.append(contentsOf: itemListNew ?? [EventItem]())
            }
            if(isNeedToCallUpComingData){
                self.apiCallUpcomingOrPast(type:self.segementSelected,isFromRefresher: isFromRefresher)
            }else{
                self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
                if self.pullRefresher.isRefreshing {
                    self.pullRefresher.endRefreshing()
                    self.pullRefresher.isHidden = true
                }
            }
        }
    }
    
    func apiCallForAllEvents(text:String,page:Int = 0,limit:Int = kLimitPage,isFromRefresher:Bool = false){
        self.noRecordFoundView.isHidden = true
        if(page == 0){
            if !isFromRefresher {
                self.segmentView.isUserInteractionEnabled = false
                self.tbl_events.isHidden = true
                self.imgShimmerView.isHidden = !self.tbl_events.isHidden
                self.imgShimmerView.startShimmeringAnimation(direction: .leftToRight)
            }else{
                print("All coming")
            }
        }
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.searchOrSuggestion(type: .SEARCH, searchTxt: text, page: page).encodeUFT8())) { (ApiRes) in
            switch ApiRes {
            case .Failure(let error):
                print(error as Any)
                self.totalmDataUpcoming.removeAll()
                self.mDataUpcoming.removeAll()
                self.mDataPast.removeAll()
                self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
                break
                
            case .Success(let data):
                let itemListNew = data?.result as? [EventItem]
                if(page == 0){
                    self.mDataUpcoming.removeAll()
                }
                self.mDataUpcoming.append(contentsOf: itemListNew ?? [EventItem]())
                break
            }
            
            self.totalmDataUpcoming =  self.mDataUpcoming
            if !isFromRefresher {
                self.segmentView.isUserInteractionEnabled = true
                self.tbl_events.isHidden = false
                self.imgShimmerView.isHidden = !self.tbl_events.isHidden
                self.imgShimmerView.stopShimmeringAnimation()
            }
            if self.pullRefresher.isRefreshing {
                self.pullRefresher.endRefreshing()
                self.pullRefresher.isHidden = true
            }
            self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
        }
    }

    func apiCallUpcomingOrPast(type:SegementOption,page:Int = 0,limit:Int = kLimitPage, isFromRefresher:Bool = false){
        self.noRecordFoundView.isHidden = true
        if(page == 0){
            if !isFromRefresher {
                self.segmentView.isUserInteractionEnabled = false
                self.tbl_events.isHidden = true
                self.imgShimmerView.isHidden = !self.tbl_events.isHidden
                self.imgShimmerView.startShimmeringAnimation(direction: .leftToRight)
            }
        }
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventUpcomingOrPast(type: type,lat: "\(localLatitude)", lng: "\(localLongtitude)", page: page, limit: limit))) { (ApiRes) in
            
            switch ApiRes {
            case.Failure(_):
                self.totalmDataUpcoming.removeAll()
                self.mDataUpcoming.removeAll()
                self.mDataPast.removeAll()
                self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
                if type == .UPCOMING {
                    if let demeEVent =  self.mDataDemoEvent.first , self.mDataUpcoming.count == 1 , self.mDataUpcoming.contains(demeEVent){
                    self.saveEventPreference()
                
                    }else if self.mDataUpcoming.count == 0{
                    self.saveEventPreference()
                    }
                }
            case .Success(let data):
                let itemListNew = data?.result as? [EventItem]
                switch type {
                case .UPCOMING:
                    if(page == 0){
                        self.mDataUpcoming.removeAll()
                    }
                    self.mDataUpcoming.append(contentsOf: itemListNew ?? [EventItem]())
                    self.addDemoEventIntoUpcomingData()
                    self.saveEventPreference(isJoinded: self.mDataUpcoming.filter({ (itemsPost) -> Bool in
                        return (itemsPost.is_joined ?? 0) > 0
                    }).count > 0)
                    break
                case .PAST_EVENT:
                    if(page == 0){
                        self.mDataPast.removeAll()
                    }
                    self.mDataPast.append(contentsOf: itemListNew ?? [EventItem]())
                    break
                case .ALL:
                    break
                }
            }
            self.totalmDataUpcoming =  self.mDataUpcoming
            if !isFromRefresher {
                self.segmentView.isUserInteractionEnabled = true
                self.tbl_events.isHidden = false
                self.imgShimmerView.isHidden = !self.tbl_events.isHidden
                self.imgShimmerView.stopShimmeringAnimation()
            }
            if self.pullRefresher.isRefreshing {
                self.pullRefresher.endRefreshing()
                self.pullRefresher.isHidden = true
            }
            self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
        }
        
    }
    
    func getDemoEventsDetailsAPi(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.demoEventDetails(viewer_id: "\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0)")) { (ApiResponse) in
            switch ApiResponse {
            case .Failure(let error):
                self.showErrorToast(description:error ?? kError) { (isDone) in
                    
                }
            case .Success(let data):
                
                if let itemListNew = data?.result as? [EventItem] {
                    print("Details Demo Event Response:\(itemListNew)")
                    self.mDataDemoEvent = itemListNew
                    self.onUpdateLiveEvent(isShowLiveEvent: self.isOnlyShownLiveEvents)
                }
            }
        }
    }
    
    func saveEventPreference(isJoinded:Bool = true) {
        
        UserDefaults.standard.removeObject(forKey: kEventIdKey)
        UserDefaults.standard.removeObject(forKey: kSectionIdKey)
        UserDefaults.standard.removeObject(forKey: kScreenKey)
        UserDefaults.standard.removeObject(forKey: kFlashKey)
        UserDefaults.standard.removeObject(forKey: kVibrateKey)
        
        var eventIdArr = [Int]()
        var sectionIdArr = [Int]()
        var isFlashArr = [Bool]()
        var isScreenArr = [Bool]()
        var isVibrateArr = [Bool]()
        
        //  if(isJoinded){
        
        for event in mDataUpcoming {
            
            switch event.subscriptionStatus{
            
            case .COMPLETED:
                
                switch event.eventStatus {
                case .ONGOING:
                    eventIdArr.append(event.id ?? 0)
                    sectionIdArr.append(event.my_section ?? 0)
                    isFlashArr.append(event.canFlash)
                    isScreenArr.append(event.canChangeScreen)
                    isVibrateArr.append(event.canVibrate)
                    
                case .UPCOMING:
                    eventIdArr.append(event.id ?? 0)
                    sectionIdArr.append(event.my_section ?? 0)
                    isFlashArr.append(event.canFlash)
                    isScreenArr.append(event.canChangeScreen)
                    isVibrateArr.append(event.canVibrate)
                    
                default:
                    print("")
                }
            default:
                print("Event not joined")
            }
        }
        //  }
        
        print("Saved EventID Arr: \(eventIdArr)")
        
        UserDefaults.standard.set(eventIdArr, forKey: kEventIdKey)
        UserDefaults.standard.set(sectionIdArr, forKey: kSectionIdKey)
        UserDefaults.standard.set(isFlashArr, forKey: kFlashKey)
        UserDefaults.standard.set(isScreenArr, forKey: kScreenKey)
        UserDefaults.standard.set(isVibrateArr, forKey: kVibrateKey)
        
        UserDefaults.standard.synchronize()
    }
    
}

extension HomeVC : TwicketSegmentedControlDelegate,NoItemFoundViewDelegate {
    
    func onClickNotFound() {
        let searchVC = SearchVC.instantiate(fromAppStoryboard: .home)
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func didSelect(_ segmentIndex: Int) {
        if self.checkUserIsLoggedIn(){
            switch segmentIndex {
            case 0:
                segementSelected = .UPCOMING
                self.onUpdateLiveEvent(isShowLiveEvent: isOnlyShownLiveEvents)
                if(self.mDataDemoEvent.count == 0){
                    self.getDemoEventsDetailsAPi()
                }
                self.apiCallUpcomingOrPast(type: .UPCOMING, page: 0)
                //            }
                break
            case 1:
                segementSelected = .PAST_EVENT
                self.onUpdateLiveEvent(isShowLiveEvent: isOnlyShownLiveEvents)
                //            if(self.mDataPast.count == 0){
                self.apiCallUpcomingOrPast(type: .PAST_EVENT, page: 0)
                //            }
                break
            case 2:
                segementSelected = .ALL
                self.onUpdateLiveEvent(isShowLiveEvent: isOnlyShownLiveEvents)
                //            if(self.mDataPast.count == 0){
                self.apiCallForAllEvents(text: "", page: 0, limit: 1000)
                //            }
                break
            default:
                segementSelected = .UPCOMING
                self.onUpdateLiveEvent(isShowLiveEvent: isOnlyShownLiveEvents)
                if(self.mDataUpcoming.count == 0){
                    self.apiCallUpcomingOrPast(type: .UPCOMING, page: 0)
                }
                break
            }
        }
    }
}

extension HomeVC : LiveEventDelegate{
    
    func onUpdateLiveEvent(isShowLiveEvent: Bool) {
        if segementSelected != .PAST_EVENT{
            isOnlyShownLiveEvents = isShowLiveEvent
            
            if isShowLiveEvent {
                self.mDataUpcoming.removeAll()
                for event in self.totalmDataUpcoming{
                    if event.eventStatus == .ONGOING{
                        self.mDataUpcoming.append(event)
                    }
                }
                addDemoEventIntoUpcomingData(isFromLive: true)
            }else{
                self.mDataUpcoming = self.totalmDataUpcoming
                addDemoEventIntoUpcomingData()
            }
            if self.mDataUpcoming.count == 0{
                self.noRecordFoundView.isHidden = false
            }else{
                self.noRecordFoundView.isHidden = true
            }
            
            if mDataSuggestion.count > 0 {
                self.noRecordVerticalConstarints.constant = 160
            }else{
                self.noRecordVerticalConstarints.constant = 0
            }
        }else{
            self.noRecordVerticalConstarints.constant = 0
            if self.mDataPast.count == 0{
                self.noRecordFoundView.isHidden = false
            }else{
                self.noRecordFoundView.isHidden = true
            }
        }
        self.tbl_events.reloadData()
    }
    
    func addDemoEventIntoUpcomingData(isFromLive: Bool = false){
        if mDataDemoEvent.count > 0 , let first = mDataDemoEvent.first , segementSelected == .UPCOMING{
            if !self.mDataUpcoming.contains(first){
                if isFromLive{
                    if first.eventStatus == .ONGOING{
                        self.mDataUpcoming.insert(first, at: 0)
                    }
                }else{
                    self.mDataUpcoming.insert(first, at: 0)
                }
            }
        }
    }
    
    func clearDemoDelighterAndMoment(_ eventId: Int?){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.clearDemoEvent(event_id: "\(eventId ?? 0)"), completion: { apiResponse in
            
            switch apiResponse {
            case .Failure(_):
               // print("\(error)")
                break
                
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                print(results)
                //if isGlobalDemoEvent {
                isGlobalDemoEvent = false
                isDemoEventClear = false
                demoDelighterNum = 1
                demoEventId = 0
                demoEventMomentType = .delighter
                isMomentApiCalled = false
                isFirstDelighterApiCalled = false
                isSecondDelighterApiCalled = false
                isThirdDelighterApiCalled = false
                
                var eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                if let id = eventId ,eventIdArr.contains(id){
                    for (index , savedId) in eventIdArr.enumerated(){
                        if savedId == id{
                            eventIdArr.remove(at: index)
                            break
                        }
                    }
                    UserDefaults.standard.set(eventIdArr, forKey: kEventIdKey)
                }
                //  }
                break
            }
        })
    }

}

extension HomeVC :  UITableViewDataSource,UITableViewDelegate{
    
    @objc func refershEvents(){
        if self.checkUserIsLoggedIn(){
            self.pullRefresher.isHidden = false
            if segementSelected == .ALL{
                self.apiCallForAllEvents(text: "", page: 0, limit: 1000, isFromRefresher: true)
            }else{
                self.apiCallSuggestion(isNeedToCallUpComingData: true, isFromRefresher: true)
                if(self.mDataDemoEvent.count == 0){
                    self.getDemoEventsDetailsAPi()
                }
            }
        }
    }
    
    func setupTable(){
        self.tbl_events.dataSource = self
        self.tbl_events.delegate = self
        self.tbl_events.register(UINib.init(nibName: EventsCell.identifier, bundle: Bundle(for: EventsCell.self)), forCellReuseIdentifier: EventsCell.identifier)
        self.tbl_events.register(UINib.init(nibName: NoDataItemCell.identifier, bundle: Bundle(for: NoDataItemCell.self)), forCellReuseIdentifier: NoDataItemCell.identifier)
        self.tbl_events.register(UINib.init(nibName: RecommendationCell.identifier, bundle: Bundle(for: RecommendationCell.self)), forCellReuseIdentifier: RecommendationCell.identifier)
        self.tbl_events.register(UINib.init(nibName: ShowLiveEventsCell.identifier, bundle: Bundle(for: ShowLiveEventsCell.self)), forCellReuseIdentifier: ShowLiveEventsCell.identifier)
        
        self.pullRefresher.tintColor = .white
        self.pullRefresher.addTarget(self, action: #selector(self.refershEvents), for: .valueChanged)
        
        self.tbl_events.refreshControl = self.pullRefresher
        //        self.tbl_events.fadeView(style: .vertical, percentage: 0.07)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segementSelected {
        case .UPCOMING:
            self.emptyView?.isHidden = false
            self.tempView.isHidden = true
            return (self.mDataUpcoming.count > 0) ? (self.mDataUpcoming.count + 2) : 2
        case .ALL:
            self.emptyView?.isHidden = false
            self.tempView.isHidden = true
            return (self.mDataUpcoming.count > 0) ? (self.mDataUpcoming.count + 1) : 1
        case .PAST_EVENT:
            self.emptyView?.isHidden = true
            self.tempView.isHidden = true
            self.tbl_events.isHidden = false
            if self.mDataPast.count == 0{
                self.noRecordFoundView.isHidden = false
            }else{
                self.noRecordFoundView.isHidden = true
            }
            return self.mDataPast.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch segementSelected {
        case .ALL:
            let indexCellPath = indexPath
            let indexCell = indexCellPath.row
            switch indexCell {
            case 0:
                let cell  = tableView.dequeueReusableCell(withIdentifier: ShowLiveEventsCell.identifier, for: indexPath) as! ShowLiveEventsCell
                cell.livedelegate = self
                cell.selectionStyle = .none
                return cell
            default:
                let cell  = tableView.dequeueReusableCell(withIdentifier: EventsCell.identifier, for: indexPath) as!  EventsCell
                let indexCell = indexPath.row - 1
                let indexCellPath = indexPath
                let item = self.mDataUpcoming[indexCell]
                cell.baseVC = self
                cell.configure(item: item)
                cell.onDragOpenDetail = {
                    if self.checkUserIsLoggedIn(){
                        self.tableView(self.tbl_events, didSelectRowAt: indexCellPath)
                    }
                }
                if item.eventStatus == .ONGOING{
                    cell.setliveEventView(isLive: true, item: item)
                    cell.selectionStyle = .none
                    return  cell
                }else{
                    cell.setliveEventView(isLive: false, item: item)
                    if isOnlyShownLiveEvents {
                        return UITableViewCell()
                    }else{
                        cell.selectionStyle = .none
                        return  cell
                    }
                }
            }
        case .UPCOMING:
            let indexCellPath = indexPath
            let indexCell = indexCellPath.row
            switch indexCell {
            case 0:
                let cell  = tableView.dequeueReusableCell(withIdentifier: RecommendationCell.identifier, for: indexPath) as! RecommendationCell
                cell.baseVC = self
                cell.setupDataArray(mDataItems: self.mDataSuggestion)
                
                cell.callBackForSuggestion = { itemSuggestion , positionItem in
                    
                    if self.checkUserIsLoggedIn(){
                        
                        self.checkUserIsBlocked(itemSuggestion.id) { (isBlocked) in
                           
                            if isBlocked{
                               let _ = SweetAlert().showAlert("", subTitle: "You've been removed from the event by the event host for violating community guidelines, terms of use, or privacy policy.\n\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57), buttonTitleColor: .white) { isConfirm in
                                    
                                }
                            }else{
                                let feed = self.storyboard?.instantiateViewController(withIdentifier: EventDetailsVC.identifier) as! EventDetailsVC
                                
                                feed.eventItem = itemSuggestion
                                feed.eventDismissDelegate = self
                                feed.view.frame = self.containerView.bounds
                                
                                if itemSuggestion.subscriptionStatus != .COMPLETED, let pwd = itemSuggestion.password  , !pwd.isEmpty{
                                    self.showPasswordAlert(itemSuggestion.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                        if let isPwdMatched = done , isPwdMatched{
                                            self.containerView.isHidden = false
                                            self.addChild(feed)
                                            self.viewSearch.isHidden = true
                                            self.containerView.addSubview(feed.view)
                                        }
                                    }
                                }else{
                                    self.containerView.isHidden = false
                                    self.addChild(feed)
                                    self.viewSearch.isHidden = true
                                    self.containerView.addSubview(feed.view)
                                }
                            }
                        }
                    }
                }
                cell.selectionStyle = .none
                return cell
            case 1:
                let cell  = tableView.dequeueReusableCell(withIdentifier: ShowLiveEventsCell.identifier, for: indexPath) as! ShowLiveEventsCell
                cell.livedelegate = self
                cell.selectionStyle = .none
                return cell
            case 2:
                if(self.mDataUpcoming.count == 0){
                    let cell  = tableView.dequeueReusableCell(withIdentifier: NoDataItemCell.identifier, for: indexPath) as! NoDataItemCell
                    cell.onClickNotFound = {
                        self.onClickNotFound()
                    }
                    cell.selectionStyle = .none
                    cell.isHidden = imgShimmerView.isHidden == false ? true : false
                    return cell
                }else{
                    let cell  = tableView.dequeueReusableCell(withIdentifier: EventsCell.identifier, for: indexPath) as!  EventsCell
                    let indexCell = indexPath.row - 2
                    let indexCellPath = indexPath
                    let item = self.mDataUpcoming[indexCell]
                    cell.baseVC = self
                    cell.configure(item: item)
                    cell.onDragOpenDetail = {
                        if self.checkUserIsLoggedIn(){
                            self.tableView(self.tbl_events, didSelectRowAt: indexCellPath)
                        }
                    }
                    if item.eventStatus == .ONGOING{
                        cell.setliveEventView(isLive: true, item: item)
                        cell.selectionStyle = .none
                        return  cell
                    }else{
                        cell.setliveEventView(isLive: false, item: item)
                        if isOnlyShownLiveEvents {
                            return UITableViewCell()
                        }else{
                            cell.selectionStyle = .none
                            return  cell
                        }
                    }
                }
            default:
                let cell  = tableView.dequeueReusableCell(withIdentifier: EventsCell.identifier, for: indexPath) as!  EventsCell
                let indexCell = indexPath.row - 2
                let indexCellPath = indexPath
                let item = self.mDataUpcoming[indexCell]
                cell.baseVC = self
                cell.configure(item: item)
                cell.onDragOpenDetail = {
                    if self.checkUserIsLoggedIn(){
                        self.tableView(self.tbl_events, didSelectRowAt: indexCellPath)
                    }
                }
                if item.eventStatus == .ONGOING {
                    cell.setliveEventView(isLive: true, item: item)
                    cell.selectionStyle = .none
                    return  cell
                }else{
                    cell.setliveEventView(isLive: false, item: item)
                    if isOnlyShownLiveEvents {
                        return UITableViewCell()
                    }else{
                        cell.selectionStyle = .none
                        return  cell
                    }
                }
            }
        case .PAST_EVENT:
            let cell  = tableView.dequeueReusableCell(withIdentifier: EventsCell.identifier, for: indexPath) as!  EventsCell
            let indexCell = indexPath.row
            let indexCellPath = indexPath
            let item = self.mDataPast[indexCell]
            cell.baseVC = self
            cell.setliveEventView(isLive: false, item: item)
            cell.configure(item: item)
            cell.onDragOpenDetail = {
                if self.checkUserIsLoggedIn(){
                    self.tableView(self.tbl_events, didSelectRowAt: indexCellPath)
                }
            }
            cell.selectionStyle = .none
            return  cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0, segementSelected == .UPCOMING, mDataSuggestion.count == 0 {
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.checkUserIsLoggedIn(){
            if let _ = tableView.cellForRow(at: indexPath) as? NoDataItemCell{
                self.onClickNotFound()
            }else if let _ = tableView.cellForRow(at: indexPath) as? RecommendationCell {
                
            }else if let _ = tableView.cellForRow(at: indexPath) as? ShowLiveEventsCell {
                
            }else{
                
                switch segementSelected {
                
                case .ALL:
                    let item = mDataUpcoming[indexPath.row - 1]
                    
                    checkUserIsBlocked(item.id) { (isBlocked) in
                        if isBlocked{
                          let _ =  SweetAlert().showAlert("", subTitle: "You've been removed from the event by the event host for violating community guidelines, terms of use, or privacy policy.\n\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57) , buttonTitleColor: .white) { isConfirm in
                                
                            }
                        }else{
                            
                            switch item.subscriptionStatus {
                            
                            case .NONE:
                                
                                switch item.eventStatus {
                                
                                case .UPCOMING,.ONGOING:
                                    //show event setting page
                                    let feed = self.storyboard?.instantiateViewController(withIdentifier: EventDetailsVC.identifier) as! EventDetailsVC
                                    feed.eventItem = item
                                    self.addChild(feed)
                                    feed.eventDismissDelegate = self
                                    feed.view.frame = self.containerView.bounds
                                    self.viewSearch.isHidden = true
                                    
                                    if let pwd = item.password  , !pwd.isEmpty{
                                        self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                            if let isPwdMatched = done , isPwdMatched{
                                                self.containerView.isHidden = false
                                                self.containerView.addSubview(feed.view)
                                            }
                                        }
                                    }else{
                                        self.containerView.isHidden = false
                                        self.containerView.addSubview(feed.view)
                                    }
                                case .ENDED:
                                    let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                                    vc.eventItem = item
                                    vc.eventTitle = item.eventName ?? ""
                                    if let pwd = item.password  , !pwd.isEmpty{
                                        self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                            if let isPwdMatched = done , isPwdMatched{
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                        }
                                    }else{
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                
                            case .PENDING:
                                //show event ticket page
                                let feed = self.storyboard?.instantiateViewController(withIdentifier: EventTicketVC.identifier) as! EventTicketVC
                                feed.eventItem = item
                                if let pwd = item.password  , !pwd.isEmpty{
                                    self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                        if let isPwdMatched = done , isPwdMatched{
                                            self.navigationController?.pushViewController(feed, animated: true)
                                        }
                                    }
                                }else{
                                    self.navigationController?.pushViewController(feed, animated: true)
                                }
                                
                            case .COMPLETED:
                                
                                switch item.eventStatus {
                                case .UPCOMING:
                                    //show countDown Screen
                                    let feed = self.storyboard?.instantiateViewController(withIdentifier: EventCountDownVC.identifier) as! EventCountDownVC
                                    feed.eventItem = item
                                    feed.eventTitle = item.eventName ?? ""
                                    //                        if let pwd = item.password  , !pwd.isEmpty{
                                    //                            self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                    //                                if let isPwdMatched = done , isPwdMatched{
                                    //                                    self.navigationController?.pushViewController(feed, animated: true)
                                    //                                }
                                    //                            }
                                    //                        }else{
                                    self.navigationController?.pushViewController(feed, animated: true)
                                    
                                // }
                                case .ONGOING:
                                    //show movement screen
                                    let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                                    vc.eventItem = item
                                    vc.eventTitle = item.eventName ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                case .ENDED:
                                    let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                                    vc.eventItem = item
                                    vc.eventTitle = item.eventName ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                
                            }
                        }
                    }
                case .UPCOMING:
                    
                    let item = mDataUpcoming[indexPath.row - 2]
                    
                    checkUserIsBlocked(item.id) { (isBlocked) in
                        if isBlocked{
                          let _ = SweetAlert().showAlert("", subTitle: "You've been removed from the event by the event host for violating community guidelines, terms of use, or privacy policy.\n\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57) , buttonTitleColor: .white) { isConfirm in
                                
                            }
                        }else{
                            
                            switch item.subscriptionStatus {
                            case .NONE:
                                //show event setting page
                                switch item.eventStatus{
                                
                                case .UPCOMING,.ONGOING:
                                    //show event setting page
                                    
                                    let feed = self.storyboard?.instantiateViewController(withIdentifier: EventDetailsVC.identifier) as! EventDetailsVC
                                    if item.title?.lowercased() == "start here!"{
                                        self.clearDemoDelighterAndMoment(item.id)
                                        feed.isFromStartHereEvent = true
                                        isGlobalDemoEvent = true
                                        isDemoEventClear = false
                                    }
                                    
                                    feed.eventItem = item
                                    feed.eventDismissDelegate = self
                                    feed.view.frame = self.containerView.bounds
                                    
                                    if let pwd = item.password  , !pwd.isEmpty{
                                        self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                            if let isPwdMatched = done , isPwdMatched{
                                                self.containerView.isHidden = false
                                                self.addChild(feed)
                                                self.viewSearch.isHidden = true
                                                self.containerView.addSubview(feed.view)
                                            }
                                        }
                                    }else{
                                        
                                        self.containerView.isHidden = false
                                        self.addChild(feed)
                                        self.viewSearch.isHidden = true
                                        self.containerView.addSubview(feed.view)
                                    }
                                    
                                case .ENDED:
                                    let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                                    vc.eventItem = item
                                    vc.eventTitle = item.eventName ?? ""
                                    if let pwd = item.password  , !pwd.isEmpty{
                                        self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                            if let isPwdMatched = done , isPwdMatched{
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                        }
                                    }else{
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        
                                    }
                                    
                                }
                                
                            case .PENDING:
                                //show event ticket page
                                let feed = self.storyboard?.instantiateViewController(withIdentifier: EventTicketVC.identifier) as! EventTicketVC
                                feed.eventItem = item
                                if let pwd = item.password  , !pwd.isEmpty{
                                    self.showPasswordAlert(item.eventName ?? "", passowrd: pwd) { (done, cancel) in
                                        if let isPwdMatched = done , isPwdMatched{
                                            self.navigationController?.pushViewController(feed, animated: true)
                                        }
                                    }
                                }else{
                                    self.navigationController?.pushViewController(feed, animated: true)
                                    
                                }
                                
                            case .COMPLETED:
                                
                                switch item.eventStatus {
                                case .UPCOMING:
                                    //show countDown Screen
                                    let feed = EventCountDownVC.instantiate(fromAppStoryboard: .home)
                                    feed.eventItem = item
                                    feed.eventTitle = item.eventName ?? ""
                                    self.navigationController?.pushViewController(feed, animated: true)
                                    
                                // }
                                case .ONGOING:
                                    //show movement screen
                                    let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                                    vc.eventItem = item
                                    vc.eventTitle = item.eventName ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)

                                case .ENDED:
                                    //show end result screen
                                    let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                                    vc.eventItem = item
                                    vc.eventTitle = item.eventName ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                    }
                    break
                    
                case .PAST_EVENT:
                    
                    checkUserIsBlocked(self.mDataPast[indexPath.row].id) { (isBlocked) in
                        if isBlocked{
                         let _ =  SweetAlert().showAlert("", subTitle: "You've been removed from the event by the event host for violating community guidelines, terms of use, or privacy policy.\n\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57), buttonTitleColor: .white) { isConfirm in
                                
                            }
                        }else{
                            
                            let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                            vc.eventItem = self.mDataPast[indexPath.row]
                            vc.eventTitle = self.mDataPast[indexPath.row].eventName ?? ""
                            
                            if self.mDataPast[indexPath.row].subscriptionStatus != .COMPLETED , let pwd = self.mDataPast[indexPath.row].password  , !pwd.isEmpty{
                                self.showPasswordAlert(self.mDataPast[indexPath.row].eventName ?? "", passowrd: pwd) { (done, cancel) in
                                    if let isPwdMatched = done , isPwdMatched{
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                
                            }else{
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        }
                    }
                    break
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? EventsCell{
                cell.transform = .init(scaleX: 0.90, y: 0.90)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = tableView.cellForRow(at: indexPath) as? EventsCell{
                cell.transform = .identity
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) < 10{
            switch segementSelected{
            case .UPCOMING,.ALL:
                if(self.mDataUpcoming.count < kLimitPage){
                    return
                }
                if(self.mDataUpcoming.count % kLimitPage == 0){
                    let page = (self.mDataUpcoming.count / kLimitPage)
                    //MARK: Call For Upcoming Paging
                    self.apiCallUpcomingOrPast(type: .UPCOMING, page: page)
                }
                break
            case .PAST_EVENT:
                if(self.mDataPast.count < kLimitPage){
                    return
                }
                if(self.mDataPast.count % kLimitPage == 0){
                    //MARK: Call For Past Paging
                    let page = (self.mDataPast.count / kLimitPage)
                    self.apiCallUpcomingOrPast(type: .PAST_EVENT, page: page)
                }
                break
            }
        }
    }
}

// MARK:- Verify Moment Notification
extension HomeVC  {
    
    func showMoment(data : [String : Any] , notificationEventId:String) {
        
        if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last as? BaseViewController {
            let m = Movement().initMovementFromNotification(json: data)
            lastVC.showLoading(on: lastVC.view, of: FrenzyColor.white)
            APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: "CALL sp_checkIfMomentTerminatedWithoutVidCount(\(m.id))"), completion: { apiResponse in
                switch apiResponse {
                case .Failure(_):
                    lastVC.hideLoading(on: lastVC.view)
                    lastVC.showErrorToast(title: "Moment Terminated!", description: "This moment no longer exist or terminated by Host") { (Bool) in
                        return
                    }
                case .Success(let resultData):
                    lastVC.hideLoading(on: lastVC.view)
                    var isTerminated = 0
                  
                    if let dataobj = resultData?.result?.firstObject as? [String:Any], let is_Terminated = dataobj["is_terminated"] as? Int{
                        isTerminated = is_Terminated
                    }
                    else if let dataobj = resultData?.result?.firstObject as? [String:Any], let is_Terminated = dataobj["is_terminated"] as? String{
                        let intTerminated = (is_Terminated as NSString).integerValue
                        isTerminated = intTerminated
                    }
                    if (resultData?.result?.count)! > 0 && isTerminated == 0 {
                        
                        let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                        let sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
                        let flashArr = UserDefaults.standard.array(forKey: kFlashKey) as? [Bool] ?? [Bool]()
                        let screenArr = UserDefaults.standard.array(forKey: kScreenKey) as? [Bool] ?? [Bool]()
                        let vibrateArr = UserDefaults.standard.array(forKey: kVibrateKey) as? [Bool] ?? [Bool]()
                                                
                        for (index,eventId) in eventIdArr.enumerated() {
                            if eventId == Int(data["event_id"] as? String ?? "0")! {
                                let movement = Movement().initMovementFromNotification(json: data)
                                let nowDuratioin = movement.remainingDuration
                                    let userPermission = UserPermissionInfo(flash: flashArr[index], screen: screenArr[index], vibrate: vibrateArr[index])
                                    let mySelectedSec = sectionIdArr[index]
                                    if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                                        if let baseVc =  lastVC  as?  BaseViewController {
                                            baseVc.openMovementVC(with: movement, userPermission: userPermission , duration: nowDuratioin, eventId: notificationEventId , mySec: mySelectedSec)
                                        }
                                    }
                            }
                        }
                    }else{
                        if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                                if let baseVc =  lastVC  as?  BaseViewController {
                                     let movement = Movement().initMovementFromNotification(json: data)
                                    selectedMomentID = movement.id
                                    baseVc.openEventScreen(id: movement.id, type: "moment")
                                }
                        }else{
                            lastVC.showErrorToast(title: "Moment Terminated!", description: "This moment no longer exist or terminated by Host") { (Bool) in
                                                       return
                                                   }
                        }
                       
                    }
                }
            })
        }
    }
}
    
extension Notification.Name {
    
    static let homeFeedRefresh = Notification.Name("homeFeedRefresh")
    static let stopVideo = Notification.Name("stopVideo")
    static let demoDeligter = Notification.Name("demoDeligter")
    static let demoDeligterThree = Notification.Name("demoDeligterThree")
    static let exitDemoMoment = Notification.Name("exitDemoMoment")
}
