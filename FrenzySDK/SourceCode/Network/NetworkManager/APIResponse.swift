
//
//  APIContants.swift
//  BandPass
//
//  Created by Jassie on 12/01/16.
//  Copyright © 2016 eeGames. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

extension API{
    // MARK: Handler Responce With Cases
    func handleResponse(parameters : JSON?) -> ResponseApi? {
        switch self {
            case .likeVideo(_, _):
                return ResponseApi.init(result: nil, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
            case .unLikeVideo(_, _):
                return ResponseApi.init(result: nil, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
        case .reportVideo(_, _) , .unReportVideo(_, _):
               
            return ResponseApi.init(result: nil, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
       
        case .validate_viewer(_, _):
            let data = parameters?["successData"]
            if let jsonData = data?.rawString()?.data(using: .utf8)
            {
                do {
                    let userObj = try JSONDecoder().decode(Validation.self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: Validation() as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }
            }else{
                return ResponseApi.init(result: Validation() as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
            }
        case .register_viewer(_, _, _, _, _, _, _, _) , .new_RegisterApi(_, _, _, _, _, _, _, _, _, _) , .login(_, _) , .forgotPassword(_) , .changePassword(_, _, _) , .sdkLogin(_, _, _, _, _, _, _):
            let data = parameters?["successData"]
            if let jsonData = data?.rawString()?.data(using: .utf8){
                do {
                    let userObj = try JSONDecoder().decode(RegistrationApiModel.self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: parameters?.dictionary?["errorCode"]?.rawValue as AnyObject , message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? parameters?.dictionary?["errorMessage"]?.rawValue as? String ?? "")
                }
            }else{
                if let errMsg =  parameters?["errorMessage"].string{
                    return ResponseApi.init(result: nil, message: errMsg)
                }else{
                    return ResponseApi.init(result: nil, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? parameters?.dictionary?["message"]?.rawValue as? String ?? "")
                }
            }
      
        case .upload_profile_image(_):
            let data = parameters?["successData"]
            if let jsonData = data?.rawString()?.data(using: .utf8)
            {
                do {
                    let userObj = try JSONDecoder().decode(RegistrationApiModel.self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: RegistrationApiModel() as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }
            }else{
                return ResponseApi.init(result: RegistrationApiModel() as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
            }
       
        case .attendEvent:
            return ResponseApi.init(result: parameters as AnyObject?, message: "Data")
       
        case .callForProcedure(let query):
//            let userInformation = parameters?.rawValue as AnyObject
            let compareQuery = query.split(separator: "(").first ?? ""
            if(compareQuery == APIProcedure.updateFrenzySticker){
               let data = parameters?.rawString()
                if let jsonData = data?.data(using: .utf8){
                    do {
                        let userObj = try JSONDecoder().decode([Sticker].self, from: jsonData)
                        return ResponseApi.init(result: userObj as AnyObject, message: "")
                    }catch{
                        print(error as Any)
                        return ResponseApi.init(result: [Sticker]() as AnyObject, message: "")
                    }
                }else{
                    return ResponseApi.init(result: [Sticker]() as AnyObject, message: "")
                }
            }else if(compareQuery == APIProcedure.searchOrSuggestionProcedure || compareQuery == APIProcedure.eventUpcomingOrPastProcedure || compareQuery == APIProcedure.getEventDetailsFromMomentorDelighter){
           
                let data = parameters?.rawString()
                if let jsonData = data?.data(using: .utf8){
                    do {
                        let userObj = try JSONDecoder().decode([EventItem].self, from: jsonData)
                        return ResponseApi.init(result: userObj as AnyObject, message: "")
                    }catch{
                        print(error as Any)
                        return ResponseApi.init(result: [EventItem]() as AnyObject, message: "")
                    }
                }else{
                    return ResponseApi.init(result: [EventItem]() as AnyObject, message: "")
                }
            }else if(compareQuery == APIProcedure.eventDetailByIdProcedure){
                let data = parameters?.rawString()
                if let jsonData = data?.data(using: .utf8){
                    do {
                        let userObj = try JSONDecoder().decode([EventItem].self, from: jsonData)
                        return ResponseApi.init(result: userObj.first as AnyObject, message: "")
                    }catch{
                        print(error as Any)
                        return ResponseApi.init(result: nil, message: "")
                    }
                }else{
                    return ResponseApi.init(result: nil, message: "")
                }
            }
            else if(compareQuery == APIProcedure.callForEmailIsVerifiedProcedure) {
                let data = parameters?.rawString()
                if let jsonData = data?.data(using: .utf8){
                    do {
                        let userObj = try JSONDecoder().decode([RegistrationApiModelProcedure].self, from: jsonData)
                        return ResponseApi.init(result: userObj as AnyObject, message: "")
                    }catch{
                        print(error as Any)
                        return ResponseApi.init(result: [RegistrationApiModelProcedure]() as AnyObject, message: "")
                    }
                }else{
                    return ResponseApi.init(result: [RegistrationApiModelProcedure]() as AnyObject, message: "")
                }
             }else if (compareQuery == APIProcedure.getMovVideos){
                let data = parameters?.rawString()
                if let jsonData = data?.data(using: .utf8){
                    do {
                        let userObj = try JSONDecoder().decode([VideoSliderModel].self, from: jsonData)
                        return ResponseApi.init(result: userObj as AnyObject, message: "")
                    }catch{
                        print(error as Any)
                        return ResponseApi.init(result: [VideoSliderModel]() as AnyObject, message: "")
                    }
                }else{
                    return ResponseApi.init(result: [VideoSliderModel]() as AnyObject, message: "")
                }
             }else{
                return ResponseApi.init(result:  parameters?.rawValue as AnyObject, message: "")
            }
        case .uploadVideo(_, _, _, _):
            let data = parameters?["data"]
            if let jsonData = data?.rawString()?.data(using: .utf8)
            {
                do {
                    let userObj = try JSONDecoder().decode(VideoSlidAPI.self, from: jsonData)
                    return ResponseApi.init(result: userObj.getVidModel() as AnyObject, message: parameters?.dictionary?["message"]?.rawValue as? String ?? "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: VideoSliderModel() as AnyObject, message: parameters?.dictionary?["message"]?.rawValue as? String ?? "")
                }
            }else{
                return ResponseApi.init(result: VideoSliderModel() as AnyObject, message: parameters?.dictionary?["message"]?.rawValue as? String ?? "")
            }
            
        case .getSticker(_),.updateStricker(_, _, _, _, _, _):
            let data = parameters?["successData"]
            if let jsonData = data?.rawString()?.data(using: .utf8){
                do {
                    let userObj = try JSONDecoder().decode(Sticker.self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: Sticker() as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
                }
            }else{
                return ResponseApi.init(result: Sticker() as AnyObject, message: parameters?.dictionary?["successMessage"]?.rawValue as? String ?? "")
            }
            
        case .demoEventCreate(_):
            
            let data = parameters?["successData"]
            if let jsonData = data?.rawString()?.data(using: .utf8){
                print("Create Demo Event Response: \(jsonData)")
                do {
                    let userObj = try JSONDecoder().decode(EventItem.self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: EventItem() as AnyObject, message: "")
                }
            }else{
                return ResponseApi.init(result: EventItem() as AnyObject, message: "")
            }
            
        case .demoEventDetails(_):
            
            let data = parameters?["successData"]
            if let jsonData = data?.rawString()?.data(using: .utf8){
                do {
                    let userObj = try JSONDecoder().decode([EventItem].self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: [EventItem]() as AnyObject, message: "")
                }
            }else{
                return ResponseApi.init(result: [EventItem]() as AnyObject, message: "")
            }
            
        case .check_blocked(_ , _):
            return ResponseApi.init(result:  parameters?.rawValue as AnyObject, message: "")
       
            
        case .check_Blocked_viewer(_):
            return ResponseApi.init(result:  parameters?.rawValue as AnyObject, message: "")
            
        case .demoMoment(_) , .demoDelighter(_, _) , .clearDemoEvent(_) , .api_DelighterLinkView(_, _, _, _, _) , .api_momentDelighterView(_, _, _, _) :
            return ResponseApi.init(result:  parameters?.rawValue as AnyObject, message: "")
            
        case .api_preferences:
            
            let data = parameters?.rawString()
         
            if let jsonData = data?.data(using: .utf8){
                do {
                    let userObj = try JSONDecoder().decode(PreferenceListModel.self, from: jsonData)
                    return ResponseApi.init(result: userObj as AnyObject, message: "")
                }catch{
                    print(error as Any)
                    return ResponseApi.init(result: PreferenceListModel() as AnyObject, message: "")
                }
            }else{
                return ResponseApi.init(result: PreferenceListModel() as AnyObject, message: "")
            }
            
        }
    }
}

struct ResponseApi {
    var result : AnyObject?
    var message :String?
    init(result:AnyObject?,message:String?) {
        self.message = message
        self.result = result
    }
}
enum APIValidation : String{
    case None
    case Success = "1"
    case ServerIssue = "500"
    case Failed = "0"
    case TokenInvalid = "401"
}

enum APIResponse {
    case Success(ResponseApi?)
    case Failure(String?)
}
