//
//  APIContants.swift
//  BandPass
//
//  Created by Jassie on 12/01/16.
//  Copyright © 2016 eeGames. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias HttpClientSuccess = (AnyObject?) -> ()
typealias HttpClientFailure = (NSError) -> ()

class HTTPClient {
    
    func postRequest(withApi api : API , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure )  {
        let params = api.parameters
        let method = api.method
        var headers: HTTPHeaders {
            if(DataManager.sharedInstance.user != nil){
                return [kAuthorization: "jwt \(DataManager.sharedInstance.getPermanentlySavedUser()?.access_token ?? "")",
                    kAccept: kApplicationJson]
            }else{
                return [kAccept: kApplicationJson]
            }
        }
      //  print("Params: \(api.parameters ?? [String : String]()), URL: \(api.url()), Method: \(api.method), Headers: \(headers)")
        
        AF.request(api.url(), method: method,parameters: params, encoding: URLEncoding.default,headers: headers).responseString { (response) in
            
            print(response)
            switch(response.result) {
            case .success(_):
                print("response:\(response)")
              //  print("Response Value:\(response.value)")
                
                if let data = response.value{
                    
                    if data.contains("<!DOCTYPE HTML PUBLIC") {
                        print(data as Any)
                        if let errorMsg =  JSON(data).dictionary, let msg = errorMsg["errorMessage"]?.string{
                            failure(NSError.init(domain: msg, code: 400, userInfo: ["message":msg]))
                        }else{
                            failure(NSError.init(domain: kServerError, code: 400, userInfo: ["message":"Server Error Not Found"]))
                        }
                    }else{
                        success(data.parseJSONString as AnyObject?)
                    }
                }else{
                    failure(NSError.init(domain: kServerError, code: 400, userInfo: ["message":"Server Error"]))
                }
            case .failure(let error):
                print("params:- \(api.parameters ?? [String : String]())")
                failure(error as NSError)
              //  failure(AFDataResponse.result.error! as NSError)
            }
        }
    }
    
    func UploadVideo(withApi api:API, image:UIImage,videoLink:URL ,success: @escaping HttpClientSuccess, failure: @escaping HttpClientFailure, progress: @escaping (Double) -> ()){
       
        let headers: HTTPHeaders = [
            "app_key": APIConstants.apiSecret,
            "Accept": "application/json",
            "session_token":DataManager.sharedInstance.getPermanentlySavedUser() != nil ? (DataManager.sharedInstance.getPermanentlySavedUser()?.access_token)! : "",
            "Content-type": "multipart/form-data"
        ]
        
        print(api.url())
        print(api.parameters as Any)
        
        let videoFileName = String(Date().timeIntervalSince1970)+"."+videoLink.pathExtension
        
        print("Video FIle Name: \(videoFileName)")
       
        let imgFIleName = String(Date().timeIntervalSince1970) + ".png"
        
        AF.upload(multipartFormData: { (multipartFormData) in
           
            multipartFormData.append(videoLink, withName: "video", fileName: videoFileName, mimeType: "video/mp4")
            
            let dt = image.jpegData(compressionQuality: 0.2)!
         
            multipartFormData.append( dt, withName: "thumbnail_image", fileName: imgFIleName, mimeType: "image/png")
            
            for (key, value) in api.parameters!{
                if let stringValue = value as? String{
                    multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        }, to: api.url(), method: .post, headers: headers).uploadProgress { (Progress) in
           
            progress(Progress.fractionCompleted)
            print("Upload Progress UploadVideo: \(Progress.fractionCompleted)")
      
        }.responseJSON { (response) in
         
            switch response.result{
            case .success(let value):
                print("YOUR JSON DATA>>  \(value)")
                success(value as AnyObject?)
            break
            case .failure( let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error as NSError)
            }
        }
    }
    
    func UploadFiles(withApi api:API,image:UIImage, success : @escaping HttpClientSuccess, failure: @escaping HttpClientFailure, progress: @escaping (Double) -> ())
    {
        let headers: HTTPHeaders = [
            "app_key": APIConstants.apiSecret,
            "Accept": "application/json",
            "session_token":DataManager.sharedInstance.getPermanentlySavedUser() != nil ? (DataManager.sharedInstance.getPermanentlySavedUser()?.access_token)! : "",
            "Content-type": "multipart/form-data"
        ]
        
        print(api.url())
        print(api.parameters as Any)
        
        AF.upload(multipartFormData: { (multipartFormData) in
            let dt = image.jpegData(compressionQuality: 0.2)
            multipartFormData.append( dt!, withName: "image", fileName: String(Date().timeIntervalSince1970) + ".png", mimeType: "image/png")
         
            for (key, value) in api.parameters!{
                if let stringValue = value as? String{
                    multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        }, to: api.url(), usingThreshold: UInt64.init(), method: .post, headers: headers
       
        ).uploadProgress(closure: { (Progress) in
            progress(Progress.fractionCompleted)
            print("Upload Progress UploadFiles: \(Progress.fractionCompleted)")
      
        }).responseString(completionHandler: { (AFDataResponse) in
          
            switch(AFDataResponse.result) {
            case .success(let value):
               // if let data = response.result.value{
                    print("YOUR JSON DATA>>  \(value.parseJSONString!)")
                    success(value.parseJSONString as AnyObject?)
               // }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error as NSError)
            }
        })
    }
}

