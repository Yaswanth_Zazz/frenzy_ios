
//
//  APIContants.swift
//  BandPass
//
//  Created by Jassie on 12/01/16.
//  Copyright © 2016 eeGames. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias OptionalDictionary = [String : Any]?
typealias OptionalSwiftJSONParameters = [String : JSON]?

infix operator =>
infix operator =|
infix operator =<
infix operator =/

func =>(key : String, json : OptionalSwiftJSONParameters) -> String?{
    return json?[key]?.stringValue
}

func =<(key : String, json : OptionalSwiftJSONParameters) -> Double?{
    return json?[key]?.double
}

func =|(key : String, json : OptionalSwiftJSONParameters) -> [JSON]?{
    return json?[key]?.arrayValue
}

func =/(key : String, json : OptionalSwiftJSONParameters) -> Int?{
    return json?[key]?.intValue
}

prefix operator ¿
prefix func ¿(value : String?) -> String {
    return value.unwrap()
}


protocol Router {
    var route : String { get }
    var baseURL : String { get }
    var parameters : OptionalDictionary { get }
    var method : Alamofire.HTTPMethod { get }
}

enum API {
    
    static func mapKeysAndValues(keys : [String]?,values : [Any]?) -> [String : Any]?{
        guard let tempValues = values,let tempKeys = keys else { return nil}
        var params = [String : Any]()
        for (key,value) in zip(tempKeys,tempValues) {
            params[key] = value
        }
        return params
    }
    // MARK: Specify Case Here
    case validate_viewer(email:String,phone:String)
    case register_viewer(email:String,phone:String,name:String,address1:String,address2:String,city:String,state:String,zip:String)
    case new_RegisterApi(email:String,phone:String,name:String, password:String, preferencesId : [Int], address1:String,address2:String,city:String,state:String,zip:String)
    
    case login(email:String , password:String)
    case changePassword(id: String, oldPassword: String,password: String)
    case forgotPassword(email: String)
    case upload_profile_image(userID: Int)
    case callForProcedure(query:String)
    case uploadVideo(viewer_id:String,moment_id:String,ratio:String,length:String)
    
    case likeVideo(viewer_id:String,video_id:String)
    case attendEvent(viewer_id:String, event_id:String, team_id:String, is_vibrate:String, is_flash:String, is_screen:String)
    case unLikeVideo(viewer_id:String,video_id:String)
    case reportVideo(viewer_id:String,video_id:String)
    case unReportVideo(viewer_id:String,video_id:String)
    case getSticker(viewer_id:String)
    case updateStricker(viewer_id:String,city : String, state : String, zip : String, address1 : String , address2 : String)
    // Demo Events APi
    case demoEventCreate(viewer_id:String)
    case demoEventDetails(viewer_id:String)
    case demoDelighter(del_number:String , event_id : String)
    case demoMoment(event_id : String)
    case clearDemoEvent(event_id : String)
    case check_blocked(event_id : String , viewer_id:String)
    case api_preferences
    case api_momentDelighterView(event_id:String,control_id:String,control_type:String, viewer_id:String)
    case api_DelighterLinkView(event_id:String,control_id:String,control_type:String, viewer_id:String , url : String)
    case check_Blocked_viewer(email: String)
    case sdkLogin(email:String , partner_id:String , partner_user_id:String, name:String , token:String , isNotification: Int , deviceId:String)

}




extension API : Router{
    // MARK: Cases For Routs Path
    var route : String {
        switch self {
        case .likeVideo(_ , _):
            return APIPaths.likeVideo

        case .unLikeVideo(_ , _):
            return APIPaths.unLikeVideo
      
        case .reportVideo(_ , _):
            return APIPaths.reportVideo
       
        case .unReportVideo(_, _):
            return APIPaths.unReportVideo
       
        case .uploadVideo(_, _, _, _):
            return APIPaths.uploadVideCall
        case .validate_viewer(_ , _):
            
            return APIPaths.validate_viewer
        case .getSticker(_):
            return APIPaths.getSticker
        case .updateStricker(_, _, _, _, _, _):
            return APIPaths.updateSticker
        case .register_viewer(_, _, _, _, _, _, _, _):
            return APIPaths.register_viewer
       
        case .new_RegisterApi(_, _, _, _, _, _, _, _, _, _):
            return APIPaths.new_RegisterApi
            
        case .login(_ , _), .sdkLogin(_, _, _, _, _, _, _):
            return APIPaths.login
            
        case .upload_profile_image(_):
            return APIPaths.upload_profile_image
        case .callForProcedure(_):
            return APIPaths.dataBasePath
        case .attendEvent(_, _, _, _, _, _):
            return APIPaths.attendee
            
            // Demo Events APi
        case .demoEventCreate:
            return APIPaths.createDemoEvent
        case .demoEventDetails:
            return APIPaths.getDemoEventDetail
        case .demoDelighter:
            return APIPaths.getDemoDelighter
        case .demoMoment:
            return APIPaths.getDemoMoment
        case .clearDemoEvent:
            return APIPaths.clearDemoEvent
            
        case .changePassword:
            return APIPaths.changePassword
        case .forgotPassword:
            return APIPaths.forgotPassword
            
        case .api_preferences:
            return APIPaths.api_preferences
            
        case .check_blocked:
            return APIPaths.check_blocked
        
        case .check_Blocked_viewer:
            return APIPaths.check_Blocked_viewer
            
        case .api_momentDelighterView:
            return APIPaths.api_momentDelighterView
            
        case .api_DelighterLinkView:
            return APIPaths.api_DelighterLinkView
            
        }
    }
    
    var baseURL : String {  return APIConstants.BasePath }
    
    var parameters : OptionalDictionary {
        var pm = formatParameters()
        if(pm != nil){
           pm![kApiSecert] = APIConstants.apiSecret
        }
        return pm
    }
    
    func url() -> String {return baseURL + route}
  
    // MARK: Cases For Routs Method Type
    var method: Alamofire.HTTPMethod {
        switch self {
        case .uploadVideo(_, _, _, _):
            return .post
        case .validate_viewer(_ , _):
            return .post
        case .register_viewer(_, _, _, _, _, _, _, _):
            return .post
        case .new_RegisterApi(_, _, _, _, _, _, _, _, _, _):
            return .post
        case .login(_ , _), .sdkLogin(_, _, _, _, _, _, _):
            return .post
        case .upload_profile_image(_):
            return .post
        case .callForProcedure(_):
            return .post
        case .likeVideo(_ , _):
            return .post
        case .unLikeVideo(_ , _):
            return .post
        case .reportVideo(_ , _), .attendEvent(_, _, _, _, _, _), .unReportVideo(_ , _):
            return .post
        case .getSticker(_):
            return .post
        case .updateStricker(_, _, _, _, _, _):
            return .post
            
            // Demo Event:
        case .demoEventCreate(_):
            return .post
        case .demoEventDetails(_):
            return .post
        
        case .changePassword(_, _, _):
            return .post
      
        case .forgotPassword(_):
            return .post
            
        case .demoMoment(_) , .demoDelighter(_ , _) , .clearDemoEvent(_):
            return.post
        case .api_preferences:
            return .post
        case .check_blocked(_, _) , .check_Blocked_viewer(_):
            return .post
        case .api_DelighterLinkView(_, _, _, _, _) , .api_momentDelighterView(_, _, _, _):
            return .post
        }
    }
}

extension API {
    
    // MARK: Cases For Routs Parametres
    func formatParameters() -> OptionalDictionary {
        switch self {
       
        case .validate_viewer(let email, let phone):
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.validate_viewer, values: [email,phone])
        case .register_viewer(let email, let phone, let name, let address1, let address2, let city, let state, let zip):
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.register_viewer, values: [email,phone,name,address1,address2,city,state,zip])
     
        case .new_RegisterApi(let email, let phone , let name, let password ,  let preferencesId  ,let address1, let address2, let city, let state, let zip):
        
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.new_RegisterViewer, values: [email,phone,name, password , preferencesId ,address1,address2,city,state,zip])
            
        case .login(let email , let password):
            
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.login, values: [email, password])
      
        case .sdkLogin(let email, let partner_id, let partner_user_id, let name , let token , let isNotification , let deviceId):
           
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.sdkLogin, values: [email, partner_id ,partner_user_id, name , FrenzyPartnerID.partnerAccessKey, token, isNotification, deviceId])

        case .changePassword(let id, let oldPassword , let password):
          
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.changePassword, values: [id,oldPassword, password])
      
        case .forgotPassword( let email):
            
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.forgotPassword, values: [email])
            
        case .upload_profile_image(let userID):
            return API.mapKeysAndValues(keys: APIParameterConstants.UploadImage.upload_profile_image, values: [String(userID)])
        
        case .callForProcedure(let query):
            return API.mapKeysAndValues(keys: APIParameterConstants.Query.query, values: [kDBModel.db_url, kDBModel.db_name ,¿query,¿kDBModel.db_password,¿kDBModel.db_user])
            
        case .uploadVideo(let viewer_id,let moment_id,let ratio,let length):
            return API.mapKeysAndValues(keys: APIParameterConstants.UploadImage.uploadVideo, values: [viewer_id,moment_id,ratio,length,"ios"])
        case .likeVideo(let viewer_id, let video_id):
           return API.mapKeysAndValues(keys: APIParameterConstants.UploadImage.likeUnlikeReport, values: [viewer_id,video_id])
        case .unLikeVideo(let viewer_id, let video_id):
            return API.mapKeysAndValues(keys: APIParameterConstants.UploadImage.likeUnlikeReport, values: [viewer_id,video_id])
        case .reportVideo(let viewer_id, let video_id):
            return API.mapKeysAndValues(keys: APIParameterConstants.UploadImage.likeUnlikeReport, values: [viewer_id,video_id])
            
        case .unReportVideo(let viewer_id, let video_id):
           
            return API.mapKeysAndValues(keys: APIParameterConstants.UploadImage.likeUnlikeReport, values: [viewer_id,video_id])
            
        case .getSticker(let viewer_id):
            return ["viewer_id":viewer_id]
     
        case .updateStricker(let viewer_id,let city,let state,let zip,let address1,let address2):
            return ["viewer_id" : viewer_id, "city" : city, "state" : state, "zip": zip,
                    "address1" : address1, "address2" : address2 ]
        case .attendEvent(let viewer_id, let event_id, let team_id,let is_vibrate,let is_flash, let is_screen):
             return API.mapKeysAndValues(keys: APIParameterConstants.Atteendee.attende, values: [viewer_id,event_id,team_id,is_vibrate,is_flash,is_screen])
         
            // Demo Events
        case .demoEventCreate(let viewer_id):
            return ["viewer_id":viewer_id]
            
        case .demoEventDetails(let viewer_id):
            return ["viewer_id":viewer_id]
            
        case .demoMoment(let event_id):
            return ["event_id" : event_id]
            
        case .check_blocked(let event_id, let viewer_id):
            return ["event_id" : event_id , "viewer_id" : viewer_id]
        
        case .clearDemoEvent(let event_id):
            return ["event_id" : event_id]
        case .demoDelighter(let del_number, let event_id):
            return ["del_number":del_number,"event_id":event_id]
            
        case .api_preferences:
            return  [String : String]()
        
        case .api_momentDelighterView(let event_id , let control_id, let control_type , let viewer_id):
            return ["event_id" : event_id , "control_id" : control_id , "control_type" : control_type , "viewer_id" : viewer_id]
            
        case .api_DelighterLinkView(let event_id , let control_id, let control_type , let viewer_id , let url):
            return ["event_id" : event_id , "control_id" : control_id , "control_type" : control_type , "viewer_id" : viewer_id , "url" : url]
            
        case .check_Blocked_viewer( let email):
            
            return API.mapKeysAndValues(keys: APIParameterConstants.Registration.forgotPassword, values: [email])
        }
    }
}
