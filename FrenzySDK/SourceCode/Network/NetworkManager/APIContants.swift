
//
//  APIContants.swift
//  BandPass
//
//  Created by Jassie on 12/01/16.
//  Copyright © 2016 eeGames. All rights reserved.
//

import Foundation

func print(_ items: Any...) {
    #if DEBUG
    Swift.print(items[0])
    #endif
}

internal struct Build{
    static let isProduction = 0
}

internal struct APIPaths {
    // API Paths
    static let validate_viewer = "/api/validate_viewer" // param (email,  phone)
    static let register_viewer = "/api/resend-email" // param (email,  phone) optional(  address1  ,  address2, city,   zip,  state ]
    static let login = "/api/login-sdk/viewer"//"/api/login/viewer"
    static let upload_profile_image = "/api/upload_profile_image"
    static let changePassword = "/api/change_password/viewer"
    static let forgotPassword = "/api/forgot_password/viewer"
    static let uploadVideCall = "/api/upload_moment_video" // post param: video,viewer_id,moment_id,ratio,thumbnail_image,length (seconds)
    static let dataBasePath = "/phpfile.php"
    static let likeVideo = "/api/like_video"//param viewer_id,video_id
    static let unLikeVideo = "/api/unlike_video" //param viewer_id,video_id
    static let reportVideo = "/api/report_video"//param viewer_id,video_id
    static let unReportVideo = "/api/unreport_video"
    static let attendee  = "/api/viewer_subscribe"
    static let getSticker  = "/api/get_sticker"
    static let updateSticker  = "/api/request_sticker"
    
    static let createDemoEvent = "/api/events/cr" // params: - viewer_id
    static let getDemoEventDetail = "/api/events/gt"
    static let getDemoDelighter = "/api/demo/delighter" // {"del_number":3,"event_id":524}
    static let getDemoMoment = "/api/demo/moment" //  {"event_id":524}
    static let clearDemoEvent = "/api/demo/clr" // {"event_id":524}
    static let new_RegisterApi = "/api/register/viewer" // {"email":"vipon41359@shzsedu.com","password":"asdfasdf","phone":"+1955888888","name":"xyz"}
    static let api_preferences = "/api/preferences"
    static let api_momentDelighterView = "/api/click"
    static let api_DelighterLinkView = "/api/click"
    static let check_blocked  = "/api/check-blocked"
    static let check_Blocked_viewer = "/api/check_viewer"
}

enum FilterCase :String{
    case recent = "recent"
    case voted = "voted"
    case my = "my"
}

struct APIProcedure{
    static let callForEmailIsVerifiedProcedure = "CALL `sp_viewerDetailsGet`"
    static let searchOrSuggestionProcedure = "CALL `sp_eventsSearchSuggetionGet`"
    static let eventUpcomingOrPastProcedure = "CALL `sp_eventsUpcomingPastGet`"
    static let eventDetailByIdProcedure = "CALL `sp_eventDetailByIdGet`"
    static let eventSettingUpdateProcedure = "CALL `sp_eventSettingUpdate`"
    static let eventTicketUpdateProcedure = "CALL `sp_eventTicketUpdate`"
    static let eventSectionGetProcedure = "CALL `sp_eventSectionsGet`"
    static let updateFrenzySticker = "CALL  `sp_requestSticker`"
    static let eventDetailGetProcedure = "CALL `sp_eventDetailByIdGet`"
    static let eventPostsGetProcedure = "CALL `sp_eventPostsGet`"
    static let delighterCommentsGetProcedure = "CALL `sp_commentsGet`"
    static let delighterCommentSetProcedure = "CALL `sp_commentSet`"
    static let delighterCommentUpdateProcedure = "CALL `sp_commentUpdate`"
    static let delighterCommentDeleteProcedure = "CALL `sp_commentUpdate`"
    static let getMovVideos = "CALL `sp_momentVideosGet`"
    static let notificationUpdate = "CALL `sp_notiCountUpdate`"
    static let getEventDetailsFromMomentorDelighter = "CALL `sp_getMomentOrDelighterDetailById`"
    
    static func getMovVideosPro(momentId:Int,type:FilterCase,page:Int = 0,limit:Int = kLimitPage) -> String{
        return "\(getMovVideos)(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0),\(momentId),'\(type.rawValue)',\(page), \(limit))"
    }
    
    static func callForEmailIsVerified(email:String? = nil) -> String {
        return "CALL `sp_viewerDetailsGet`('0',  '\(email == nil ? "\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.email ?? "")" : "\(email!)")',  '')"
    }
    static func searchOrSuggestion(type:SearchOrSuggestion,searchTxt:String,lat:String = "0",lng:String = "0",page:Int = 0,limit:Int = kLimitPage) -> String{
        return "CALL `sp_eventsSearchSuggetionGet`( \(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), '\(type.rawValue)', '\(searchTxt)', '\(lat)', '\(lng)', \(page), \(limit),'\(FrenzyPartnerID.partnerID)')"
    }
    
    // make SDK changes
    static func eventUpcomingOrPast(type:SegementOption,lat:String = "0",lng:String = "0",page:Int = 0,limit:Int = kLimitPage) -> String{
        return "CALL `sp_eventsUpcomingPastGet`( \(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), '\(type.rawValue)', '\(lat)', '\(lng)', \(page), \(limit) ,'\(FrenzyPartnerID.partnerID)')"
    }
    
    static func getEventDetailsFromMomentorDelighter( id : Int , getType : String) -> String{
           return "CALL `sp_getMomentOrDelighterDetailById`('\(id)', '\(getType)',\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0))"
       }
    static func eventDetailById(eventId:Int?) -> String{
        return "CALL `sp_eventDetailByIdGet`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0))"
    }
    static func eventSettingUpdate(eventId:Int?,favTeamId:Int?,isVibrate:Bool,isFlash:Bool,isScreen:Bool) -> String{
        return "CALL `sp_eventSettingUpdate`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0), \(favTeamId ?? 0), \(isVibrate ? 1 : 0), \(isFlash ? 1 : 0),  \(isScreen ? 1 : 0) )"
    }
    
    static func updateNotifcation()-> String{
        return "CALL `sp_notiCountUpdate`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0),  0,'ios')"
    }
    static func updateFrenzySticker( state : String? = "" , zip : String? = "",  city : String? = "" , adress1 : String? = "" , address2 : String? = "") -> String{
        return "CALL  `sp_requestSticker`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0),  '\(state ?? "")',  '\(zip ?? "")' , '\(city ?? "")',  '\(adress1 ?? "")',  '\(address2 ?? "")' )"
    }
    
    static func eventTicketUpdate(eventId:Int?,section:Int?,level:String? = "" ,row:String = "",seatNo:Int = 0, isVenu:Int) -> String{
        return "CALL `sp_eventTicketUpdate`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0), \(section ?? 0), '\(level ?? "0")','\(row)', \(seatNo) ,'\(isVenu)')"
    }
    
    static func eventSectionGet(eventId:Int?) -> String{
        return "CALL `sp_eventSectionsGet`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0))"
    }
    
    static func eventDetailGet(eventId:Int?) -> String {
        let path = "CALL `sp_eventDetailByIdGet`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0))"
        return path
    }
    //Call `sp_eventPostsGetWithMoment`( Viewer_id INT, EventId INT)
    static func eventPostsGet(eventId:Int?) -> String {
        return "CALL `sp_eventPostsGet`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0))"
    }
    static func eventPostsGetWithMoment(eventId:Int?) -> String {
        print ("CALL `sp_eventPostsGetWithMoment`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0))")
           return "CALL `sp_eventPostsGetWithMoment`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(eventId ?? 0))"
       }
    
    static func commentsGet(delighterId: Int?) -> String {
        return "CALL `sp_commentsGet`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(delighterId ?? 0))"
    }
    
    static func commentSet(delighterId: Int?, comment: String) -> String {
        return "CALL `sp_commentSet`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(delighterId ?? 0), '\(comment.encodeUFT8())')"
    }
    
    static func commentUpdate(commentId: Int, delighterId: Int?, comment: String) -> String {
        return "CALL `sp_commentUpdate`(\(commentId),\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(delighterId ?? 0), '\(comment.encodeUFT8())')"
    }
    
    static func commentDelete(commentId: Int, delighterId: Int?) -> String {
        return "CALL `sp_commentDelete`(\(commentId),\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(delighterId ?? 0))"
    }
    
    //
}
enum SearchOrSuggestion :String {
    case SEARCH = "search"
    case SUGGESTION = "suggetion"
}

internal struct FormatParameterKeys{
    //Paramaters
    static let email = "email"
    static let phone = "phone"
    static let name = "name"
    static let password = "password"
    static let address1 = "address1"
    static let address2 = "address2"
    static let city = "city"
    static let zip = "zip"
    static let state = "state"
    static let id = "id"
    static let oldPassword = "oldPassword"
    static let preferencesId = "preferencesId"
    
    //DB Param
    static let model = "model"
    static let dataBaseURL = "h"
    static let dataBaseName = "d"
    static let dataBaseUserName = "u"
    static let dataBasePassword = "p"
    static let dataBaseQuery = "e"
    
    //Upload Image Params
    static let viewer_id = "viewer_id"
    static let partner_id = "partner_id"
    static let partner_user_id = "partner_user_id"
    static let access_key = "access_key"
    static let token = "deviceToken"
    static let isNotification = "isNotification"
    static let deviceId = "deviceId"
}

internal struct APIParameterConstants {
    // Param Array System
    struct Registration {
        static let validate_viewer = [FormatParameterKeys.email,FormatParameterKeys.phone]
        static let register_viewer =
            
            [FormatParameterKeys.email,FormatParameterKeys.phone,FormatParameterKeys.name,FormatParameterKeys.address1,FormatParameterKeys.address2,FormatParameterKeys.city,FormatParameterKeys.state,FormatParameterKeys.zip]
       
        static let new_RegisterViewer =
            
            [FormatParameterKeys.email,FormatParameterKeys.phone,FormatParameterKeys.name, FormatParameterKeys.password , FormatParameterKeys.preferencesId, FormatParameterKeys.address1,FormatParameterKeys.address2,FormatParameterKeys.city,FormatParameterKeys.state,FormatParameterKeys.zip]
        
        static let login = [FormatParameterKeys.email, FormatParameterKeys.password]
        static let sdkLogin = [FormatParameterKeys.email, FormatParameterKeys.partner_id , FormatParameterKeys.partner_user_id , FormatParameterKeys.name , FormatParameterKeys.access_key, FormatParameterKeys.token , FormatParameterKeys.isNotification , FormatParameterKeys.deviceId]

        static let forgotPassword = [FormatParameterKeys.email]
        static let changePassword = [FormatParameterKeys.id , FormatParameterKeys.oldPassword, FormatParameterKeys.password]
        
    }
    struct Query{
        static let query = [FormatParameterKeys.dataBaseURL, FormatParameterKeys.dataBaseName, FormatParameterKeys.dataBaseQuery, FormatParameterKeys.dataBasePassword, FormatParameterKeys.dataBaseUserName]
    }

    struct Atteendee{
           static let attende = ["viewer_id", "event_id", "team_id", "is_vibrate", "is_flash", "is_screen"]
       }
       
    struct UploadImage{
        static let upload_profile_image = [FormatParameterKeys.viewer_id]
        static let uploadVideo = ["viewer_id","moment_id","ratio","length","source"]
        static let likeUnlikeReport = ["viewer_id","video_id"]
    }
    
}


