//
//  SearchLocationModel.swift
//  Frenzy
//
//  Created by CP on 1/27/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation
import SearchTextField

class SearchLocationModel : SearchTextFieldItem {
    var placeId:String
    
    init(title: String, subtitle: String?,placeId:String) {
        self.placeId = placeId
        super.init(title: title, subtitle: subtitle)
    }
}
