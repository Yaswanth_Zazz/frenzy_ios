
import UIKit

class EventItem: Codable , Equatable{
    
    var subscriptionStatus: SubscriptionStatus {
        if subscription_status ?? "" == SubscriptionStatus.PENDING.rawValue {
            return SubscriptionStatus.PENDING
        } else if subscription_status ?? "" == SubscriptionStatus.COMPLETED.rawValue {
            return SubscriptionStatus.COMPLETED
        } else {
            return SubscriptionStatus.NONE
        }
    }
    
    var eventStatus : EventStatus {
        switch event_status ?? "" {
        case EventStatus.UPCOMING.rawValue:
            return EventStatus.UPCOMING
        case EventStatus.ONGOING.rawValue:
            return EventStatus.ONGOING
        case EventStatus.ENDED.rawValue:
            return EventStatus.ENDED
        default:
            return EventStatus.UPCOMING
        }
    }
    
    var eventCategory:EventCategory?{
        get {
            return event_category == EventCategory.MATCH.rawValue ? EventCategory.MATCH: EventCategory.EVENT
        }
    }
    var time: String?{
        get {
            return event_timestamp?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "hh:mm a")//2020-02-20 16:30:12
        }
    }
    var date: String?{
        get {
            return event_timestamp?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "MMM dd")//2020-02-20 16:30:12
        }
    }
    var day: String?{
        get {
            return event_timestamp?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "EEE")//2020-02-20 16:30:12
        }
    }
    var remainingEventSeconds: Int? {
        get {
            return event_timestamp?.getRemainingSeconds(inputFormat: "yyyy-MM-dd HH:mm:ss")
        }
    }
    var place: String?{
        get {
            return loc_title
        }
    }
    var location: String?{
        get {
            return "\(loc_address1 ?? "")"
        }
    }
    var teamOne: String? {
        get {
            return team1_title
        }
    }
    var teamTwo: String? {
        get {
            return team2_title
        }
    }
    var eventName: String? {
        get {
            return title
        }
    }
    var eventPlace: String? {
        get {
            return loc_title
        }
    }
    var zipCode: String? {
        get {
            return loc_zip?.trimmingCharacters(in: CharacterSet.whitespaces)
        }
    }
    
    var canFlash: Bool {
        get {
            return is_flash == 0 ? false : true
        }
    }
    
    var canVibrate: Bool {
        get{
            return is_vibrate == 0 ? false : true
        }
    }
    
    var canChangeScreen: Bool {
        get {
            return is_screen == 0 ? false : true
        }
    }
    
    var topColor : String {
        get{
            guard let color = top_color, !color.isEmpty else {
                return "1C0E44"
            }
            return String(color.dropFirst())
        }
    }
    
    var bottomColor : String {
        get{
            guard let color = bottom_color, !color.isEmpty else {
                return "8CC63F"
            }
            return String(color.dropFirst())
        }
    }
    
    var isGroundEvent : Bool {
        get {
            if self.category == "remote" {
                return false
            }
            return true
        }
    }
    
    var isGenralevent : Bool {
        get{
            if event_type == "General Admission Event" || seating == "general"{
                return true
            }
            return false
        }
    }
    
    var isAssignedevent : Bool {
        get{
            if event_type != "General Admission Event" && seating == "assigned"{
                return true
            }
            return false
        }
    }
    
    
    var is_joined:Int? = 0
    var colorCode: String? = ""
    var id: Int? = 0
    var sponsor_image : String? = ""
    var sponsor_link : String? = ""
    var loc_address2 : String? = ""
    var loc_address1 : String? = ""
    var event_timestamp : String? = ""
    var loc_city : String? = ""
    var user_id : Int? = 0
    var loc_zip : String? = ""
    var loc_lat : Double? = 0.0
    var title : String? = ""
    var updated_at : String? = ""
    var loc_state : String? = ""
    var loc_lng : Double? = 0.0
    var location_id : Int? = 0
    var second_half : Int? = 0
    var event_category : String? = ""
    var loc_title : String? = ""
    var distance : Double? = 0.0
    var total_sections : Int? = 0
    var event_type : String? = ""
    var created_at : String? = ""
    var first_half : Int? = 0
    var ground_type : String? = ""
    var total_seats : Int? = 0
    var team1_logo : String? = ""
    var team2_id : Int? = 0
    var team1_id : Int? = 0
    var team2_logo : String? = ""
    var team1_title : String? = ""
    var team2_title : String? = ""
    var subscription_status : String? = ""
    var event_status: String? = ""
    var team1_goals : Int? = 0
    var team2_goals : Int? = 0
    var my_section : Int? = 0
    var section_id : Int? = 0
    var total_viewers : Int? = 0
    var is_flash : Int? = 0
    var is_vibrate : Int? = 0
    var is_screen : Int? = 0
    var top_color: String? = "#1C0E44"
    var bottom_color: String? = "#8CC63F"
    var isGenralSeats: Bool? = true
    var seats_per_row : Int? = 0
    var level_order : String?  = ""
    var rows_per_level : Int? = 0
    var levels : Int? = 0
    var category : String? = ""
    var seating : String? = ""
    var password: String? = ""
    var section_title: String? = ""
    var show_score : Int? = 0
    var nba_status_text:String? = ""
    var is_nba:String? = ""
    var nba_game_code:String? = ""
    
    enum CodingKeys: String, CodingKey {
        
        case is_joined = "is_joined"
        case colorCode = "colorCode"
        case sponsor_image = "sponsor_image"
        case sponsor_link = "sponsor_link"
        case loc_address2 = "loc_address2"
        case loc_address1 = "loc_address1"
        case event_timestamp = "event_timestamp"
        case loc_city = "loc_city"
        case user_id = "user_id"
        case loc_zip = "loc_zip"
        case loc_lat = "loc_lat"
        case title = "title"
        case updated_at = "updated_at"
        case loc_state = "loc_state"
        case loc_lng = "loc_lng"
        case location_id = "location_id"
        case second_half = "second_half"
        case event_category = "event_category"
        case loc_title = "loc_title"
        case distance = "distance"
        case total_sections = "total_sections"
        case event_type = "event_type"
        case created_at = "created_at"
        case first_half = "first_half"
        case ground_type = "ground_type"
        case total_seats = "total_seats"
        case team1_logo = "team1_logo"
        case team2_id = "team2_id"
        case team1_id = "team1_id"
        case team2_logo = "team2_logo"
        case team1_title = "team1_title"
        case team2_title = "team2_title"
        case subscription_status = "subscription_status"
        case event_status = "event_status"
        case team1_goals = "team1_goals"
        case team2_goals = "team2_goals"
        case my_section = "my_section"
        case section_id = "section_id"
        case total_viewers = "total_viewers"
        case is_flash = "is_flash"
        case is_vibrate = "is_vibrate"
        case is_screen = "is_screen"
        case top_color = "top_color"
        case bottom_color = "bottom_color"
        case isGenralSeats = "isGenralSeats"
        case seats_per_row = "seats_per_row"
        case level_order = "level_order"
        case rows_per_level = "rows_per_level"
        case levels = "levels"
        case category = "category"
        case seating = "seating"
        case password = "password"
        case id = "id"
        case show_score = "show_score"
        case nba_status_text = "nba_status_text"
        case is_nba = "is_nba"
        case nba_game_code = "nba_game_code"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        sponsor_image = try values.decodeIfPresent(String.self, forKey: .sponsor_image)
        bottom_color = try values.decodeIfPresent(String.self, forKey: .bottom_color)
        isGenralSeats = try values.decodeIfPresent(Bool.self, forKey: .isGenralSeats)
        seats_per_row = try values.decodeIfPresent(Int.self, forKey: .seats_per_row)
        level_order = try values.decodeIfPresent(String.self, forKey: .level_order)
        rows_per_level = try values.decodeIfPresent(Int.self, forKey: .rows_per_level)
        levels = try values.decodeIfPresent(Int.self, forKey: .levels)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        seating = try values.decodeIfPresent(String.self, forKey: .seating)
        team1_logo = try values.decodeIfPresent(String.self, forKey: .team1_logo)
        team2_id = try values.decodeIfPresent(Int.self, forKey: .team2_id)
        team1_id = try values.decodeIfPresent(Int.self, forKey: .team1_id)
        team2_logo = try values.decodeIfPresent(String.self, forKey: .team2_logo)
        team1_title = try values.decodeIfPresent(String.self, forKey: .team1_title)
        team2_title = try values.decodeIfPresent(String.self, forKey: .team2_title)
        subscription_status = try values.decodeIfPresent(String.self, forKey: .subscription_status)
        event_status = try values.decodeIfPresent(String.self, forKey: .event_status)
        team1_goals = try values.decodeIfPresent(Int.self, forKey: .team1_goals)
        team2_goals = try values.decodeIfPresent(Int.self, forKey: .team2_goals)
        my_section = try values.decodeIfPresent(Int.self, forKey: .my_section)
        section_id = try values.decodeIfPresent(Int.self, forKey: .section_id)
        total_viewers = try values.decodeIfPresent(Int.self, forKey: .total_viewers)
        is_flash = try values.decodeIfPresent(Int.self, forKey: .is_flash)
        is_vibrate = try values.decodeIfPresent(Int.self, forKey: .is_vibrate)
        is_screen = try values.decodeIfPresent(Int.self, forKey: .is_screen)
        top_color = try values.decodeIfPresent(String.self, forKey: .top_color)
        colorCode = try values.decodeIfPresent(String.self, forKey: .colorCode)
        sponsor_link = try values.decodeIfPresent(String.self, forKey: .sponsor_link)
        loc_address2 = try values.decodeIfPresent(String.self, forKey: .loc_address2)
        event_timestamp = try values.decodeIfPresent(String.self, forKey: .event_timestamp)
        loc_city = try values.decodeIfPresent(String.self, forKey: .loc_city)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        loc_zip = try values.decodeIfPresent(String.self, forKey: .loc_zip)
        loc_lat = try values.decodeIfPresent(Double.self, forKey: .loc_lat)
        loc_lng = try values.decodeIfPresent(Double.self, forKey: .loc_lng)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        loc_state = try values.decodeIfPresent(String.self, forKey: .loc_state)
        location_id = try values.decodeIfPresent(Int.self, forKey: .location_id)
        second_half = try values.decodeIfPresent(Int.self, forKey: .second_half)
        event_category = try values.decodeIfPresent(String.self, forKey: .event_category)
        loc_title = try values.decodeIfPresent(String.self, forKey: .loc_title)
        distance = try values.decodeIfPresent(Double.self, forKey: .distance)
        total_sections = try values.decodeIfPresent(Int.self, forKey: .total_sections)
        event_type = try values.decodeIfPresent(String.self, forKey: .event_type)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        first_half = try values.decodeIfPresent(Int.self, forKey: .first_half)
        ground_type = try values.decodeIfPresent(String.self, forKey: .ground_type)
        total_seats = try values.decodeIfPresent(Int.self, forKey: .total_seats)
        is_joined = try values.decodeIfPresent(Int.self, forKey: .is_joined)
        show_score = try values.decodeIfPresent(Int.self, forKey: .show_score)
        nba_status_text = try values.decodeIfPresent(String.self, forKey: .nba_status_text)
        is_nba = try values.decodeIfPresent(String.self, forKey: .is_nba)
        nba_game_code = try values.decodeIfPresent(String.self, forKey: .nba_game_code)
        
        do {
            if let address = try values.decodeIfPresent(String.self, forKey: .loc_address1){
                loc_address1 = address
            }
        } catch {
            if let address = try values.decodeIfPresent(Int.self, forKey: .loc_address1){
                loc_address1 = "\(address)"
            }
        }
        
        do {
            if let pwd = try values.decodeIfPresent(String.self, forKey: .password){
                password = pwd
            }
        } catch {
            if let pwd = try values.decodeIfPresent(Int.self, forKey: .password){
                password = "\(pwd)"
            }
        }
    }
    
    init() {
        
    }
    
    static func == (lhs: EventItem, rhs: EventItem)  -> Bool{
        return lhs.id == rhs.id && lhs.eventName == rhs.eventName
    }
}
