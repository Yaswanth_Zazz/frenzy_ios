//
//  Delighters.swift
//  Frenzy
//
//  Created by CodingPixel on 27/02/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation

class Delighter: Codable {
    
    var postTime: String? {
        get{
            return time?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "hh:mm a")
        }
    }
    
    var remainingSeconds: Int? {
        get{
            return time?.getRemainingSeconds(inputFormat: "yyyy-MM-dd HH:mm:ss")
        }
    }
    
    var eventHalf: EventHalf? {
        get {
            return event_half == EventHalf.FIRST.rawValue ? EventHalf.FIRST : EventHalf.SECOND
        }
    }
    
    var id: Int? = 0
    var ratio: Double? = 1.0
    var imageType: String? = ""
    var image: String? = ""
    var title: String = ""
    var description: String = ""
    var event_half: String? = ""
    var time: String? = ""
    var isLeft:Bool = false
    var sections : String = ""
    var event_id : String = "0"
    var isForFarAway = "0"
    var isGround = "0"
    var event_type :String? = ""
    var bottom_color : String?
    
    func initDelighter(from json: [String: Any]) -> Delighter{
        let delighter = Delighter()
        delighter.id = json["id"] as? Int ?? 0
        if delighter.id == 0 {
            delighter.id = Int(json["id"] as? String ?? "0")
        }
        delighter.ratio = json["ratio"] as? Double ?? 1.0
        delighter.image = json["image"] as? String ?? ""
        delighter.title = json["title"] as? String ?? ""
        delighter.imageType = json["image_type"] as? String ?? ""
        delighter.event_half = json["event_half"] as? String ?? ""
        delighter.time = json["created_at"] as? String ?? ""
        delighter.description = json["description"] as? String ?? ""
        delighter.sections = json["sections"] as? String ?? ""
        delighter.event_id = json["event_id"] as? String ?? "\(json["event_id"] as? Int ?? 0)"
        delighter.event_type = json["event_type"] as? String ?? ""
        
        delighter.isForFarAway = json["is_far_away"] as? String ?? "\(json["is_far_away"] as? Int ?? 0)"
        delighter.isGround = json["is_ground"] as? String ?? "\(json["is_ground"] as? Int ?? 0)"
        delighter.bottom_color = json["bottom_color"] as? String ?? ""
        
        return delighter
    }
}

extension String {
  
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let encodedData = self.data(using: String.Encoding.utf8) else {return nil}
        do {
            return try NSMutableAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
      
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
       
        } catch {
            print("error")
            return nil
        }
    }
    
    var htmlToString: String? {
        guard let encodedData = self.data(using: String.Encoding.utf8) else {return nil}
        return  String(data: encodedData, encoding: .utf8)//htmlToAttributedString?.string ?? ""
    } 

}

extension NSMutableAttributedString {

    func trimmedAttributedString(set: CharacterSet) -> NSMutableAttributedString {

        let invertedSet = set.inverted

        var range = (string as NSString).rangeOfCharacter(from: invertedSet)
        let loc = range.length > 0 ? range.location : 0

        range = (string as NSString).rangeOfCharacter(
                            from: invertedSet, options: .backwards)
        let len = (range.length > 0 ? NSMaxRange(range) : string.count) - loc

        let r = self.attributedSubstring(from: NSMakeRange(loc, len))
        return NSMutableAttributedString(attributedString: r)
    }
}
