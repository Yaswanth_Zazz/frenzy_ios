//
//  Movement.swift
//  Frenzy
//
//  Created by CodingPixel on 28/02/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation

class Movement {
    
    var style: MovementStyle {
        get {
            if movement_style == MovementStyle.FLASH.rawValue {
                return .FLASH
            }
            else if movement_style == MovementStyle.THROB.rawValue {
                return .THROB
            }
            else if movement_style == MovementStyle.FLOW.rawValue {
                return .FLOW
            }
            else if movement_style == MovementStyle.WAVE.rawValue {
                return .WAVE
            }
            else if movement_style == MovementStyle.FRENZY.rawValue {
                return .FRENZY
            }
            else if movement_style == MovementStyle.ANIMATE.rawValue {
                return .ANIMATE
            }
            return .FLASH
        }
    }
    
    var color : String {
        get{
            if !bgColor.isEmpty {
                if bgColor.contains("#"){
                    return String(bgColor.dropFirst())
                }else{
                    return String(bgColor)
                }
            }
            return ""
        }
    }
    
    var colorTitle: String {
        get{
            if !titleColor.isEmpty {
                if titleColor.contains("#"){
                    return String(titleColor.dropFirst())
                }else{
                    return String(titleColor)
                }
            }
            return "ffffff"
        }
    }
    var isLive : Bool {
        get{
            if self.is_terminated == 1{
                return false
            }
            let currentTime  = Date().toLocalTime().toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
            
            let Dateformatter = DateFormatter()

            Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //convert string to date
            let datenew = Dateformatter.date(from: currentTime)!

            let dateold = Dateformatter.date(from: self.updated_at)!.toLocalTime()
             //use default datecomponents with two dates
             let calendar1 = Calendar.current
             let components = calendar1.dateComponents([.year,.month,.day,.hour,.minute,.second], from:  dateold, to:   datenew)
             let seconds = components.second
             let minuts = components.minute
            let hours = components.hour
            var totalSeccornds  = seconds! + (minuts!*60)
            totalSeccornds = totalSeccornds + (hours!*60*60)
            print("Calculated Seconds: \(totalSeccornds ), Duration : \(duration)")
            if totalSeccornds < duration {
                return true
            }
            return false
        }
    }
    
    var remainingDuration : Int {
        get{
            let currentTime  = Date().toLocalTime().toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
            
            let Dateformatter = DateFormatter()

            Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //convert string to date
            let datenew = Dateformatter.date(from: currentTime)!

            let dateold = Dateformatter.date(from: self.updated_at)!.toLocalTime()
             //use default datecomponents with two dates
             let calendar1 = Calendar.current
             let components = calendar1.dateComponents([.year,.month,.day,.hour,.minute,.second], from:  dateold, to:   datenew)
             let seconds = components.second
             let minuts = components.minute
            let hours = components.hour
            var totalSeccornds  = seconds! + (minuts!*60)
            totalSeccornds = totalSeccornds + (hours!*60*60)
            return self.duration - totalSeccornds
        }
    }
    
    var eventId = 0
    var movement_style = ""
    var imageType = ""
    var image = ""
    var sections = ""
    var isVibration = 0
    var bgColor = "ffffff"
    var isFlash = 0
    var isScreen = 0
    var date = ""
    var flashSpeed = 0
    var isStyle = 0
    var duration = -1
    var title = ""
    var titleColor = "ffffff"
    var id = 0
    var isLeft = true
    var isForFarAway = 0
    var isGround = 0
    var updated_at = ""
    var is_terminated = 0
    var video_count = 0
    var moment_url = ""
    var is_scroll = 1

    ///Here Adding Extra Key
    var event_type :String? = ""
    
    func initMovementFrom(json: [String:Any]) -> Movement {
        let movement = Movement()
        movement.id = json["id"] as? Int ?? 0
        movement.event_type = json["event_type"] as? String ?? ""
        movement.eventId = json["event_id"] as? Int ?? 0
        movement.movement_style = json["style"] as? String ?? ""
        movement.image = json["image"] as? String ?? ""
        movement.sections = json["sections"] as? String ?? ""
        movement.bgColor = json["color"] as? String ?? ""
        movement.imageType = json["image_type"] as? String ?? ""
        movement.isFlash = json["is_flash"] as? Int ?? 0
        movement.isVibration = json["is_vibrate"] as? Int ?? 0
        movement.flashSpeed = json["flash_speed"] as? Int ?? -1
        movement.isScreen = json["is_screen"] as? Int ?? 0
        movement.isStyle = json["is_style"] as? Int ?? 0
        movement.isForFarAway = json["is_far_away"] as? Int ?? 0
        movement.isGround = json["is_ground"] as? Int ?? 0
        movement.is_terminated = json["is_terminated"] as? Int ?? 0
        movement.video_count = json["video_count"] as? Int ?? 0
        movement.date = json["created_at"] as? String ?? ""
        movement.moment_url = json["moment_url"] as? String ?? ""
    
        if let updatedDate = json["updated_at"] as? String, !updatedDate.isEmpty{
            movement.updated_at = updatedDate
        }else{
            movement.updated_at = json["created_at"] as? String ?? ""
        }
        
        if let isScroll = json["is_scroll"] as? Int {
            movement.is_scroll = isScroll
        }else if let isScroll = json["is_scroll"] as? String{
            movement.is_scroll = isScroll == "true" ? 1 : 0
        }

        if movement.flashSpeed == -1 {
            let dura = json["flash_speed"] as? String ?? "0"
            movement.flashSpeed = Int(dura) ?? -1
        }
        
        movement.duration = json["duration"] as? Int ?? -1
        if movement.duration == -1 {
            let dura = json["duration"] as? String ?? "0"
            movement.duration = Int(dura) ?? -1
        }
        movement.title = json["title"] as? String ?? ""
        movement.titleColor = json["title_color"] as? String ?? "ffffff"
        return movement
    }
    
    func initMovementFromNotification(json: [String:Any]) -> Movement {
        let movement = Movement()
        movement.id = Int(json["id"] as? String ?? "0")!
        movement.eventId = Int(json["event_id"] as? String ?? "0")!
        movement.event_type = json["event_type"] as? String ?? ""
        movement.movement_style = json["style"] as? String ?? ""
        movement.updated_at = json["created_at"] as? String ?? ""//json["updated_at"] as? String ?? ""
        movement.image = json["image"] as? String ?? ""
        movement.sections = json["sections"] as? String ?? ""
        movement.bgColor = json["color"] as? String ?? ""
        movement.video_count = json["video_count"] as? Int ?? 0
        movement.imageType = json["image_type"] as? String ?? ""
        movement.moment_url = json["moment_url"] as? String ?? ""
      
        if let isScroll = json["is_scroll"] as? Int {
            movement.is_scroll = isScroll
        }else if let isScroll = json["is_scroll"] as? String{
            movement.is_scroll = isScroll == "true" ? 1 : 0
        }
        
        let isFlshOn = json["is_flash"] as? String ?? "false"
        if isFlshOn == "true" {
            movement.isFlash =  1
        }else{
            movement.isFlash = 0
        }
        
        let is_vibrate = json["is_vibrate"]  as? String ?? "false"
        if is_vibrate == "true"{
            movement.isVibration =  1
        }else{
            movement.isVibration = 0
        }
        
        
        let isFarAway = json["is_far_away"]  as? String ?? "false"
        if isFarAway == "true"{
            movement.isForFarAway =  1
        }else{
            movement.isForFarAway = 0
        }
        
        let isGro = json["is_ground"]  as? String ?? "false"
        if isGro == "true"{
            movement.isGround =  1
        }else{
            movement.isGround = 0
        }
        let is_screen = json["is_screen"]  as? String ?? "false"
        if is_screen == "true"{
            movement.isScreen =  1
        }else{
            movement.isScreen = 0
        }
        
        let is_style = json["is_style"]  as? String ?? "false"
        if is_style == "true"{
            movement.isStyle =  1
        }else{
            movement.isStyle = 0
        }
    
        movement.flashSpeed = json["flash_speed"] as? Int ?? -1
        
        if movement.flashSpeed == -1 {
            let dura = json["flash_speed"] as? String ?? "0"
            movement.flashSpeed = Int(dura) ?? -1
        }
        
        movement.duration = json["duration"] as? Int ?? -1
        if movement.duration == -1 {
            let dura = json["duration"] as? String ?? "0"
            movement.duration = Int(dura) ?? -1
        }
        movement.title = json["title"] as? String ?? ""
        movement.titleColor = json["title_color"] as? String ?? "ffffff"
        return movement
    }
}
