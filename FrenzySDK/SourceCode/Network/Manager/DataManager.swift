//
//  DataManager.swift
//  Frenzy
//
//  Created by CP on 1/20/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation

import UIKit
import MapKit

class DataManager {
    
    var user: User?{
        set{
            self.saveUserPermanentally(newValue)
        }
        get{
            self.getPermanentlySavedUser()
        }
    }
    
    func saveGiftData(gifDataArray: [GifModel]){
        do {
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: gifDataArray, requiringSecureCoding: false)
            UserDefaults.standard.set(encodedData, forKey: kPrefKeyGifData)
        }catch{
            print(error as Any)
        }
    }
    
    func getGiftArray() -> [GifModel]{
        do {
            if let data = UserDefaults.standard.data(forKey: kPrefKeyGifData),
                let giftData = try NSKeyedUnarchiver.unarchivedObject(ofClasses: [GifModel.self], from: data) as? [GifModel] {
                return giftData
            } else {
                return []
            }
        }catch{
            print(error as Any)
            return []
        }
    }
    
    func saveAdditionalDataForAnimateMoment(dataArray: [AdditionalInfoAnimateMoment]){
        let encodedData = try? JSONEncoder().encode(dataArray)
        UserDefaults.standard.set(encodedData, forKey: KPrefKeyAdditionalInfoAnimateMoment)
    }
    
    func getAdditionalDataForAnimateMoment() -> [AdditionalInfoAnimateMoment]{
        if let data = UserDefaults.standard.data(forKey: KPrefKeyAdditionalInfoAnimateMoment),
            let patientData = try? JSONDecoder().decode([AdditionalInfoAnimateMoment].self, from: data) {
            return patientData
        } else {
            return []
        }
    }
    
    func saveFarAwayEvents(dataArray: [Int]){
           let encodedData = try? JSONEncoder().encode(dataArray)
           UserDefaults.standard.set(encodedData, forKey: KPrefKeyFarAwayEvents)
       }
       
       func getFarAwayEvents() -> [Int]{
           if let data = UserDefaults.standard.data(forKey: KPrefKeyFarAwayEvents),
               let patientData = try? JSONDecoder().decode([Int].self, from: data) {
               return patientData
           } else {
               return []
           }
       }
    
    func saveGenralEvents(dataArray: [Int]){
        let encodedData = try? JSONEncoder().encode(dataArray)
        UserDefaults.standard.set(encodedData, forKey: KPrefKeyGenralEvents)
    }
    
    func getGenralEvents() -> [Int]{
        if let data = UserDefaults.standard.data(forKey: KPrefKeyGenralEvents),
            let patientData = try? JSONDecoder().decode([Int].self, from: data) {
            return patientData
        } else {
            return []
        }
    }
    
    
    func saveUserPermanentally(_ item:User?) {
        if item != nil {
            let encodedData = try? JSONEncoder().encode(item)
            UserDefaults.standard.set(encodedData, forKey: kPrefKeyUser)
        }
    }
    
    func getPermanentlySavedUser() -> User? {
        if let data = UserDefaults.standard.data(forKey: kPrefKeyUser),
            let userData = try? JSONDecoder().decode(User.self, from: data) {
            return userData
        } else {
            return nil
        }
    }
    
    var deviceToken:String = UIDevice.current.identifierForVendor!.uuidString
    
    static let sharedInstance = DataManager()
    
    func logoutUser() {
        user = nil
        self.resetDefaults()
    }
    
    func resetDefaults() {
       // let bundle = Bundle(identifier: "com.FrenzySDK")
      //  guard let appDomain = Bundle.main.bundleIdentifier else {return}
        UserDefaults.standard.removePersistentDomain(forName: "com.FrenzySDK")//appDomain)
        UserDefaults.standard.synchronize()
        
       /* let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        } */
    }
    
    func setSuccessPopUpShown(isShown : Bool = false){
        UserDefaults.standard.set(isShown, forKey: kIsShownSuccess)
    }
    
    func isSuccessPopUpShown() -> Bool{
        return UserDefaults.standard.bool(forKey: kIsShownSuccess)
    }
    
    
    func setlocationSettingON(isShown : Bool = false){
           UserDefaults.standard.set(isShown, forKey: "setlocationSettingON")
       }
       
       func islocationSettingON() -> Bool{
           return UserDefaults.standard.bool(forKey: "setlocationSettingON")
       }
       
    func setIntroScreenShown(isShown : Bool = false){
           UserDefaults.standard.set(isShown, forKey: "setIntroScreenShown")
       }
       
       func isIntroScreenNotShown() -> Bool{
           return UserDefaults.standard.bool(forKey: "setIntroScreenShown")
       }
    
    
    func setUserLogout(isLogout: Bool = false){
        UserDefaults.standard.set(isLogout, forKey: "isUserLogout")
    }
    
    func getUserLogout()->Bool{
        return UserDefaults.standard.bool(forKey: "isUserLogout")
    }
}
