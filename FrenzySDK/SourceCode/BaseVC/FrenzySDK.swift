//
//  FrenzySDK.swift
//
//
//  Created by Nandini on 20/12/21.
//

import Foundation
import UIKit

public protocol FrenzySDKDelegate: NSObject {
    func frenzyDidEndSDK()
}

public class FrenzySDK: NSObject {
    
    public static let shared = FrenzySDK()
    public var delegate: FrenzySDKDelegate?
    private var rootVC: UIViewController?
    
    public func frenzySDK_SetThemeColor(hex ColorStr: String){
        FrenzyColor.themeColor = UIColor.init(hexColor: ColorStr)
    }
    
    public func frenzySDK_SetPartnerId(partner Id : String , acessKey: String){
        FrenzyPartnerID.partnerID = Id
        FrenzyPartnerID.partnerAccessKey = acessKey
    }
    
    public func frenzyVerifyUser(controller: UIViewController, partnerUserID: String, partnerId: String, accesskey:String, userEmailId:String, name: String , deviceToken: String , isNotification: Int, deviceId: String, completion: @escaping (Bool) -> ()) {
        FrenzyPartnerID.partnerID = partnerId
        FrenzyPartnerID.partnerAccessKey = accesskey
        callForUser(userEmailId, partnerId: partnerId, partner_user_id: partnerUserID, name: name , deviceToken : deviceToken, isNotification: isNotification, deviceId: deviceId) { success in
            completion(success)
        }
    }
    
    public func frenzyOpenHomePage(controller: UIViewController, userID: String, partnerId: String, userEmailId:String , isFromNotification:Bool = false , notiData: [String:Any]?) {
        let homeVC =  HomeVC.instantiate(fromAppStoryboard: .home)
        homeVC.isfromNotification = isFromNotification
        homeVC.notiData = notiData
        rootVC = controller
        controller.navigationController?.navigationBar.isHidden = true
        controller.navigationController?.pushViewController(homeVC, animated: true)
    }

    private func callForUser(_ email: String = "", partnerId: String = "0" ,  partner_user_id: String = "" , name: String = "" , deviceToken: String, isNotification: Int, deviceId: String, completion: @escaping (Bool)->()){
        //
        APIManager.sharedInstance.opertationWithRequest(withApi: API.sdkLogin(email: email, partner_id: partnerId, partner_user_id: partner_user_id, name: name , token: deviceToken , isNotification: isNotification , deviceId: deviceId)) { (ApiResponse) in
            switch ApiResponse {
           
            case .Failure(let error):
                print("error:\(error ?? "")")
                completion(false)
                
            case .Success(let data):
                
                print(data as Any)
                if let result = data?.result as? RegistrationApiModel {
                        let user = User()
                        user.userItem = result
                        DataManager.sharedInstance.user = user
                        isLogOutCalled = false
                        DataManager.sharedInstance.setUserLogout(isLogout: false)
                        DataManager.sharedInstance.saveUserPermanentally(user)
                        self.creatDemoEventAPi()
                        completion(true)
                }
                else{
                    completion(false)
                    print("Error Result: \(data?.result as? Int ?? 0)")
                }
            }
        }
    }
    
    private func creatDemoEventAPi(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.demoEventCreate(viewer_id: "\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0)")) { (ApiResponse) in
            switch ApiResponse {
            case .Failure(let error):
                print("demo event error:\(error ?? "")")
            case .Success(let data):
                print("demo event api:\(String(describing: data))")
            }
        }
    }
    
    //MARK:-  Check moment validate Frenzy SDK Notification
    private func showMoment(data : [String : Any] , notificationEventId:String) {
        
        if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last as? BaseViewController {
            let m = Movement().initMovementFromNotification(json: data)
            lastVC.showLoading(on: lastVC.view, of: FrenzyColor.white)
            APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: "CALL sp_checkIfMomentTerminatedWithoutVidCount(\(m.id))"), completion: { apiResponse in
    
                switch apiResponse {
                case .Failure(_):
                    lastVC.hideLoading(on: lastVC.view)
                    lastVC.showErrorToast(title: "Moment Terminated!", description: "This moment no longer exist or terminated by Host") { (Bool) in
                        return
                    }
                case .Success(let resultData):
                    lastVC.hideLoading(on: lastVC.view)
                    var isTerminated = 0
                  
                    if let dataobj = resultData?.result?.firstObject as? [String:Any], let is_Terminated = dataobj["is_terminated"] as? Int{
                        isTerminated = is_Terminated
                    }
                    else if let dataobj = resultData?.result?.firstObject as? [String:Any], let is_Terminated = dataobj["is_terminated"] as? String{
                        let intTerminated = (is_Terminated as NSString).integerValue
                        isTerminated = intTerminated
                    }
                    if (resultData?.result?.count)! > 0 && isTerminated == 0 {
                        
                        let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                        let sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
                        let flashArr = UserDefaults.standard.array(forKey: kFlashKey) as? [Bool] ?? [Bool]()
                        let screenArr = UserDefaults.standard.array(forKey: kScreenKey) as? [Bool] ?? [Bool]()
                        let vibrateArr = UserDefaults.standard.array(forKey: kVibrateKey) as? [Bool] ?? [Bool]()
                                            
                        for (index,eventId) in eventIdArr.enumerated() {
                            print("for loop eventId \(eventId)")
                            if eventId == Int(data["event_id"] as? String ?? "0")! {
                                
                                let movement = Movement().initMovementFromNotification(json: data)
                                
                                let nowDuratioin = movement.remainingDuration
                                let userPermission = UserPermissionInfo(flash: flashArr[index], screen: screenArr[index], vibrate: vibrateArr[index])
                                let mySelectedSec = sectionIdArr[index]
                              
                                if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                                    if let baseVc =  lastVC  as?  BaseViewController {
                                        baseVc.openMovementVC(with: movement, userPermission: userPermission , duration: nowDuratioin, eventId: notificationEventId , mySec: mySelectedSec)
                                    }
                                }
                            }
                        }
                    }else{
                        if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                                if let baseVc =  lastVC  as?  BaseViewController {
                                     let movement = Movement().initMovementFromNotification(json: data)
                                    selectedMomentID = movement.id
                                    baseVc.openEventScreen(id: movement.id, type: "moment")
                                }
                        }else{
                            lastVC.showErrorToast(title: "Moment Terminated!", description: "This moment no longer exist or terminated by Host") { (Bool) in
                                                       return
                                                   }
                        }
                       
                    }
                }
            })
        }
    }
}
