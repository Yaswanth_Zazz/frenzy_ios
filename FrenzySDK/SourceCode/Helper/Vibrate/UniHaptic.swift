//
//  UniHaptics.swift
//  UniHaptics
//
//  Created by Foti Dim on 14.12.19.
//  Copyright © 2019 navideck. All rights reserved.
//

import UIKit
import CoreHaptics
import AudioToolbox
import AVFoundation
import Haptico

public enum Style: CaseIterable {
    case selection
    case impact
    case notification
    case custom //Uses CoreHaptics API
}

public struct UniHaptic: Vibrating {
    
    private var engine: Vibrating
    
    public init(style: Style = .selection) {
        engine = {
            if #available(iOS 13.0, *), style == .custom {
                return CoreHapticsWrapper()
            }
            else if #available(iOS 10.0, *) {
                return FeedbackGeneratorWrapper(style: style)
            } else {
                return AudioToolboxWrapper()
            }
        }()
    }
    
    public func vibrate(intensity: Float = 0.7, sharpness: Float = 0.7) {
        engine.vibrate(intensity: intensity, sharpness: sharpness)
    }
}

protocol Vibrating {
    func vibrate(intensity: Float, sharpness: Float)
}

@available(iOS 13.0,*)
struct CoreHapticsWrapper: Vibrating {
    private var hapticEngine: CHHapticEngine?
    
    init() {
        hapticEngine = try? CHHapticEngine()
        hapticEngine?.resetHandler = resetHandler
        hapticEngine?.stoppedHandler = restartHandler
        hapticEngine?.playsHapticsOnly = true
        try? start()
    }
    
    func vibrate(intensity: Float, sharpness: Float) {
        let event = CHHapticEvent(
            eventType: .hapticTransient,
            parameters: [
                CHHapticEventParameter(parameterID: .hapticIntensity, value: intensity),
                CHHapticEventParameter(parameterID: .hapticSharpness, value: sharpness)
            ],
            relativeTime: 0
            ,duration: 2)
        guard let pattern = try? CHHapticPattern(events: [event], parameters: []) else { return }
        do {
            guard let player = try hapticEngine?.makePlayer(with: pattern) else { return }
            do{
                try player.start(atTime: CHHapticTimeImmediate)
            }catch let err {
                print("\(err.localizedDescription)")
            }
        }catch let err{
                print("\(err.localizedDescription)")
            }
    }
    
    public func start() throws {
        try hapticEngine?.start()
    }
    
    private func resetHandler() {
        try? hapticEngine?.start()
    }
    
    private func restartHandler(_ reasonForStopping: CHHapticEngine.StoppedReason? = nil) {
        resetHandler()
    }
}

@available(iOS 10.0,*)
struct FeedbackGeneratorWrapper: Vibrating {
    var feedbackGenerator: UIFeedbackGenerator
    var style: Style
    
    init(style: Style) {
        func makeFeedbackGenerator(of style: Style) -> UIFeedbackGenerator {
            switch style {
            case .selection:
                return UISelectionFeedbackGenerator()
            case .notification:
                return UINotificationFeedbackGenerator()
            default:
                return UIImpactFeedbackGenerator(style: .medium)
            }
        }
        
        self.style = style
        feedbackGenerator = makeFeedbackGenerator(of: style)
        feedbackGenerator.prepare()
    }
    
    func vibrate(intensity: Float, sharpness: Float) {
        defer {
            feedbackGenerator.prepare()
        }
        
        switch style {
        case .selection:
            (feedbackGenerator as? UISelectionFeedbackGenerator)?.selectionChanged()
        case .notification:
            switch intensity {
            case -Float.infinity..<0.4:
                (feedbackGenerator as? UINotificationFeedbackGenerator)?.notificationOccurred(.success)
            case 0.4..<0.6:
                (feedbackGenerator as? UINotificationFeedbackGenerator)?.notificationOccurred(.warning)
            default:
                (feedbackGenerator as? UINotificationFeedbackGenerator)?.notificationOccurred(.error)
            }
        default:
            switch (intensity, sharpness) {
            case (-Float.infinity...0, _):
                if #available(iOS 13.0, *) {
                    UIImpactFeedbackGenerator(style: .soft).impactOccurred()
                } else {
                    fallthrough
                }
            case (0..<0.4, _):
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
            case (0.4..<0.6, _):
                UIImpactFeedbackGenerator(style: .medium).impactOccurred()
            case (1.0...Float.infinity, _):
                if #available(iOS 13.0, *) {
                    UIImpactFeedbackGenerator(style: .rigid).impactOccurred()
                } else {
                    fallthrough
                }
            case (0.6...1.0, _):
                UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
            default:
                (feedbackGenerator as? UIImpactFeedbackGenerator)?.impactOccurred()
            }
        }
    }
}

struct AudioToolboxWrapper: Vibrating {
    
    func vibrate(intensity: Float, sharpness: Float) {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
}

//            VibrateFrenzy.vibrate(for: 1)
//            FlashFrenzy.flash(for: 3)
class VibrateFrenzy{
    /*Each character in this string represents some specific haptic impact:

    "O" (capital "o") - heavy impact
    "o" - medium impact
    "." - light impact
    "-" - delay which has duration of 0.1 second*/
    
    
    static func vibrate(for interval:Int){
//        Haptico.shared().generateFeedbackFromPattern("..oO-Oo..", delay: 0.1)
        DispatchQueue.global(qos: .background).async {
//            Haptico.shared().generateFeedbackFromPattern("..oO-Oo..", delay: 0.1)
            if interval > 0 {
                for _ in 1...interval{
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    sleep(1)
                }
            }
        }
    }
    
    static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
}

class FlashFrenzy{
    static var increaseBrightness = false
    static let device = AVCaptureDevice.default(for: AVMediaType.video)
    static var queue : DispatchWorkItem?
   
    static func offFlash(){
        queue?.cancel()
        if (device?.hasTorch ?? false){
            do {
                try device?.lockForConfiguration()
                device?.torchMode = AVCaptureDevice.TorchMode.off
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    static func flash(for interval:TimeInterval){
        if (device?.hasTorch ?? false){
            do {
                try device?.lockForConfiguration()
                device?.torchMode = AVCaptureDevice.TorchMode.on
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
        queue = DispatchWorkItem  {
            if (device?.hasTorch ?? false){
                do {
                    try device?.lockForConfiguration()
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                    device?.unlockForConfiguration()
                } catch {
                    print(error)
                }
            }
        }
        DispatchQueue.global().asyncAfter(deadline: .now() + interval) {
            queue?.perform()
        }
    }

    static func toggleTorch() {//on: Bool
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
        else { return }

        do {
            try device.lockForConfiguration()
            device.torchMode = !device.isTorchActive ? .on : .off
            device.unlockForConfiguration()
        } catch {
            print("Torch could not be used")
        }
    }
    
    static func isTorchOn()->Bool {//on: Bool
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch else { return false}

        do {
            try device.lockForConfiguration()
            return device.isTorchActive
        } catch {
            print("Torch could not be used")
            return false
        }
    }
    
    static func torchOn() {//on: Bool
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
        else { return }

        do {
            try device.lockForConfiguration()
            device.torchMode = AVCaptureDevice.TorchMode.on//!device.isTorchActive ? .on : .off
            device.unlockForConfiguration()
        } catch {
            print("Torch could not be used")
        }
    }

    static func toggleBrightness() {
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
            else { return }
        
        do {
            var brightnessLevel = device.torchLevel
            brightnessLevel = increaseBrightness ? brightnessLevel + 0.1 : brightnessLevel - 0.1
            
            if brightnessLevel > 1.0 {
                brightnessLevel = 1.0
                increaseBrightness = false
            }
            else if brightnessLevel < 0.1 {
                brightnessLevel = 0.1
                increaseBrightness = true
            }
            
            try device.lockForConfiguration()
            try device.setTorchModeOn(level: brightnessLevel)
            
            device.unlockForConfiguration()
        } catch {
            print("Torch could not be used")
        }
    }
}
