//
//  NoDataItemCell.swift
//  Frenzy
//
//  Created by CP on 2/13/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class NoDataItemCell: UITableViewCell {

    static let identifier = "NoDataItemCell"
    
    @IBOutlet weak var ivNotFoundImage:UIImageView!
    @IBOutlet weak var lblNotFoundTitle:UILabel!
    @IBOutlet weak var lblNotFoundDescription:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblNotFoundTitle.textAlignment = .center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var onClickNotFound:(() -> Void)?
    
    @IBAction func onClickFound(_ sender:UIButton){
        self.onClickNotFound?()
    }
    
    @IBAction func onClickBtnNotFound(_ sender:UIButton){
        self.onClickNotFound?()
    }
}
