

import UIKit

protocol NoItemFoundViewDelegate {
    func onClickNotFound()
}

class NoItemFoundView: UIView {
    static let identifier = "NoItemFoundView"
    var delegate:NoItemFoundViewDelegate?
    @IBOutlet var contentView : UIView!
    @IBOutlet weak var ivNotFoundImage:UIImageView!
    @IBOutlet weak var lblNotFoundTitle:UILabel!
    @IBOutlet weak var lblNotFoundDescription:UILabel!
    @IBOutlet weak var btnNotFound:UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI(){
        Bundle.main.loadNibNamed(NoItemFoundView.identifier, owner: self, options: nil)
        addSubview(contentView)
        self.contentView.frame =  self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight , .flexibleWidth]
    }
    
    @IBAction func onClickBtnNotFound(_ sender:UIButton){
        self.delegate?.onClickNotFound()
    }
    @IBAction func onClickBtnTemp(_ sender:UIButton){
        self.delegate?.onClickNotFound()
    }
    
}
